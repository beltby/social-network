FROM bellsoft/liberica-openjdk-centos:8u372
RUN mkdir socialnetwork
COPY /web/target/web-1.0-SNAPSHOT.war /socialnetwork
WORKDIR socialnetwork/
CMD ["java", "-jar", "web-1.0-SNAPSHOT.war"]
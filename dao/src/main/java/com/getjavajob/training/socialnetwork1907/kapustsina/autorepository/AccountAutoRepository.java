package com.getjavajob.training.socialnetwork1907.kapustsina.autorepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public interface AccountAutoRepository extends JpaRepository<Account, Integer>, AccountRepository {

    @Override
    default Account get(int id) {
        return findById(id).orElseThrow(() -> new RuntimeException("Account with ID=" + id + " not found"));
    }

    @Override
    default Optional<SessionAccountDto> get(String email, String password) {
        return findAccountByEmailAndPassword(email, password);
    }

    Optional<SessionAccountDto> findAccountByEmailAndPassword(String email, String password);

    @Override
    default Account get(String email) {
        return getByEmail(email);
    }

    Account getByEmail(String email);

    @Override
    @Query("SELECT account.photo FROM Account account WHERE account.id = :id")
    AbstractImage getPhoto(@Param("id") int id);

    @Override
    default Account create(Account account) {
        return save(account);
    }

    @Override
    @Query("SELECT account FROM Account account JOIN FETCH account.phones WHERE account.id = :id")
    Account getWithLazyFields(@Param("id") int id);

    @Override
    default Page<Account> search(String searchQuery, int limit, int offset) {
        Pageable pageable = PageRequest.of(offset, limit);
        return findAllByFirstNameIsContainingIgnoreCaseOrLastNameIsContainingIgnoreCaseOrderById(searchQuery,
                searchQuery, pageable);
    }

    Page<Account> findAllByFirstNameIsContainingIgnoreCaseOrLastNameIsContainingIgnoreCaseOrderById(String firstName,
            String lastName, Pageable page);

    @Override
    default List<Account> getAll() {
        return findAll();
    }

    @Override
    default void remove(Account account) {
        delete(account);
    }

    @Override
    default void update(Account account) {
        save(account);
    }

}
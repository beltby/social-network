package com.getjavajob.training.socialnetwork1907.kapustsina.autorepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.FriendshipRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
public interface FriendshipAutoRepository extends JpaRepository<Friendship, Friendship.FriendshipId>, FriendshipRepository {

    @Override
    default void create(Friendship friendship) {
        save(friendship);
    }

    @Override
    @Query("SELECT f FROM Friendship f WHERE (f.sender = :sender AND f.recipient = :recipient) OR (f.sender = :recipient AND f.recipient = :sender)")
    Friendship get(@Param("sender") Account sender, @Param("recipient") Account recipient);

    @Override
    default void update(Friendship friendship) {
        save(friendship);
    }

    @Override
    default void remove(Friendship friendship) {
        delete(friendship);
    }

    @Override
    @Query("SELECT f FROM Friendship f JOIN FETCH f.recipient r JOIN FETCH f.sender s WHERE (f.sender = :user OR f.recipient = :user) AND NOT f.status='friend' ORDER BY f.sender.id, f.recipient.id")
    List<Friendship> getFriendshipRequests(@Param("user") Account user);

    @Override
    @Query("SELECT f FROM Friendship f JOIN FETCH f.recipient r JOIN FETCH f.sender s WHERE (f.sender = :user OR f.recipient = :user) AND f.status='friend'")
    List<Friendship> getFriends(@Param("user") Account user);

}
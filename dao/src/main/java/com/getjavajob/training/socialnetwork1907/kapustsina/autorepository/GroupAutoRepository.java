package com.getjavajob.training.socialnetwork1907.kapustsina.autorepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
public interface GroupAutoRepository extends JpaRepository<Group, Integer>, GroupRepository {

    @Override
    @Query("SELECT g FROM Group g JOIN FETCH g.creator WHERE g.id = :id")
    Group getWithLazyFields(@Param("id") int id);

    Group findByName(String name);

    @Override
    default Group get(String name) {
        return findByName(name);
    }

    @Override
    default Group get(int id) {
        return findById(id).orElseThrow(() -> new RuntimeException("Group with ID=" + id + " not found"));
    }

    Page<Group> findAllByNameIsContainingIgnoreCaseOrderById(String name, Pageable page);

    @Override
    default Page<Group> search(String searchQuery, int limit, int offset) {
        Pageable pageable = PageRequest.of(offset, limit);
        return findAllByNameIsContainingIgnoreCaseOrderById(searchQuery, pageable);
    }

    @Override
    default List<Group> getAll() {
        return findAll();
    }

    @Override
    @Query("SELECT g.photo FROM Group g WHERE g.id = :id")
    AbstractImage getPhoto(@Param("id") int id);

    @Override
    default Group create(Group group) {
        return save(group);
    }

    @Override
    default void remove(Group group) {
        delete(group);
    }

    @Override
    default void update(Group group) {
        save(group);
    }

}
package com.getjavajob.training.socialnetwork1907.kapustsina.autorepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupMembershipRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public interface GroupMembershipAutoRepository extends JpaRepository<GroupMembership, GroupMembership.GroupMembershipId>, GroupMembershipRepository {

    @Override
    default void create(GroupMembership relationship) {
        save(relationship);
    }

    @Override
    default GroupMembership get(Account account, Group group) {
        return findGroupMembershipByUserAndGroup(account, group);
    }

    GroupMembership findGroupMembershipByUserAndGroup(Account account, Group group);

    @Override
    default void update(GroupMembership membership) {
        save(membership);
    }

    @Override
    default void remove(GroupMembership membership) {
        delete(membership);
    }

    @Override
    @Query("SELECT g.role FROM GroupMembership g WHERE g.user=:user AND g.group=:group")
    Optional<String> getUserRole(@Param("user") Account user, @Param("group") Group group);

    @Override
    @Query("SELECT g.user FROM GroupMembership g WHERE g.group=:group AND g.role='request'")
    List<Account> getIncomingJoiningRequests(@Param("group") Group group);

    @Override
    @Query("SELECT g.group FROM GroupMembership g JOIN FETCH g.group.creator c WHERE g.user = :user AND NOT g.role = 'request'")
    List<Group> getUserGroups(@Param("user") Account user);

    //The first way to initialize a lazy field.
    @Override
    @Query("SELECT groupMembership FROM GroupMembership groupMembership JOIN FETCH groupMembership.group g JOIN FETCH g.creator c WHERE groupMembership.user = :user AND NOT groupMembership.role = 'request' ORDER BY groupMembership.role DESC, groupMembership.group.id")
    List<GroupMembership> getUserGroupsWithRoles(@Param("user") Account user);

    @Override
    default List<GroupMembership> getGroupMembershipUserRequests(Account user) {
        return getGroupMembershipByUserAndRoleIs(user, "request");
    }

    //The second way to initialize a lazy field.
    @EntityGraph(attributePaths = {"group", "group.creator"})
    List<GroupMembership> getGroupMembershipByUserAndRoleIs(Account user, String role);

    @Override
    default List<GroupMembership> getGroupFollowers(Group group) {
        return getGroupMembershipsByGroupAndRoleNotOrderByRoleDescUser(group, "request");
    }

    @EntityGraph(attributePaths = {"user"})
    List<GroupMembership> getGroupMembershipsByGroupAndRoleNotOrderByRoleDescUser(Group group, String role);

}
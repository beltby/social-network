package com.getjavajob.training.socialnetwork1907.kapustsina.autorepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.AbstractMessage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.MessageRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
public interface MessageAutoRepository extends JpaRepository<AbstractMessage, Integer>, MessageRepository {

    @Override
    default int create(AbstractMessage message) {
        return save(message).getId();
    }

    @SuppressWarnings("SqlResolve")
    @Override
    @Query(value = "SELECT id, sender_id, recipient_id, msg_text, msg_scope, creation_time, photo_exists FROM (WITH constants (user_id) AS (VALUES (?)) SELECT MAX(id) AS max_id, CASE WHEN (sender_id = user_id) then recipient_id else sender_id  end as interlocutor FROM socialnetwork.message, constants WHERE (sender_id = user_id or recipient_id = user_id) and msg_scope = 'chat' GROUP BY interlocutor)  AS a INNER JOIN socialnetwork.message ON a.max_id = id ORDER BY id DESC",
            nativeQuery = true)
    List<Message> getLastChatMessages(@Param("user_id") int userId);

    @Override
    @Query("SELECT message FROM Message message WHERE (message.sender = :sender AND message.recipient = :recipient) OR (message.sender = :recipient AND message.recipient = :sender) AND message.scope ='chat' ORDER BY message.id")
    List<Message> getChat(@Param("sender") Account sender, @Param("recipient") Account recipient);

    @Override
    @Query("SELECT groupPost FROM GroupPost groupPost JOIN FETCH groupPost.sender s WHERE groupPost.recipient = :recipient ORDER BY groupPost.id DESC ")
    List<GroupPost> getGroupPosts(@Param("recipient") Group recipient);

    @Override
    @Query("SELECT profilePost FROM ProfilePost profilePost JOIN FETCH profilePost.sender s WHERE profilePost.recipient = :recipient ORDER BY profilePost.id DESC")
    List<ProfilePost> getProfilePosts(@Param("recipient") Account recipient);

    @Override
    @Query("SELECT m.image FROM AbstractMessage m WHERE m.id = :id")
    AbstractImage getPhoto(@Param("id") int id);

    @Override
    @Query("SELECT profilePost.id from ProfilePost profilePost where profilePost.sender.id=:userId and profilePost.id<:postId")
    List<Integer> getUncachedProfilePostIds(@Param("userId") int userId,
            @Param("postId") int lastCachedProfilePostId);

    @Override
    @Query("SELECT profilePost FROM ProfilePost profilePost WHERE profilePost.id = :id")
    ProfilePost get(int id);

}
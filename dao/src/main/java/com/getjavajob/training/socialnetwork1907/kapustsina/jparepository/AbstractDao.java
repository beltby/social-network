package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.EntityDao;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.HavingPhoto;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.Searchable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public abstract class AbstractDao<T> implements EntityDao<T>, Searchable<T>, HavingPhoto {
    private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);

    private Class<T> entityClass;
    @PersistenceContext
    private EntityManager entityManager;

    public Class<T> getEntityClass() {
        return entityClass;
    }

    protected void setTClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public T create(T entity) {
        logger.debug("Create new entity of class {}.", getEntityClass().getSimpleName());
        getEntityManager().persist(entity);
        return entity;
    }

    @Override
    public T get(int id) {
        logger.debug("Get entity of class {} from DB, id = {}.", getEntityClass().getSimpleName(), id);
        try {
            return getEntityManager().find(entityClass, id);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<T> getAll() {
        logger.debug("Get all entities of class {} from DB.", getEntityClass().getSimpleName());
        return getEntityManager().createQuery("from " + entityClass.getName()).getResultList();
    }

    @Override
    public void update(T entity) {
        logger.debug("Update entity of class {}.", getEntityClass().getSimpleName());
        getEntityManager().merge(entity);
    }

    @Override
    public void remove(T entity) {
        logger.debug("Delete entity of class {}.", getEntityClass().getSimpleName());
        getEntityManager().remove(entity);
    }

    @Override
    public AbstractImage getPhoto(int id) {
        logger.debug("Get image from DB, id = {}.", id);
        TypedQuery<AbstractImage> query = getEntityManager().createQuery(
                "SELECT entity.photo FROM " + entityClass.getName() + " entity WHERE entity.id = :id",
                AbstractImage.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
}
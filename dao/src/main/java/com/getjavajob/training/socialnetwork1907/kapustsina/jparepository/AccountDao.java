package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Optional;

@SuppressWarnings("JpaQlInspection")
@Repository
public class AccountDao extends AbstractDao<Account> implements AccountRepository {
    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    public AccountDao() {
        setTClass(Account.class);
    }

    @Override
    public Optional<SessionAccountDto> get(String email, String password) {
        logger.debug("Attempt to authorize. email = {}.", email);
        TypedQuery<SessionAccountDto> query = getEntityManager().createQuery(
                "SELECT new com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto " +
                        "(account.id, account.email, account.firstName, account.lastName, account.administrator, " +
                        "account.avatarExists, account.active) FROM Account account " +
                        "WHERE account.email = :email AND account.password = :password", SessionAccountDto.class);
        query.setParameter("email", email);
        query.setParameter("password", password);
        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Account get(String email) {
        logger.debug("Get account by email. email = {}.", email);
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Account> root = query.from(Account.class);
            query.select(root);
            query.where(builder.equal(root.get("email"), email));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Account getWithLazyFields(int id) {
        logger.debug("Get account with lazy fields. id = {}.", id);
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Account> root = query.from(Account.class);
            root.fetch("phones", JoinType.INNER);
            query.select(root);
            query.where(builder.equal(root.get("id"), id));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Page<Account> search(String searchQuery, int limit, int offset) {
        logger.debug("Searching for query = {}. Limit = {}, offset = {}.", searchQuery, limit, offset);
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = builder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate predicate = builder.or(
                builder.like(builder.lower(root.get("firstName")), "%" + searchQuery.toLowerCase() + "%"),
                builder.like(builder.lower(root.get("lastName")), "%" + searchQuery.toLowerCase() + "%")
        );
        criteriaQuery.where(predicate);
        criteriaQuery.orderBy(builder.asc(root.get("id")));

        TypedQuery<Account> query = getEntityManager().createQuery(criteriaQuery);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return new PageImpl<>(query.getResultList());
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class FriendshipDao implements com.getjavajob.training.socialnetwork1907.kapustsina.repository.FriendshipRepository {
    private static final Logger logger = LoggerFactory.getLogger(FriendshipDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public List<Friendship> getFriends(Account user) {
        logger.debug("Getting a list of friendship requests by user id = {}.", user.getId());
        TypedQuery<Friendship> query = getEntityManager().createQuery(
                "SELECT f FROM Friendship f " +
                        "JOIN FETCH f.recipient r " +
                        "JOIN FETCH f.sender s " +
                        "WHERE (f.sender = :user " +
                        "OR f.recipient = :user) " +
                        "AND f.status='friend' ", Friendship.class);
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Override
    public List<Friendship> getFriendshipRequests(Account user) {
        logger.debug("Getting a list of friendship requests by user id = {}.", user.getId());
        @SuppressWarnings("JpaQlInspection") TypedQuery<Friendship> query = getEntityManager().createQuery(
                "SELECT f FROM Friendship f " +
                        "JOIN FETCH f.recipient r " +
                        "JOIN FETCH f.sender s " +
                        "WHERE (f.sender = :user " +
                        "OR f.recipient = :user ) " +
                        "AND NOT f.status='friend' " +
                        "ORDER BY f.sender.id, f.recipient.id ", Friendship.class);
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Override
    public Friendship get(Account user, Account friend) {
        logger.debug("Getting the friendship entity between users id = {} and id = {}.", user.getId(), friend.getId());
        TypedQuery<Friendship> query = getEntityManager().createQuery(
                "SELECT f FROM Friendship f " +
                        "WHERE (f.sender = :user AND f.recipient = :friend) " +
                        "OR (f.sender = :friend AND f.recipient = :user)", Friendship.class);
        query.setParameter("user", user);
        query.setParameter("friend", friend);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void create(Friendship friendship) {
        logger.debug("Create friendship between users id = {} and id = {}.",
                friendship.getSender().getId(), friendship.getRecipient().getId());
        getEntityManager().persist(friendship);
    }

    @Override
    public void remove(Friendship friendship) {
        logger.debug("Delete friendship between users id = {} and id = {}.",
                friendship.getSender().getId(), friendship.getRecipient().getId());
        getEntityManager().remove(friendship);
    }

    @Override
    public void update(Friendship friendship) {
        logger.debug("Update friendship between users id = {} and id = {}.",
                friendship.getSender().getId(), friendship.getRecipient().getId());
        getEntityManager().merge(friendship);
    }
}
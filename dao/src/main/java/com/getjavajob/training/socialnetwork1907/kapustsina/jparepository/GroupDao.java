package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@SuppressWarnings("JpaQlInspection")
@Repository
public class GroupDao extends AbstractDao<Group> implements GroupRepository {
    public GroupDao() {
        setTClass(Group.class);
    }

    @Override
    public Group getWithLazyFields(int id) {
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Group> query = builder.createQuery(Group.class);
            Root<Group> root = query.from(Group.class);
            root.fetch("creator", JoinType.INNER);
            query.select(root);
            query.where(builder.equal(root.get("id"), id));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Group get(String name) {
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Group> query = builder.createQuery(Group.class);
            Root<Group> root = query.from(Group.class);
            query.select(root);
            query.where(builder.equal(root.get("name"), name));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Page<Group> search(String searchQuery, int limit, int offset) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = builder.createQuery(Group.class);
        Root<Group> root = criteriaQuery.from(Group.class);
        Predicate predicate = builder.or(
                builder.like(builder.lower(root.get("name")), "%" + searchQuery.toLowerCase() + "%")
        );
        criteriaQuery.where(predicate);
        criteriaQuery.orderBy(builder.asc(root.get("id")));

        TypedQuery<Group> query = getEntityManager().createQuery(criteriaQuery);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return new PageImpl<>(query.getResultList());
    }
}
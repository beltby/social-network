package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupMembershipRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

@SuppressWarnings("JpaQlInspection")
@Repository
public class GroupMembershipDao implements GroupMembershipRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public List<Account> getIncomingJoiningRequests(Group group) {
        TypedQuery<Account> query = getEntityManager().createQuery(
                "SELECT groupMembership.user FROM GroupMembership groupMembership " +
                        "WHERE groupMembership.group = :group " +
                        "AND groupMembership.role = :status", Account.class);
        query.setParameter("group", group);
        query.setParameter("status", "request");
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            e.printStackTrace();
            return emptyList();
        }
    }

    @Override
    public Optional<String> getUserRole(Account user, Group group) {
        TypedQuery<String> query = getEntityManager().createQuery(
                "SELECT groupMembership.role FROM GroupMembership groupMembership " +
                        "WHERE groupMembership.user = :user " +
                        "AND groupMembership.group = :group", String.class);
        query.setParameter("user", user);
        query.setParameter("group", group);
        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public List<Group> getUserGroups(Account user) {
        TypedQuery<Group> query = getEntityManager().createQuery(
                "SELECT groupMembership.group FROM GroupMembership groupMembership " +
                        "JOIN FETCH groupMembership.group.creator c " +
                        "WHERE groupMembership.user = :user " +
                        "AND NOT groupMembership.role = :role", Group.class);
        query.setParameter("user", user);
        query.setParameter("role", "request");
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            e.printStackTrace();
            return emptyList();
        }
    }

    @Override
    public List<GroupMembership> getUserGroupsWithRoles(Account user) {
        TypedQuery<GroupMembership> query = getEntityManager().createQuery(
                "SELECT groupMembership FROM GroupMembership groupMembership " +
                        "JOIN FETCH groupMembership.group g " +
                        "JOIN FETCH g.creator c " +
                        "WHERE groupMembership.user = :user  " +
                        "AND NOT groupMembership.role = :role " +
                        "ORDER BY groupMembership.role DESC, groupMembership.group.id", GroupMembership.class);
        query.setParameter("user", user);
        query.setParameter("role", "request");
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            e.printStackTrace();
            return emptyList();
        }
    }

    @Override
    public List<GroupMembership> getGroupMembershipUserRequests(Account user) {
        TypedQuery<GroupMembership> query = getEntityManager().createQuery(
                "SELECT groupMembership FROM GroupMembership groupMembership " +
                        "JOIN FETCH groupMembership.group g " +
                        "JOIN FETCH g.creator c " +
                        "WHERE groupMembership.user = :user " +
                        "AND groupMembership.role = :role", GroupMembership.class);
        query.setParameter("user", user);
        query.setParameter("role", "request");
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            e.printStackTrace();
            return emptyList();
        }
    }

    @Override
    public List<GroupMembership> getGroupFollowers(Group group) {
        TypedQuery<GroupMembership> query = getEntityManager().createQuery(
                "SELECT groupMembership FROM GroupMembership groupMembership " +
                        "JOIN FETCH groupMembership.user u " +
                        "WHERE groupMembership.group = :group " +
                        "AND NOT groupMembership.role = :role " +
                        "ORDER BY groupMembership.role DESC, groupMembership.user.id", GroupMembership.class);
        query.setParameter("group", group);
        query.setParameter("role", "request");
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            e.printStackTrace();
            return emptyList();
        }
    }

    @Override
    public GroupMembership get(Account user, Group group) {
        TypedQuery<GroupMembership> query = getEntityManager().createQuery(
                "SELECT groupMembership FROM GroupMembership groupMembership " +
                        "WHERE groupMembership.user = :user " +
                        "AND groupMembership.group = :group", GroupMembership.class);
        query.setParameter("user", user);
        query.setParameter("group", group);
        return query.getSingleResult();
    }

    @Override
    public void create(GroupMembership groupMembership) {
        getEntityManager().persist(groupMembership);
    }

    @Override
    public void remove(GroupMembership groupMembership) {
        getEntityManager().remove(groupMembership);
    }

    @Override
    public void update(GroupMembership groupMembership) {
        getEntityManager().merge(groupMembership);
    }
}

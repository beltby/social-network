package com.getjavajob.training.socialnetwork1907.kapustsina.jparepository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.AbstractMessage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.MessageRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("JpaQlInspection")
@Repository
public class MessageDao implements MessageRepository {
    private final JdbcTemplate jdbcTemplate;
    private final AccountDao accountDao;

    @PersistenceContext
    private EntityManager entityManager;

    public MessageDao(JdbcTemplate jdbcTemplate, AccountDao accountDao) {
        this.jdbcTemplate = jdbcTemplate;
        this.accountDao = accountDao;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public int create(AbstractMessage message) {
        getEntityManager().persist(message);
        getEntityManager().flush();
        return message.getId();
    }

    @SuppressWarnings("SqlResolve")
    @Override
    public List<Message> getLastChatMessages(int userId) {
        return jdbcTemplate.query(
                "SELECT id, sender_id, recipient_id, msg_text, msg_scope, creation_time, photo_exists FROM " +
                        "(WITH constants (user_id) AS (VALUES (?)) SELECT MAX(id) AS max_id, " +
                        "CASE WHEN (sender_id = user_id) then recipient_id else sender_id  end as interlocutor " +
                        "FROM socialnetwork.message, constants WHERE (sender_id = user_id or recipient_id = user_id) " +
                        "AND msg_scope = 'chat' GROUP BY interlocutor) AS a INNER JOIN socialnetwork.message " +
                        "ON a.max_id = id ORDER BY id DESC",
                (resultSet, i) -> convertToEntity(resultSet), userId);
    }

    @Override
    public List<Message> getChat(Account sender, Account recipient) {
        TypedQuery<Message> query = getEntityManager().createQuery(
                "SELECT message FROM Message message " +
                        "WHERE (message.sender = :sender AND message.recipient = :recipient) " +
                        "OR (message.sender = :recipient AND message.recipient = :sender) " +
                        "AND message.scope ='chat' " +
                        "ORDER BY message.id ", Message.class);
        query.setParameter("sender", sender);
        query.setParameter("recipient", recipient);
        return query.getResultList();
    }

    @Override
    public List<GroupPost> getGroupPosts(Group recipient) {
        TypedQuery<GroupPost> query = getEntityManager().createQuery(
                "SELECT groupPost FROM GroupPost groupPost " +
                        "JOIN FETCH groupPost.sender s " +
                        "WHERE groupPost.recipient = :recipient " +
                        "ORDER BY groupPost.id DESC ", GroupPost.class);
        query.setParameter("recipient", recipient);
        return query.getResultList();
    }

    @Override
    public List<ProfilePost> getProfilePosts(Account recipient) {
        TypedQuery<ProfilePost> query = getEntityManager().createQuery(
                "SELECT profilePost FROM ProfilePost profilePost " +
                        "JOIN FETCH profilePost.sender s " +
                        "WHERE profilePost.recipient = :recipient " +
                        "ORDER BY profilePost.id DESC ", ProfilePost.class);
        query.setParameter("recipient", recipient);
        return query.getResultList();
    }

    @Override
    public List<Integer> getUncachedProfilePostIds(int userId, int lastCachedProfilePostId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ProfilePost get(int profilePostId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AbstractImage getPhoto(int id) {
        TypedQuery<AbstractImage> query = getEntityManager().createQuery(
                "SELECT entity.image FROM AbstractMessage entity WHERE entity.id = :id",
                AbstractImage.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    private Message convertToEntity(ResultSet resultSet) {

        Message message = new Message();
        try {
            message.setId(resultSet.getInt("id"));
            message.setSender(accountDao.get(resultSet.getInt("sender_id")));
            message.setRecipient(accountDao.get(resultSet.getInt("recipient_id")));
            message.setText(resultSet.getString("msg_text"));
            message.setScope(resultSet.getString("msg_scope"));
            message.setCreationTime(resultSet.getTimestamp("creation_time").toLocalDateTime());
            message.setImageExists(resultSet.getBoolean("photo_exists"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return message;
    }
}
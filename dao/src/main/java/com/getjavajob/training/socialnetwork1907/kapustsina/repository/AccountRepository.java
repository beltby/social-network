package com.getjavajob.training.socialnetwork1907.kapustsina.repository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.EntityDao;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.HavingPhoto;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.Searchable;

import java.util.Optional;

public interface AccountRepository extends EntityDao<Account>, Searchable<Account>, HavingPhoto {

    Optional<SessionAccountDto> get(String email, String password);

}
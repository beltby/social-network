package com.getjavajob.training.socialnetwork1907.kapustsina.repository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.RelationshipDao;

import java.util.List;

public interface FriendshipRepository extends RelationshipDao<Friendship, Account, Account> {

    List<Friendship> getFriendshipRequests(Account user);

    List<Friendship> getFriends(Account user);

}
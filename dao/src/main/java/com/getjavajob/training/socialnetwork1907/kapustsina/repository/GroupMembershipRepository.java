package com.getjavajob.training.socialnetwork1907.kapustsina.repository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.RelationshipDao;

import java.util.List;
import java.util.Optional;

public interface GroupMembershipRepository extends RelationshipDao<GroupMembership, Account, Group> {

    Optional<String> getUserRole(Account user, Group group);

    List<Account> getIncomingJoiningRequests(Group group);

    List<Group> getUserGroups(Account user);

    List<GroupMembership> getUserGroupsWithRoles(Account user);

    List<GroupMembership> getGroupMembershipUserRequests(Account user);

    List<GroupMembership> getGroupFollowers(Group group);

}
package com.getjavajob.training.socialnetwork1907.kapustsina.repository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.EntityDao;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.HavingPhoto;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.Searchable;

public interface GroupRepository extends EntityDao<Group>, Searchable<Group>, HavingPhoto {
}
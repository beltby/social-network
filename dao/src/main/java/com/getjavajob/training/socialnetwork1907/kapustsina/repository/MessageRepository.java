package com.getjavajob.training.socialnetwork1907.kapustsina.repository;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.AbstractMessage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.common.HavingPhoto;

import java.util.List;

public interface MessageRepository extends HavingPhoto {

    int create(AbstractMessage message);

    List<Message> getLastChatMessages(int userId);

    List<Message> getChat(Account sender, Account recipient);

    List<GroupPost> getGroupPosts(Group recipient);

    List<ProfilePost> getProfilePosts(Account recipient);

    List<Integer> getUncachedProfilePostIds(int userId, int lastCachedProfilePostId);

    ProfilePost get(int profilePostId);

}
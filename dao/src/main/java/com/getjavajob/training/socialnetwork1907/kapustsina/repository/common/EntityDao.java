package com.getjavajob.training.socialnetwork1907.kapustsina.repository.common;

import java.util.List;

public interface EntityDao<T> {

    T create(T entity);

    T get(int id);

    T get(String data);

    T getWithLazyFields(int id);

    List<T> getAll();

    void remove(T entity);

    void update(T entity);

}
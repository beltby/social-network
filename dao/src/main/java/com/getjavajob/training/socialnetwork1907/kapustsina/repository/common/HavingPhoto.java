package com.getjavajob.training.socialnetwork1907.kapustsina.repository.common;

import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;

public interface HavingPhoto {

    AbstractImage getPhoto(int id);

}
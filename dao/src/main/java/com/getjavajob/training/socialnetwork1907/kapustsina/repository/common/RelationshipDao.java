package com.getjavajob.training.socialnetwork1907.kapustsina.repository.common;

public interface RelationshipDao<T, K, V> {

    void create(T relationship);

    T get(K firstEntity, V secondEntity);

    void update(T relationship);

    void remove(T relationship);

}
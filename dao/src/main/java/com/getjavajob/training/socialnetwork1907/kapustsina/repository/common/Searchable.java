package com.getjavajob.training.socialnetwork1907.kapustsina.repository.common;

import org.springframework.data.domain.Page;

public interface Searchable<T> {

    Page<T> search(String searchQuery, int limit, int offset);

}
package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AccountImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.file.Files.readAllBytes;
import static java.time.LocalDate.now;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(value = {"classpath:SQL_init_scripts/init_account.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:SQL_init_scripts/clean_account.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class AccountDaoTest {
    @Autowired
    private AccountRepository accountDao;

    @Test
    public void getByAuthorizationData() {
        Account expected = accountDao.get(2);
        Optional<SessionAccountDto> optional = accountDao.get("2@gmail.com", "2");
        assertTrue(optional.isPresent());
        SessionAccountDto actual = optional.get();
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
    }

    @Test
    public void getByIncorrectAuthorizationData() {
        assertFalse(accountDao.get("2@gmail.com", "3").isPresent());
    }

    @Test
    public void searchFirstName() {
        Page<Account> accountsPage = accountDao.search("aL", 5, 0);
        assertEquals(1, accountsPage.getTotalPages());
        assertEquals(2, accountsPage.getTotalElements());
        List<Account> accounts = accountsPage.getContent();
        assertEquals(2, accounts.size());
        assertEquals("belt@gmail.com", accounts.get(0).getEmail());
        assertEquals("3@gmail.com", accounts.get(1).getEmail());
    }

    @Test
    public void searchLastName() {
        Page<Account> accountsPage = accountDao.search("kApU", 5, 0);
        List<Account> accounts = accountsPage.getContent();
        assertEquals(2, accounts.size());
        assertEquals("belt@gmail.com", accounts.get(0).getEmail());
        assertEquals("4@gmail.com", accounts.get(1).getEmail());
    }

    @Test
    public void searchNoResults() {
        Page<Account> accounts = accountDao.search("ABC", 10, 0);
        assertEquals(0, accounts.getTotalElements());
        assertTrue(accounts.isEmpty());
        assertFalse(accounts.hasNext());
    }

    @Test
    public void getPhoto() throws IOException, URISyntaxException {
        AbstractImage image = accountDao.getPhoto(4);
        byte[] expected = readAllBytes(Paths.get(getSystemResource("images/account_4.png").toURI()));
        assertArrayEquals(expected, image.getData());
        assertEquals(expected.length, image.getSize());
        assertEquals("image/png", image.getContentType());
    }

    @Test
    public void getPhotoEmpty() {
        assertNull(accountDao.getPhoto(2).getData());
        assertNull(accountDao.getPhoto(2).getContentType());
        assertEquals(0, accountDao.getPhoto(2).getSize());
    }

    @Test
    public void create() {
        Account expected = new Account();
        expected.setEmail("11@gmail.com");
        expected.setPassword("11");
        expected.setFirstName("Ivan");
        expected.setLastName("Mironenko");
        expected.setBirthDate("10.04.1991");
        expected.setHomeAddress("BY, Gomel, Centralnaya 4, kv.49");
        expected.setWorkAddress("BY, Gomel, Bartashova 43");
        expected.setIcq("128500");
        expected.setSkype("mironenkoi");
        expected.setAdditionalData("All fine");
        expected.setAdministrator(true);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setPhoto(new AccountImage());
        Phone phone = new Phone();
        phone.setType("cell");
        phone.setValue("123456789");
        expected.setPhones(singletonList(phone));
        expected.setRegistrationDate(now());
        accountDao.create(expected);
        Account actual = accountDao.get(11);
        assertEquals(actual, expected);
        assertEquals(11, actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalData(), actual.getAdditionalData());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
        Account actualFull = accountDao.getWithLazyFields(11);
        assertEquals(1, actualFull.getPhones().size());
        assertEquals("123456789", actualFull.getPhones().get(0).getValue());
        assertEquals("cell", actualFull.getPhones().get(0).getType());
    }

    @Test
    public void createReturn() {
        Account account = new Account();
        account.setEmail("11@gmail.com");
        account.setPassword("11");
        account.setFirstName("Ivan");
        account.setLastName("Mironenko");
        account.setBirthDate("10.04.1991");
        account.setHomeAddress("BY, Gomel, Centralnaya 4, kv.49");
        account.setWorkAddress("BY, Gomel, Bartashova 43");
        account.setIcq("758358210");
        account.setAdditionalData("All fine");
        account.setRegistrationDate(now());
        account.setPhoto(new AccountImage());
        assertEquals(account, accountDao.create(account));
    }

    @Test(expected = DataIntegrityViolationException.class)
    @Transactional(propagation = Propagation.NEVER)
    public void createFailed() {
        Account expected = new Account();
        expected.setEmail("2@gmail.com");
        expected.setPassword("2");
        expected.setFirstName("Ivan");
        expected.setLastName("Mironenko");
        expected.setBirthDate("10.04.1991");
        expected.setHomeAddress("BY, Gomel, Centralnaya 4, kv.49");
        expected.setWorkAddress("BY, Gomel, Bartashova 43");
        expected.setIcq("128500");
        expected.setSkype("mironenkoi");
        expected.setAdditionalData("All fine");
        expected.setAdministrator(true);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setRegistrationDate(now());
        expected.setPhoto(new AccountImage());
        accountDao.create(expected);
    }

    @Test
    public void get() {
        Account expected = new Account();
        expected.setId(2);
        expected.setEmail("2@gmail.com");
        expected.setPassword("2");
        expected.setFirstName("Dmitry");
        expected.setLastName("Uranou");
        expected.setBirthDate("18.11.1981");
        expected.setHomeAddress("BY, Gomel, Centralnaya 2, kv.9");
        expected.setWorkAddress("BY, Gomel, Bartashova 3");
        expected.setIcq("758658210");
        expected.setAdditionalData("Great guy too");
        expected.setAdministrator(false);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setRegistrationDate(now());
        expected.setPhoto(new AccountImage());
        Account actual = accountDao.get(2);
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalData(), actual.getAdditionalData());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
    }

    @Test
    public void getByEmail() {
        Account expected = new Account();
        expected.setId(2);
        expected.setEmail("2@gmail.com");
        expected.setPassword("2");
        expected.setFirstName("Dmitry");
        expected.setLastName("Uranou");
        expected.setBirthDate("18.11.1981");
        expected.setHomeAddress("BY, Gomel, Centralnaya 2, kv.9");
        expected.setWorkAddress("BY, Gomel, Bartashova 3");
        expected.setIcq("758658210");
        expected.setAdditionalData("Great guy too");
        expected.setAdministrator(false);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setRegistrationDate(now());
        expected.setPhoto(new AccountImage());
        Account actual = accountDao.get("2@gmail.com");
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalData(), actual.getAdditionalData());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
    }

    @Test
    public void getNullByEmail() {
        assertNull(accountDao.get("777@gmail.com"));
    }

    @Test
    public void getWithLazyFields() {
        Account expected = new Account();
        expected.setId(2);
        expected.setEmail("2@gmail.com");
        expected.setPassword("2");
        expected.setFirstName("Dmitry");
        expected.setLastName("Uranou");
        expected.setBirthDate("18.11.1981");
        expected.setHomeAddress("BY, Gomel, Centralnaya 2, kv.9");
        expected.setWorkAddress("BY, Gomel, Bartashova 3");
        expected.setIcq("758658210");
        expected.setAdditionalData("Great guy too");
        expected.setAdministrator(false);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setRegistrationDate(now());
        expected.setPhoto(new AccountImage());
        Phone workPhone = new Phone();
        workPhone.setType("work");
        workPhone.setValue("+375233423665");
        Phone cellPhone = new Phone();
        cellPhone.setType("cell");
        cellPhone.setValue("+375447970737");
        List<Phone> phones = new ArrayList<>();
        phones.add(cellPhone);
        phones.add(workPhone);
        expected.setPhones(phones);
        Account actual = accountDao.getWithLazyFields(2);
        assertEquals(expected, actual);
        assertEquals(expected.getPhones(), actual.getPhones());
        assertEquals(expected.getPhones().size(), actual.getPhones().size());
        assertEquals(expected.getPhones().get(0), actual.getPhones().get(0));
        assertEquals(expected.getPhones().get(1), actual.getPhones().get(1));
        assertEquals(expected.getPhones().get(1).getType(), actual.getPhones().get(1).getType());
        assertEquals(expected.getPhones().get(1).getValue(), actual.getPhones().get(1).getValue());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalData(), actual.getAdditionalData());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
    }

    @Test
    public void getAll() {
        List<Account> accounts = accountDao.getAll();
        assertEquals("belt@gmail.com", accounts.get(0).getEmail());
        assertEquals("2@gmail.com", accounts.get(1).getEmail());
        assertEquals("3@gmail.com", accounts.get(2).getEmail());
        assertEquals("4@gmail.com", accounts.get(3).getEmail());
        assertEquals("5@gmail.com", accounts.get(4).getEmail());
        assertEquals("6@gmail.com", accounts.get(5).getEmail());
        assertEquals("7@gmail.com", accounts.get(6).getEmail());
        assertEquals("8@gmail.com", accounts.get(7).getEmail());
        assertEquals("9@gmail.com", accounts.get(8).getEmail());
        assertEquals("10@gmail.com", accounts.get(9).getEmail());
        assertEquals("2", accounts.get(1).getPassword());
        assertEquals(now(), accounts.get(4).getRegistrationDate());
        assertNull(accounts.get(4).getIcq());
        assertNull(accounts.get(4).getAdditionalData());
        assertNotNull(accounts.get(4).getHomeAddress());
    }

    @Test
    public void getAllSize() {
        List<Account> accounts = accountDao.getAll();
        assertEquals(10, accounts.size());
    }

    @Test(expected = RuntimeException.class)
    public void remove() {
        Account account = accountDao.get(1);
        accountDao.remove(account);
        accountDao.get(1);
    }

    @Test
    public void updatePhoto() throws URISyntaxException, IOException {
        Account account = accountDao.get(3);
        AccountImage image = new AccountImage();
        image.setData(readAllBytes(Paths.get(getSystemResource("images/account_3.jpg").toURI())));
        image.setContentType("image/jpg");
        image.setSize(readAllBytes(Paths.get(getSystemResource("images/account_3.jpg").toURI())).length);
        account.setPhoto(image);
        accountDao.update(account);
        byte[] expected = readAllBytes(Paths.get(getSystemResource("images/account_3.jpg").toURI()));
        assertArrayEquals(expected, accountDao.getPhoto(3).getData());
        assertEquals("image/jpg", accountDao.getPhoto(3).getContentType());
        assertEquals(838, accountDao.getPhoto(3).getSize());
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void update() {
        Account expected = accountDao.get(2);
        expected.setEmail("flostongear@gmail.com");
        expected.setPassword("000");
        expected.setFirstName("Ivan");
        expected.setLastName("Mironenko");
        expected.setBirthDate("10.04.1991");
        expected.setHomeAddress("BY, Gomel, Centralnaya 4, kv.49");
        expected.setWorkAddress("BY, Gomel, Bartashova 43");
        expected.setIcq("00000");
        expected.setAdditionalData("All fine");
        expected.setAdministrator(false);
        expected.setAvatarExists(false);
        expected.setActive(true);
        Phone phone = new Phone();
        phone.setType("work");
        phone.setValue("123456789");
        expected.setPhones(Collections.singletonList(phone));
        accountDao.update(expected);
        Account actual = accountDao.getWithLazyFields(2);
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalData(), actual.getAdditionalData());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAdministrator(), actual.isAdministrator());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isActive(), actual.isActive());
        assertEquals(expected.getPhones().size(), actual.getPhones().size());
        assertEquals(expected.getPhones().get(0).getValue(), actual.getPhones().get(0).getValue());
        assertEquals(expected.getPhones().get(0).getType(), actual.getPhones().get(0).getType());
    }

    /*@Test(expected = DataIntegrityViolationException.class)*/
    @Test
    //todo Sonar Exception rule
    @Transactional(propagation = Propagation.NEVER)
    public void updateFailed() {
        Account expected = new Account();
        expected.setId(1);
        expected.setEmail("3@gmail.com");
        expected.setPassword("2");
        expected.setFirstName("Ivan");
        expected.setLastName("Mironenko");
        expected.setBirthDate("10.04.1991");
        expected.setHomeAddress("BY, Gomel, Centralnaya 4, kv.49");
        expected.setWorkAddress("BY, Gomel, Bartashova 43");
        expected.setIcq("128500");
        expected.setSkype("mironenkoi");
        expected.setAdditionalData("All fine");
        expected.setAdministrator(true);
        expected.setAvatarExists(false);
        expected.setActive(true);
        expected.setRegistrationDate(now());
        /*accountDao.update(expected);*/
        assertThrows(DataIntegrityViolationException.class, () -> accountDao.update(expected));
    }

    @Test(expected = RuntimeException.class)
    /*@Test*/
    public void getException() {
        assertEquals(15, accountDao.get(15).getId());
        /*assertNull(accountDao.get(15));*/
        /*assertThrows(RuntimeException.class, () -> accountDao.get(15));
        System.out.println("After throw");*/
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.FriendshipRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(value = {"classpath:SQL_init_scripts/init_account.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:SQL_init_scripts/clean_account.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class FriendshipDaoTest {

    @Autowired
    private FriendshipRepository friendshipDao;

    @Autowired
    private AccountRepository accountDao;

    @Test
    public void getFriendshipRequests() {
        List<Friendship> friendshipRequests = friendshipDao.getFriendshipRequests(accountDao.get(1));
        assertEquals(2, friendshipRequests.size());
        assertEquals(3, friendshipRequests.get(0).getRecipient().getId());
        assertEquals(4, friendshipRequests.get(1).getSender().getId());
    }

    @Test
    public void getFriendshipRequestsEmpty() {
        assertTrue(friendshipDao.getFriendshipRequests(accountDao.get(10)).isEmpty());
    }

    @Test
    public void getFriends() {
        List<Friendship> friendships = friendshipDao.getFriends(accountDao.get(1));
        assertEquals(4, friendships.size());
        assertEquals(2, friendships.get(0).getRecipient().getId());
        assertEquals(6, friendships.get(1).getRecipient().getId());
        assertEquals(7, friendships.get(2).getRecipient().getId());
        assertEquals(8, friendships.get(3).getRecipient().getId());
    }

    @Test
    public void getFriendsEmpty() {
        assertTrue(friendshipDao.getFriends(accountDao.get(10)).isEmpty());
    }

    @Test
    public void createFriendship() {
        Account first = accountDao.get(3);
        Account second = accountDao.get(7);
        Friendship friendship = new Friendship();
        friendship.setSender(first);
        friendship.setRecipient(second);
        friendship.setStatus("sent");
        assertNull(friendshipDao.get(first, second));
        friendshipDao.create(friendship);
        assertNotNull(friendshipDao.get(first, second));
        assertEquals("sent", friendshipDao.get(first, second).getStatus());
    }

    @Test
    public void getFriendship() {
        Account first = accountDao.get(1);
        Account second = accountDao.get(3);
        Account third = accountDao.get(7);
        assertEquals("sent", friendshipDao.get(first, second).getStatus());
        assertEquals(1, friendshipDao.get(first, second).getSender().getId());
        assertEquals(3, friendshipDao.get(first, second).getRecipient().getId());
        assertEquals("friend", friendshipDao.get(first, third).getStatus());
    }

    @Test
    public void getFriendshipEmpty() {
        Account second = accountDao.get(3);
        Account third = accountDao.get(7);
        assertNull(friendshipDao.get(second, third));
    }

    @Test
    public void updateFriendship() {
        Account first = accountDao.get(1);
        Account second = accountDao.get(3);
        Friendship friendship = new Friendship();
        friendship.setSender(first);
        friendship.setRecipient(second);
        friendship.setStatus("friend");
        assertEquals("sent", friendshipDao.get(first, second).getStatus());
        friendshipDao.update(friendship);
        assertEquals("friend", friendshipDao.get(first, second).getStatus());
    }

    @Test
    public void deleteFriendship() {
        Account first = accountDao.get(1);
        Account second = accountDao.get(7);
        Friendship friendship = new Friendship();
        friendship.setSender(first);
        friendship.setRecipient(second);
        assertNotNull(friendshipDao.get(first, second));
        friendshipDao.remove(friendship);
        assertNull(friendshipDao.get(first, second));
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.GroupImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.file.Files.readAllBytes;
import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(value = {"classpath:SQL_init_scripts/init_group.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:SQL_init_scripts/clean_group.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class GroupDaoTest {
    @Autowired
    private GroupRepository groupDao;

    @Autowired
    private AccountRepository accountDao;

    @Test
    public void search() {
        Page<Group> groupsPage = groupDao.search("iN", 5, 0);
        assertEquals(1, groupsPage.getTotalPages());
        assertEquals(2, groupsPage.getTotalElements());
        List<Group> groups = groupsPage.getContent();
        assertEquals(2, groups.size());
        assertEquals("Weather in world", groups.get(0).getName());
        assertEquals("Minsk news", groups.get(1).getName());
    }

    @Test
    public void searchNoResults() {
        Page<Group> groupsPage = groupDao.search("ABC", 10, 0);
        assertEquals(0, groupsPage.getTotalElements());
        assertTrue(groupsPage.isEmpty());
        assertFalse(groupsPage.hasNext());
        assertTrue(groupsPage.getContent().isEmpty());
    }

    @Test
    public void getPhoto() throws IOException, URISyntaxException {
        AbstractImage image = groupDao.getPhoto(4);
        byte[] expected = readAllBytes(Paths.get(getSystemResource("images/group_4.jpg").toURI()));
        assertArrayEquals(expected, image.getData());
        assertEquals(expected.length, image.getSize());
        assertEquals("image/jpg", image.getContentType());
    }

    @Test
    public void getPhotoEmpty() {
        assertNull(groupDao.getPhoto(3).getData());
        assertNull(groupDao.getPhoto(3).getContentType());
        assertEquals(0, groupDao.getPhoto(3).getSize());
    }

    @Test
    public void create() {
        Group expected = new Group();
        expected.setName("Kotlin");
        expected.setDescription("All about kotlin.");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(now());
        expected.setAvatarExists(false);
        expected.setOpen(true);
        GroupImage groupImage = new GroupImage();
        expected.setPhoto(groupImage);
        groupDao.create(expected);
        Group actual = groupDao.get(6);
        assertEquals(expected, actual);
        assertEquals(6, actual.getId());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator().getId(), actual.getCreator().getId());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isOpen(), actual.isOpen());
    }

    @Test
    public void createReturn() {
        Group group = new Group();
        group.setName("Kotlin");
        group.setDescription("All about kotlin.");
        group.setCreator(accountDao.get(1));
        group.setRegistrationDate(now());
        group.setAvatarExists(false);
        group.setOpen(true);
        GroupImage groupImage = new GroupImage();
        group.setPhoto(groupImage);
        assertEquals(6, groupDao.create(group).getId());
    }

    @Test(expected = DataIntegrityViolationException.class)
    @Transactional(propagation = Propagation.NEVER)
    public void createFailed() {
        Group group = new Group();
        group.setName("Java");
        group.setDescription("All about kotlin.");
        group.setCreator(accountDao.get(1));
        group.setRegistrationDate(now());
        group.setAvatarExists(false);
        group.setOpen(true);
        GroupImage groupImage = new GroupImage();
        group.setPhoto(groupImage);
        assertEquals(0, groupDao.create(group).getId());
    }

    @Test
    public void get() {
        Group actual = groupDao.get(1);
        Group expected = new Group();
        expected.setId(1);
        expected.setName("Java");
        expected.setDescription("Group about JAVA");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(parse("2019-06-04"));
        expected.setOpen(true);
        expected.setAvatarExists(true);
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator().getId(), actual.getCreator().getId());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isOpen(), actual.isOpen());
    }

    @Test(expected = RuntimeException.class)
    public void getNull() {
        assertNull(groupDao.get(6));
    }

    @Test
    public void getByName() {
        Group expected = new Group();
        expected.setId(1);
        expected.setName("Java");
        expected.setDescription("Group about JAVA");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(parse("2019-06-04"));
        expected.setOpen(true);
        expected.setAvatarExists(true);
        Group actual = groupDao.get("Java");
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator().getId(), actual.getCreator().getId());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isOpen(), actual.isOpen());
    }

    @Test
    public void getByNameNull() {
        assertNull(groupDao.get("A group that doesn't exist"));
    }

    @Test
    public void getWithLazyFields() {
        Group actual = groupDao.getWithLazyFields(1);
        Group expected = new Group();
        expected.setId(1);
        expected.setName("Java");
        expected.setDescription("Group about JAVA");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(parse("2019-06-04"));
        expected.setOpen(true);
        expected.setAvatarExists(true);
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator().getId(), actual.getCreator().getId());
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isOpen(), actual.isOpen());
    }

    @Test()
    public void getWithLazyFieldsNull() {
        assertNull(groupDao.getWithLazyFields(6));
    }

    @Test
    public void getAll() {
        List<Group> groups = groupDao.getAll();
        assertEquals("Java", groups.get(0).getName());
        assertEquals("Weather in world", groups.get(1).getName());
        assertNotNull(groups.get(2).getName());
        assertEquals("Minsk news", groups.get(3).getName());
        assertEquals("Test group", groups.get(4).getName());
        assertEquals(1, groups.get(0).getCreator().getId());
        assertEquals(now().toString(), groups.get(3).getRegistrationDate().toString());
        assertNull(groups.get(1).getDescription());
    }

    @Test
    public void getAllSize() {
        List<Group> groups = groupDao.getAll();
        assertEquals(5, groups.size());
    }

    @Test(expected = RuntimeException.class)
    public void remove() {
        Group group = groupDao.get(1);
        groupDao.remove(group);
        groupDao.get(1);
    }

    @Test
    public void updatePhoto() throws URISyntaxException, IOException {
        Group group = groupDao.get(2);
        GroupImage image = new GroupImage();
        image.setData(readAllBytes(Paths.get(getSystemResource("images/group_2.jpg").toURI())));
        image.setContentType("image/jpg");
        image.setSize(readAllBytes(Paths.get(getSystemResource("images/group_2.jpg").toURI())).length);
        group.setPhoto(image);
        groupDao.update(group);
        byte[] expected = readAllBytes(Paths.get(getSystemResource("images/group_2.jpg").toURI()));
        assertArrayEquals(expected, groupDao.getPhoto(2).getData());
        assertEquals("image/jpg", groupDao.getPhoto(2).getContentType());
        assertEquals(expected.length, groupDao.getPhoto(2).getSize());
    }

    @Test
    public void update() {
        Group expected = groupDao.get(2);
        expected.setName("C++");
        expected.setDescription("All about C++");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(parse("2019-12-12"));
        expected.setAvatarExists(true);
        expected.setOpen(false);
        groupDao.update(expected);
        Group actual = groupDao.get(2);
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator().getId(), actual.getCreator().getId());
        assertEquals(expected.getCreator().getPhones().get(0), actual.getCreator().getPhones().get(0));
        System.out.println(expected.getCreator().getPhones().get(0));
        System.out.println(actual.getCreator().getPhones().get(0));
        assertEquals(expected.getRegistrationDate(), actual.getRegistrationDate());
        assertEquals(expected.isAvatarExists(), actual.isAvatarExists());
        assertEquals(expected.isOpen(), actual.isOpen());
    }

    @Test(expected = DataIntegrityViolationException.class)
    @Transactional(propagation = Propagation.NEVER)
    public void updateFailed() {
        Group expected = groupDao.get(2);
        expected.setName("Java");
        expected.setDescription("All about C++");
        expected.setCreator(accountDao.get(1));
        expected.setRegistrationDate(now());
        expected.setAvatarExists(true);
        expected.setOpen(false);
        groupDao.update(expected);
    }
}
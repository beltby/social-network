package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupMembershipRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(value = {"classpath:SQL_init_scripts/init_group_membership.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:SQL_init_scripts/clean_group_membership.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class GroupMembershipDaoTest {
    @Autowired
    private GroupMembershipRepository groupMembershipDao;

    @Autowired
    private AccountRepository accountDao;

    @Autowired
    private GroupRepository groupDao;

    @Test
    public void getUserRole() {
        assertTrue(groupMembershipDao.getUserRole(accountDao.get(1), groupDao.get(1)).isPresent());
        assertEquals("owner", groupMembershipDao.getUserRole(accountDao.get(1), groupDao.get(1)).orElse(""));
        assertEquals(Optional.of("moderator"), groupMembershipDao.getUserRole(accountDao.get(7), groupDao.get(4)));
        assertEquals("member", groupMembershipDao.getUserRole(accountDao.get(8), groupDao.get(4)).orElse(""));
        assertEquals("request", groupMembershipDao.getUserRole(accountDao.get(3), groupDao.get(1)).orElse(""));
    }

    @Test(expected = NoSuchElementException.class)
    public void getUserRoleEmpty() {
        assertFalse(groupMembershipDao.getUserRole(accountDao.get(3), groupDao.get(3)).isPresent());
        assertEquals("", groupMembershipDao.getUserRole(accountDao.get(3), groupDao.get(3)).orElse(""));
        assertEquals("", groupMembershipDao.getUserRole(accountDao.get(3), groupDao.get(3)).get());
    }

    @Test
    public void getIncomingJoiningRequests() {
        List<Account> incomingJoiningRequests = groupMembershipDao.getIncomingJoiningRequests(groupDao.get(1));
        assertEquals(2, incomingJoiningRequests.size());
        assertEquals(3, incomingJoiningRequests.get(0).getId());
        assertEquals(10, incomingJoiningRequests.get(1).getId());
    }

    @Test
    public void getIncomingJoiningRequestsEmpty() {
        assertTrue(groupMembershipDao.getIncomingJoiningRequests(groupDao.get(4)).isEmpty());
    }

    @Test
    public void getUserGroups() {
        List<Group> userGroups = groupMembershipDao.getUserGroups(accountDao.get(1));
        assertEquals(4, userGroups.size());
        assertEquals(1, userGroups.get(0).getId());
        assertEquals(3, userGroups.get(1).getId());
        assertEquals(4, userGroups.get(2).getId());
        assertEquals(5, userGroups.get(3).getId());
    }

    @Test
    public void getUserGroupsEmpty() {
        assertTrue(groupMembershipDao.getUserGroups(accountDao.get(4)).isEmpty());
    }

    @Test
    public void getUserGroupsWithRoles() {
        List<GroupMembership> userRoles = groupMembershipDao.getUserGroupsWithRoles(accountDao.get(1));
        assertEquals(4, userRoles.size());
        assertEquals("owner", userRoles.get(0).getRole());
        assertEquals("moderator", userRoles.get(1).getRole());
        assertEquals("member", userRoles.get(2).getRole());
        assertEquals("member", userRoles.get(3).getRole());
    }

    @Test
    public void getUserGroupsWithRolesEmpty() {
        assertTrue(groupMembershipDao.getUserGroupsWithRoles(accountDao.get(4)).isEmpty());
    }

    @Test
    public void getGroupMembershipUserRequests() {
        List<GroupMembership> outgoingJoiningRequests = groupMembershipDao.getGroupMembershipUserRequests(
                accountDao.get(3));
        assertEquals(1, outgoingJoiningRequests.size());
        assertEquals(1, outgoingJoiningRequests.get(0).getGroup().getId());
        assertEquals("request", outgoingJoiningRequests.get(0).getRole());
    }

    @Test
    public void getGroupMembershipUserRequestsEmpty() {
        assertTrue(groupMembershipDao.getGroupMembershipUserRequests(accountDao.get(1)).isEmpty());
    }

    @Test
    public void getGroupFollowers() {
        List<GroupMembership> groupFollowers = groupMembershipDao.getGroupFollowers(groupDao.get(2));
        assertEquals(3, groupFollowers.size());
        assertEquals(7, groupFollowers.get(0).getUser().getId());
        assertEquals(6, groupFollowers.get(1).getUser().getId());
        assertEquals(8, groupFollowers.get(2).getUser().getId());
    }

    @Test
    public void createGroupMembership() {
        GroupMembership firstMembership = new GroupMembership();
        firstMembership.setUser(accountDao.get(2));
        firstMembership.setGroup(groupDao.get(2));
        firstMembership.setRole("request");
        groupMembershipDao.create(firstMembership);

        GroupMembership secondMembership = new GroupMembership();
        secondMembership.setUser(accountDao.get(5));
        secondMembership.setGroup(groupDao.get(3));
        secondMembership.setRole("owner");
        groupMembershipDao.create(secondMembership);

        assertTrue(groupMembershipDao.getUserRole(accountDao.get(2), groupDao.get(2)).isPresent());
        assertEquals("request", groupMembershipDao.getUserRole(accountDao.get(2), groupDao.get(2)).orElse(""));
        assertTrue(groupMembershipDao.getUserRole(accountDao.get(5), groupDao.get(3)).isPresent());
        assertEquals("owner", groupMembershipDao.getUserRole(accountDao.get(5), groupDao.get(3)).orElse(""));
    }

    @Test
    public void getGroupMembership() {
        GroupMembership firstMembership = groupMembershipDao.get(accountDao.get(1), groupDao.get(1));
        GroupMembership secondMembership = groupMembershipDao.get(accountDao.get(7), groupDao.get(3));

        assertEquals("owner", firstMembership.getRole());
        assertEquals(accountDao.get(1), firstMembership.getUser());
        assertEquals(groupDao.get(1), firstMembership.getGroup());

        assertEquals("moderator", secondMembership.getRole());
        assertEquals(accountDao.get(7), secondMembership.getUser());
        assertEquals(groupDao.get(3), secondMembership.getGroup());
    }

    @Test
    public void getGroupMembershipEmpty() {
        assertNull(groupMembershipDao.get(accountDao.get(5), groupDao.get(2)));
    }

    @Test
    public void updateGroupMembership() {
        GroupMembership firstMembership = groupMembershipDao.get(accountDao.get(5), groupDao.get(1));
        firstMembership.setRole("moderator");
        groupMembershipDao.update(firstMembership);

        GroupMembership secondMembership = groupMembershipDao.get(accountDao.get(7), groupDao.get(2));
        secondMembership.setRole("member");
        groupMembershipDao.update(secondMembership);

        assertTrue(groupMembershipDao.getUserRole(accountDao.get(5), groupDao.get(1)).isPresent());
        assertEquals("moderator", groupMembershipDao.getUserRole(accountDao.get(5), groupDao.get(1)).orElse(""));
        assertTrue(groupMembershipDao.getUserRole(accountDao.get(7), groupDao.get(2)).isPresent());
        assertEquals("member", groupMembershipDao.getUserRole(accountDao.get(7), groupDao.get(2)).orElse(""));
    }

    @Test
    public void deleteGroupMembership() {
        GroupMembership firstMembership = groupMembershipDao.get(accountDao.get(1), groupDao.get(1));
        groupMembershipDao.remove(firstMembership);
        GroupMembership secondMembership = groupMembershipDao.get(accountDao.get(7), groupDao.get(3));
        groupMembershipDao.remove(secondMembership);

        assertFalse(groupMembershipDao.getUserRole(accountDao.get(1), groupDao.get(1)).isPresent());
        assertFalse(groupMembershipDao.getUserRole(accountDao.get(7), groupDao.get(3)).isPresent());
    }
}
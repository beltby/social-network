package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.MessageImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static java.lang.ClassLoader.getSystemResource;
import static java.nio.file.Files.readAllBytes;
import static java.time.LocalDateTime.parse;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(value = {"classpath:SQL_init_scripts/init_message.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:SQL_init_scripts/clean_message.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class MessageDaoTest {
    @Autowired
    private MessageRepository messageDao;

    @Autowired
    private AccountRepository accountDao;

    @Autowired
    private GroupRepository groupDao;

    @Test
    public void createNoImage() {
        Message expected = new Message();
        expected.setSender(accountDao.get(1));
        expected.setRecipient(accountDao.get(3));
        expected.setText("Hello! Test.");
        expected.setScope("chat");
        expected.setCreationTime(parse("2020-06-04T17:00:15"));
        expected.setImageExists(false);
        expected.setImage(new MessageImage());
        messageDao.create(expected);
        List<Message> messages = messageDao.getChat(accountDao.get(1), accountDao.get(3));
        Message actual = messages.get(messages.size() - 1);
        assertEquals(expected, actual);
        assertEquals(30, actual.getId());
        assertEquals(expected.getSender(), actual.getSender());
        assertEquals(expected.getRecipient(), actual.getRecipient());
        assertEquals(expected.getText(), actual.getText());
        assertEquals(expected.getScope(), actual.getScope());
        assertEquals(expected.getCreationTime(), actual.getCreationTime());
        assertEquals(expected.isImageExists(), actual.isImageExists());
    }

    @Test
    public void createWithImage() throws URISyntaxException, IOException {
        Message expected = new Message();
        expected.setSender(accountDao.get(1));
        expected.setRecipient(accountDao.get(3));
        expected.setText("no problem!");
        expected.setScope("chat");
        expected.setCreationTime(parse("2020-06-04T17:00:15"));
        expected.setImageExists(true);
        MessageImage image = new MessageImage();
        image.setContentType("image/jpg");
        image.setSize(readAllBytes(Paths.get(getSystemResource("images/message_21.jpg").toURI())).length);
        image.setData(readAllBytes(Paths.get(getSystemResource("images/message_21.jpg").toURI())));
        expected.setImage(image);
        messageDao.create(expected);
        List<Message> messages = messageDao.getChat(accountDao.get(1), accountDao.get(3));
        Message actual = messages.get(messages.size() - 1);
        assertEquals(expected, actual);
        assertEquals(30, actual.getId());
        assertEquals(expected.getSender(), actual.getSender());
        assertEquals(expected.getRecipient(), actual.getRecipient());
        assertEquals(expected.getText(), actual.getText());
        assertEquals(expected.getScope(), actual.getScope());
        assertEquals(expected.getCreationTime(), actual.getCreationTime());
        assertEquals(expected.isImageExists(), actual.isImageExists());
        assertArrayEquals(expected.getImage().getData(), actual.getImage().getData());
        assertEquals("image/jpg", actual.getImage().getContentType());
        assertEquals(986, actual.getImage().getSize());
    }

    @Test
    public void getChat() {
        List<Message> messages = messageDao.getChat(accountDao.get(1), accountDao.get(2));
        assertEquals(10, messages.size());
        assertEquals(2, messages.get(2).getSender().getId());
        assertEquals(1, messages.get(2).getRecipient().getId());
        assertEquals("2020-06-02T11:25:30", messages.get(2).getCreationTime().toString());
        assertFalse(messages.get(9).isImageExists());
        assertEquals("hello", messages.get(9).getText());
        assertEquals("chat", messages.get(9).getScope());
    }

    @Test
    public void getChatEmpty() {
        List<Message> messages = messageDao.getChat(accountDao.get(1), accountDao.get(7));
        assertTrue(messages.isEmpty());
    }

    @Test
    public void getGroupPosts() {
        List<GroupPost> posts = messageDao.getGroupPosts(groupDao.get(1));
        assertEquals(4, posts.size());
        assertEquals(1, posts.get(0).getSender().getId());
        assertEquals(1, posts.get(0).getRecipient().getId());
        assertEquals("2020-06-19T12:06:33", posts.get(0).getCreationTime().toString());
        assertEquals(28, posts.get(0).getId());
        assertFalse(posts.get(0).isImageExists());
    }

    @Test
    public void getGroupPostsEmpty() {
        List<GroupPost> messages = messageDao.getGroupPosts(groupDao.get(3));
        assertTrue(messages.isEmpty());
    }

    @Test
    public void getProfilePosts() throws URISyntaxException, IOException {
        List<ProfilePost> posts = messageDao.getProfilePosts(accountDao.get(1));
        assertEquals(3, posts.size());
        assertEquals(1, posts.get(0).getSender().getId());
        assertEquals(1, posts.get(0).getRecipient().getId());
        assertEquals("2020-07-09T23:00:01", posts.get(0).getCreationTime().toString());
        assertEquals(29, posts.get(0).getId());
        assertFalse(posts.get(0).isImageExists());

        assertEquals(2, posts.get(1).getSender().getId());
        assertEquals("2020-06-07T11:21:32", posts.get(1).getCreationTime().toString());
        assertEquals(20, posts.get(1).getId());
        assertTrue(posts.get(1).isImageExists());
        assertEquals(1019, posts.get(1).getImage().getSize());
        assertArrayEquals(readAllBytes(Paths.get(getSystemResource("images/message_20.jpg").toURI())),
                posts.get(1).getImage().getData());
    }

    @Test
    public void getProfilePostsEmpty() {
        List<GroupPost> messages = messageDao.getGroupPosts(groupDao.get(3));
        assertTrue(messages.isEmpty());
    }

    @Test
    public void getPhoto() throws URISyntaxException, IOException {
        byte[] expected = readAllBytes(Paths.get(getSystemResource("images/message_23.png").toURI()));
        AbstractImage actual = messageDao.getPhoto(23);
        assertArrayEquals(expected, actual.getData());
        assertEquals(expected.length, actual.getSize());
        assertEquals("image/png", actual.getContentType());
    }

    @Test
    public void getPhotoEmpty() {
        assertNull(messageDao.getPhoto(2).getData());
        assertNull(messageDao.getPhoto(2).getContentType());
        assertEquals(0, messageDao.getPhoto(2).getSize());
    }
}
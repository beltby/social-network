DROP SCHEMA IF EXISTS socialnetwork cascade;

CREATE SCHEMA socialnetwork;

CREATE TABLE socialnetwork.account
(
    id                SERIAL       NOT NULL UNIQUE,
    email             VARCHAR(45)  NOT NULL UNIQUE,
    password          VARCHAR(45)  NOT NULL,
    first_name        VARCHAR(45)  NOT NULL,
    last_name         VARCHAR(45)  NOT NULL,
    birth_date        VARCHAR(10)  NOT NULL,
    home_address      VARCHAR(80)  NULL     DEFAULT NULL,
    work_address      VARCHAR(80)  NULL     DEFAULT NULL,
    icq               VARCHAR(15)  NULL     DEFAULT NULL,
    skype             VARCHAR(45)  NULL     DEFAULT NULL,
    additional_data   VARCHAR(200) NULL     DEFAULT NULL,
    administrator     BOOLEAN      NOT NULL DEFAULT FALSE,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,

    active            BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   skype, additional_data, administrator, registration_date, active)
VALUES ('belt@gmail.com', '1', 'Alex', 'Kapustsin', '25.03.1985', 'BY, Gomel, Mira 8', 'BY, Gomel, Pobedy 18',
        '106967575', 'kapustinby', 'Great guy', TRUE, '2019-06-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   additional_data, registration_date, active)
VALUES ('2@gmail.com', '2', 'Dmitry', 'Uranou', '18.11.1981', 'BY, Gomel, Centralnaya 2, kv.9',
        'BY, Gomel, Bartashova 3', '758658210', 'Great guy too', CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   skype, additional_data, registration_date, active)
VALUES ('3@gmail.com', '3', 'Alena', 'Lebedzeva', '04.06.1989', 'BY, Moscow, Pravdi 12, kv.5',
        'BY, Moscow, Akopova 8, kv.78', 'lebedzeval', 'Great girl', '2019-06-04', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   registration_date, active)
VALUES ('4@gmail.com', '4', 'Michael', 'Kapustsin', '28.05.2016', 'BY, Gomel, Mira 8', 'Great baby', '2019-05-28',
        TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   administrator, registration_date, active)
VALUES ('5@gmail.com', '5', 'Vladimir', 'Karavaev', '19.02.1965', 'BY, Zhlobin, Internacionalnaya 21',
        'BY, Zhlobin, Promishlennaya 13', TRUE, CURRENT_DATE, FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('6@gmail.com', '6', 'Олег', 'Павленок', '02.04.1985', 'Беларусь, Гомель, Бакунина 77',
        'Беларусь, Гомель, Речицкое шоссе 45', 'Предприниматель', FALSE, '2020-09-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('7@gmail.com', '7', 'Yurii', 'Korotkevich', '14.11.1982', 'BY, Minsk, Korolya 6', 'BY, Minsk, Zhlobinskaya 100',
        'Phaeton', TRUE, CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   administrator, registration_date, active)
VALUES ('8@gmail.com', '8', 'Dasha', 'Loginovskaya', '20.12.1990', 'BY, Zhlobin, Mikrorajon 16, dom 29, kv.11',
        'Dont worry', FALSE, '2020-01-02', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('9@gmail.com', '9', 'Денис', 'Быстров', '28.06.1970', 'Беларусь, Гомель, Гагарина 1',
        'Беларусь, Гомель, Елисеева 12к4', 'Системный администратор', TRUE, '2019-11-15', FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('10@gmail.com', '10', 'Test', 'Testov', '01.01.1987', 'BY, Test, Testovaya 1', 'BY, Test, Testovaya 2',
        'Test account', TRUE, CURRENT_DATE, TRUE);

CREATE TABLE socialnetwork.phone
(
    id         SERIAL      NOT NULL UNIQUE,
    account_id INTEGER     NOT NULL,
    type       VARCHAR(4)  NOT NULL,
    value      VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'cell', '+375296580636');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'work', '+375233436558');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'cell', '+375447970737');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'work', '+375233423665');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'cell', '+375291989406');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'work', '+375233458969');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (4, 'cell', '+375296975577');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'cell', '+375296214536');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'work', '+375233426613');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'cell', '+375232578156');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'work', '+375445998510');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (7, 'cell', '+375172568800');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'cell', '+375233475569');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'work', '+375296201211');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'cell', '+375232450029');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'work', '+375297771512');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'cell', '+375233422222');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'work', '+375296666666');

CREATE TABLE socialnetwork.image_account
(
    id           SERIAL      NOT NULL UNIQUE,
    size         INTEGER     NULL DEFAULT 0,
    content_type VARCHAR(13) NULL DEFAULT NULL,
    data         bytea       NULL DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;
INSERT INTO socialnetwork.image_account DEFAULT
VALUES;

UPDATE socialnetwork.image_account
SET size=776,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/account_1.jpg')
WHERE id = 1;

UPDATE socialnetwork.image_account
SET size=838,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/account_3.jpg')
WHERE id = 3;

UPDATE socialnetwork.image_account
SET size=1313,
    content_type='image/png',
    data=FILE_READ('classpath:images/account_4.png')
WHERE id = 4;

UPDATE socialnetwork.image_account
SET size=848,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/account_5.jpg')
WHERE id = 5;

UPDATE socialnetwork.image_account
SET size=795,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/account_7.jpg')
WHERE id = 7;

UPDATE socialnetwork.image_account
SET size=662,
    content_type='image/png',
    data=FILE_READ('classpath:images/account_10.png')
WHERE id = 10;


CREATE TABLE socialnetwork.friendship
(
    sender    INTEGER     NOT NULL,
    recipient INTEGER     NOT NULL,
    status    VARCHAR(15) NOT NULL,
    PRIMARY KEY (sender, recipient)
);

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (1, 2, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (1, 6, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (1, 7, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (1, 8, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (1, 3, 'sent');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (2, 6, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (2, 7, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (2, 8, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (2, 9, 'sent');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (3, 6, 'sent');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (3, 8, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (3, 9, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (4, 1, 'sent');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (4, 5, 'friend');

INSERT INTO socialnetwork.friendship (sender, recipient, status)
VALUES (4, 9, 'friend');
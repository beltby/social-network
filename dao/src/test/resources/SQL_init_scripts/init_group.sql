DROP SCHEMA IF EXISTS socialnetwork CASCADE;

CREATE SCHEMA socialnetwork;

CREATE TABLE socialnetwork.groups
(
    id                SERIAL       NOT NULL UNIQUE,
    name              VARCHAR(70)  NOT NULL UNIQUE,
    description       VARCHAR(100) NULL,
    creator_id        INTEGER      NOT NULL,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,
    open              BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Java', 'Group about JAVA', 1, '2019-06-04', '1', '1');
INSERT INTO socialnetwork.groups (name, creator_id, registration_date, avatar_exists, open)
VALUES ('Weather in world', 1, '2019-12-12', '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, open)
VALUES ('Большие и страшные секреты', 'Группа с большими и страшными секретами', 2, CURRENT_DATE, '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Minsk news', 'Only news', 5, CURRENT_DATE, '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Test group', 'Only for tests', 10, CURRENT_DATE, '1', '1');

CREATE TABLE socialnetwork.image_group
(
    id           SERIAL      NOT NULL UNIQUE,
    size         INTEGER     NULL DEFAULT 0,
    content_type VARCHAR(13) NULL DEFAULT NULL,
    data         bytea       NULL DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.image_group DEFAULT
VALUES;
INSERT INTO socialnetwork.image_group DEFAULT
VALUES;
INSERT INTO socialnetwork.image_group DEFAULT
VALUES;
INSERT INTO socialnetwork.image_group DEFAULT
VALUES;
INSERT INTO socialnetwork.image_group DEFAULT
VALUES;

UPDATE socialnetwork.image_group
SET size=810,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/group_1.jpg')
WHERE id = 1;

UPDATE socialnetwork.image_group
SET size=907,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/group_2.jpg')
WHERE id = 2;

UPDATE socialnetwork.image_group
SET size=878,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/group_4.jpg')
WHERE id = 4;

UPDATE socialnetwork.image_group
SET size=867,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/group_5.jpg')
WHERE id = 5;

CREATE TABLE socialnetwork.group_members
(
    id_user  INTEGER     NOT NULL,
    id_group INTEGER     NOT NULL,
    role     VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_user, id_group)
);

INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 1, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 5, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 1, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 3, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 4, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 2, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 1, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 2, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 3, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 4, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 2, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (10, 5, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (3, 1, 'request');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (10, 1, 'request');

CREATE TABLE socialnetwork.account
(
    id                SERIAL       NOT NULL UNIQUE,
    email             VARCHAR(45)  NOT NULL UNIQUE,
    password          VARCHAR(45)  NOT NULL,
    first_name        VARCHAR(45)  NOT NULL,
    last_name         VARCHAR(45)  NOT NULL,
    birth_date        VARCHAR(10)  NOT NULL,
    home_address      VARCHAR(80)  NULL     DEFAULT NULL,
    work_address      VARCHAR(80)  NULL     DEFAULT NULL,
    icq               VARCHAR(15)  NULL     DEFAULT NULL,
    skype             VARCHAR(45)  NULL     DEFAULT NULL,
    additional_data   VARCHAR(200) NULL     DEFAULT NULL,
    administrator     BOOLEAN      NOT NULL DEFAULT FALSE,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,
    active            BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   skype, additional_data, administrator, registration_date, active)
VALUES ('belt@gmail.com', '1', 'Alex', 'Kapustsin', '25.03.1985', 'BY, Gomel, Mira 8', 'BY, Gomel, Pobedy 18',
        '106967575', 'kapustinby', 'Great guy', TRUE, '2019-06-01', TRUE);

CREATE TABLE socialnetwork.phone
(
    id         SERIAL      NOT NULL UNIQUE,
    account_id INTEGER     NOT NULL,
    type       VARCHAR(4)  NOT NULL,
    value      VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'cell', '+375296580636');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'work', '+375233436558');
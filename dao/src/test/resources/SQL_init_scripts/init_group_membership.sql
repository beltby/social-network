DROP SCHEMA IF EXISTS socialnetwork cascade;

CREATE SCHEMA socialnetwork;

CREATE TABLE socialnetwork.account
(
    id                SERIAL       NOT NULL UNIQUE,
    email             VARCHAR(45)  NOT NULL UNIQUE,
    password          VARCHAR(45)  NOT NULL,
    first_name        VARCHAR(45)  NOT NULL,
    last_name         VARCHAR(45)  NOT NULL,
    birth_date        VARCHAR(10)  NOT NULL,
    home_address      VARCHAR(80)  NULL     DEFAULT NULL,
    work_address      VARCHAR(80)  NULL     DEFAULT NULL,
    icq               VARCHAR(15)  NULL     DEFAULT NULL,
    skype             VARCHAR(45)  NULL     DEFAULT NULL,
    additional_data   VARCHAR(200) NULL     DEFAULT NULL,
    administrator     BOOLEAN      NOT NULL DEFAULT FALSE,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,

    active            BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   skype, additional_data, administrator, registration_date, active)
VALUES ('belt@gmail.com', '1', 'Alex', 'Kapustsin', '25.03.1985', 'BY, Gomel, Mira 8', 'BY, Gomel, Pobedy 18',
        '106967575', 'kapustinby', 'Great guy', TRUE, '2019-06-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   additional_data, registration_date, active)
VALUES ('2@gmail.com', '2', 'Dmitry', 'Uranou', '18.11.1981', 'BY, Gomel, Centralnaya 2, kv.9',
        'BY, Gomel, Bartashova 3', '758658210', 'Great guy too', CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   skype, additional_data, registration_date, active)
VALUES ('3@gmail.com', '3', 'Alena', 'Lebedzeva', '04.06.1989', 'BY, Moscow, Pravdi 12, kv.5',
        'BY, Moscow, Akopova 8, kv.78', 'lebedzeval', 'Great girl', '2019-06-04', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   registration_date, active)
VALUES ('4@gmail.com', '4', 'Michael', 'Kapustsin', '28.05.2016', 'BY, Gomel, Mira 8', 'Great baby', '2019-05-28',
        TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   administrator, registration_date, active)
VALUES ('5@gmail.com', '5', 'Vladimir', 'Karavaev', '19.02.1965', 'BY, Zhlobin, Internacionalnaya 21',
        'BY, Zhlobin, Promishlennaya 13', TRUE, CURRENT_DATE, FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('6@gmail.com', '6', 'Олег', 'Павленок', '02.04.1985', 'Беларусь, Гомель, Бакунина 77',
        'Беларусь, Гомель, Речицкое шоссе 45', 'Предприниматель', FALSE, '2020-09-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('7@gmail.com', '7', 'Yurii', 'Korotkevich', '14.11.1982', 'BY, Minsk, Korolya 6', 'BY, Minsk, Zhlobinskaya 100',
        'Phaeton', TRUE, CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   administrator, registration_date, active)
VALUES ('8@gmail.com', '8', 'Dasha', 'Loginovskaya', '20.12.1990', 'BY, Zhlobin, Mikrorajon 16, dom 29, kv.11',
        'Dont worry', FALSE, '2020-01-02', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('9@gmail.com', '9', 'Денис', 'Быстров', '28.06.1970', 'Беларусь, Гомель, Гагарина 1',
        'Беларусь, Гомель, Елисеева 12к4', 'Системный администратор', TRUE, '2019-11-15', FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('10@gmail.com', '10', 'Test', 'Testov', '01.01.1987', 'BY, Test, Testovaya 1', 'BY, Test, Testovaya 2',
        'Test account', TRUE, CURRENT_DATE, TRUE);

CREATE TABLE socialnetwork.phone
(
    id         SERIAL      NOT NULL UNIQUE,
    account_id INTEGER     NOT NULL,
    type       VARCHAR(4)  NOT NULL,
    value      VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'cell', '+375296580636');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'work', '+375233436558');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'cell', '+375447970737');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'work', '+375233423665');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'cell', '+375291989406');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'work', '+375233458969');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (4, 'cell', '+375296975577');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'cell', '+375296214536');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'work', '+375233426613');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'cell', '+375232578156');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'work', '+375445998510');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (7, 'cell', '+375172568800');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'cell', '+375233475569');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'work', '+375296201211');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'cell', '+375232450029');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'work', '+375297771512');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'cell', '+375233422222');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'work', '+375296666666');

CREATE TABLE socialnetwork.groups
(
    id                SERIAL       NOT NULL UNIQUE,
    name              VARCHAR(70)  NOT NULL UNIQUE,
    description       VARCHAR(100) NULL,
    creator_id        INTEGER      NOT NULL,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,
    open              BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Java', 'Group about JAVA', 1, '2019-06-04', '1', '1');
INSERT INTO socialnetwork.groups (name, creator_id, registration_date, avatar_exists, open)
VALUES ('Weather in world', 1, '2019-12-12', '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, open)
VALUES ('Большие и страшные секреты', 'Группа с большими и страшными секретами', 2, CURRENT_DATE, '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Minsk news', 'Only news', 5, CURRENT_DATE, '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Test group', 'Only for tests', 10, CURRENT_DATE, '1', '1');

CREATE TABLE socialnetwork.group_members
(
    id_user  INTEGER     NOT NULL,
    id_group INTEGER     NOT NULL,
    role     VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_user, id_group)
);

INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 1, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (1, 5, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 1, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 3, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (2, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (5, 4, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 2, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (6, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 1, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 2, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 3, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (7, 4, 'moderator');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 1, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 2, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 3, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (8, 4, 'member');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (10, 5, 'owner');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (3, 1, 'request');
INSERT INTO socialnetwork.group_members (id_user, id_group, role)
VALUES (10, 1, 'request');
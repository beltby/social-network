DROP SCHEMA IF EXISTS socialnetwork cascade;

CREATE SCHEMA socialnetwork;

CREATE TABLE socialnetwork.message
(
    id            SERIAL       NOT NULL UNIQUE,
    sender_id     INTEGER      NOT NULL,
    recipient_id  INTEGER      NOT NULL,
    msg_text      VARCHAR(500) NULL     DEFAULT NULL,
    msg_scope     VARCHAR(15)  NOT NULL,
    creation_time TIMESTAMP    NOT NULL DEFAULT now(),
    photo_exists  BOOLEAN      NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_scope, creation_time)
VALUES (1, 2, 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());
INSERT INTO socialnetwork.message (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 2, 'hello', 'chat', NOW());

UPDATE socialnetwork.message
SET creation_time='2020-02-14 15:06:35.000'
WHERE id = 1;
UPDATE socialnetwork.message
SET creation_time='2020-02-14 22:06:35.000',
    msg_text='how are you?'
WHERE id = 2;
UPDATE socialnetwork.message
SET creation_time='2020-02-15 08:59:36.000',
    msg_text='hello dear!',
    sender_id=2,
    recipient_id=1
WHERE id = 3;
UPDATE socialnetwork.message
SET creation_time='2020-02-15 15:16:36.000',
    msg_text='i am fine',
    sender_id=2,
    recipient_id=1
WHERE id = 4;
UPDATE socialnetwork.message
SET msg_scope='group',
    msg_text='hello again, at the wall',
    sender_id=2,
    recipient_id=1
WHERE id = 5;
UPDATE socialnetwork.message
SET creation_time='2019-02-15 15:06:36.000',
    msg_text='hello! add me to friends',
    sender_id=3,
    recipient_id=1
WHERE id = 6;
UPDATE socialnetwork.message
SET creation_time='2019-12-15 01:06:37.000',
    msg_text='please))',
    sender_id=3,
    recipient_id=1
WHERE id = 7;
UPDATE socialnetwork.message
SET creation_time='2020-01-15 15:06:37.000',
    msg_text='no problem!',
    recipient_id=3
WHERE id = 8;
UPDATE socialnetwork.message
SET sender_id=3
WHERE id = 9;
UPDATE socialnetwork.message
SET msg_scope='chat',
    sender_id=2,
    recipient_id=5
WHERE id = 10;
UPDATE socialnetwork.message
SET msg_scope='wall',
    msg_text='nice profile',
    sender_id=2,
    recipient_id=1
WHERE id = 11;
UPDATE socialnetwork.message
SET msg_scope='group',
    msg_text='nice group',
    sender_id=2,
    recipient_id=1
WHERE id = 12;
UPDATE socialnetwork.message
SET sender_id=2,
    recipient_id=3
WHERE id = 13;
UPDATE socialnetwork.message
SET sender_id=3,
    recipient_id=6
WHERE id = 14;
UPDATE socialnetwork.message
SET sender_id=6,
    recipient_id=2
WHERE id = 15;
UPDATE socialnetwork.message
SET sender_id=6
WHERE id = 16;
UPDATE socialnetwork.message
SET msg_scope='group',
    msg_text='классная группа'
WHERE id = 17;
UPDATE socialnetwork.message
SET msg_scope='wall',
    msg_text='классный профиль'
WHERE id = 18;
UPDATE socialnetwork.message
SET msg_scope='group',
    msg_text='тоже классная группа',
    sender_id=2,
    recipient_id=1
WHERE id = 19;
UPDATE socialnetwork.message
SET msg_scope='wall',
    msg_text='тоже классный профиль',
    sender_id=2,
    recipient_id=1
WHERE id = 20;

UPDATE socialnetwork.message
SET msg_scope='wall',
    msg_text='на стену.'
WHERE id = 26;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 21;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 25;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 23;

UPDATE socialnetwork.message
SET recipient_id=5
WHERE id = 14;
UPDATE socialnetwork.message
SET recipient_id=1,
    sender_id=5
WHERE id = 15;
UPDATE socialnetwork.message
SET recipient_id=5,
    sender_id=1
WHERE id = 16;
UPDATE socialnetwork.message
SET creation_time='2020-03-12 21:40:31.000'
WHERE id = 16;
UPDATE socialnetwork.message
SET sender_id=7
WHERE id = 11;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 20;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 26;

UPDATE socialnetwork.message
SET msg_text='nice group',
    sender_id=7,
    creation_time='2020-02-15 08:59:45.000'
WHERE id = 5;


UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 12;

UPDATE socialnetwork.message
SET photo_exists= true
WHERE id = 19;

INSERT INTO socialnetwork.message
    (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 1, 'Сообщение от создателя группы', 'group', '2020-07-09 23:00:01');


INSERT INTO socialnetwork.message
    (sender_id, recipient_id, msg_text, msg_scope, creation_time)
VALUES (1, 1, 'Сообщение от владельца страницы', 'wall', '2020-07-09 23:00:01');


UPDATE socialnetwork.message
SET creation_time='2020-06-02 08:06:35.000'
WHERE id = 1;
UPDATE socialnetwork.message
SET creation_time='2020-06-02 09:11:35.000'
WHERE id = 2;
UPDATE socialnetwork.message
SET creation_time='2020-06-02 11:25:30.000'
WHERE id = 3;
UPDATE socialnetwork.message
SET creation_time='2020-06-03 12:01:28.000'
WHERE id = 4;
UPDATE socialnetwork.message
SET creation_time='2020-06-03 23:10:02.000'
WHERE id = 5;
UPDATE socialnetwork.message
SET creation_time='2020-06-04 04:12:00.000'
WHERE id = 6;
UPDATE socialnetwork.message
SET creation_time='2020-06-04 05:21:15.000'
WHERE id = 7;
UPDATE socialnetwork.message
SET creation_time='2020-06-04 17:00:15.000'
WHERE id = 8;
UPDATE socialnetwork.message
SET creation_time='2020-06-04 19:17:45.000'
WHERE id = 9;
UPDATE socialnetwork.message
SET creation_time='2020-06-04 23:01:42.000'
WHERE id = 10;
UPDATE socialnetwork.message
SET creation_time='2020-06-05 15:06:12.000'
WHERE id = 11;
UPDATE socialnetwork.message
SET creation_time='2020-06-06 02:04:41.000'
WHERE id = 12;
UPDATE socialnetwork.message
SET creation_time='2020-06-06 13:08:09.000'
WHERE id = 13;
UPDATE socialnetwork.message
SET creation_time='2020-06-06 13:10:40.000'
WHERE id = 14;
UPDATE socialnetwork.message
SET creation_time='2020-06-06 13:31:52.000'
WHERE id = 15;
UPDATE socialnetwork.message
SET creation_time='2020-06-06 15:08:02.000'
WHERE id = 16;
UPDATE socialnetwork.message
SET creation_time='2020-06-07 01:01:01.000'
WHERE id = 17;
UPDATE socialnetwork.message
SET creation_time='2020-06-07 03:08:12.000'
WHERE id = 18;
UPDATE socialnetwork.message
SET creation_time='2020-06-07 10:01:32.000'
WHERE id = 19;
UPDATE socialnetwork.message
SET creation_time='2020-06-07 11:21:32.000'
WHERE id = 20;
UPDATE socialnetwork.message
SET creation_time='2020-06-16 23:01:42.000'
WHERE id = 21;
UPDATE socialnetwork.message
SET creation_time='2020-06-16 23:22:22.000'
WHERE id = 22;
UPDATE socialnetwork.message
SET creation_time='2020-06-16 23:31:02.000'
WHERE id = 23;
UPDATE socialnetwork.message
SET creation_time='2020-06-17 20:11:06.000'
WHERE id = 24;
UPDATE socialnetwork.message
SET creation_time='2020-06-17 22:08:22.000'
WHERE id = 25;
UPDATE socialnetwork.message
SET creation_time='2020-06-17 23:00:02.000'
WHERE id = 26;
UPDATE socialnetwork.message
SET creation_time='2020-06-18 08:09:10.000'
WHERE id = 27;
UPDATE socialnetwork.message
SET creation_time='2020-06-19 12:06:33.000'
WHERE id = 28;

UPDATE socialnetwork.message
SET msg_text='в группу с фото'
WHERE id = 19;

UPDATE socialnetwork.message
SET msg_text='картинка с текстом',
    sender_id=2,
    recipient_id=1
WHERE id = 21;


CREATE TABLE socialnetwork.image_message
(
    id           SERIAL      NOT NULL UNIQUE,
    size         INTEGER     NULL DEFAULT 0,
    content_type VARCHAR(13) NULL DEFAULT NULL,
    data         bytea       NULL DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;
INSERT INTO socialnetwork.image_message DEFAULT
VALUES;



UPDATE socialnetwork.image_message
SET size=994,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_12.jpg')
WHERE id = 12;

UPDATE socialnetwork.image_message
SET size=974,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_19.jpg')
WHERE id = 19;

UPDATE socialnetwork.image_message
SET size=1019,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_20.jpg')
WHERE id = 20;

UPDATE socialnetwork.image_message
SET size=986,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_21.jpg')
WHERE id = 21;

UPDATE socialnetwork.image_message
SET size=772,
    content_type='image/png',
    data=FILE_READ('classpath:images/message_23.png')
WHERE id = 23;

UPDATE socialnetwork.image_message
SET size=952,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_25.jpg')
WHERE id = 25;

UPDATE socialnetwork.image_message
SET size=1054,
    content_type='image/jpg',
    data=FILE_READ('classpath:images/message_26.jpg')
WHERE id = 26;



CREATE TABLE socialnetwork.account
(
    id                SERIAL       NOT NULL UNIQUE,
    email             VARCHAR(45)  NOT NULL UNIQUE,
    password          VARCHAR(45)  NOT NULL,
    first_name        VARCHAR(45)  NOT NULL,
    last_name         VARCHAR(45)  NOT NULL,
    birth_date        VARCHAR(10)  NOT NULL,
    home_address      VARCHAR(80)  NULL     DEFAULT NULL,
    work_address      VARCHAR(80)  NULL     DEFAULT NULL,
    icq               VARCHAR(15)  NULL     DEFAULT NULL,
    skype             VARCHAR(45)  NULL     DEFAULT NULL,
    additional_data   VARCHAR(200) NULL     DEFAULT NULL,
    administrator     BOOLEAN      NOT NULL DEFAULT FALSE,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,

    active            BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   skype, additional_data, administrator, registration_date, active)
VALUES ('belt@gmail.com', '1', 'Alex', 'Kapustsin', '25.03.1985', 'BY, Gomel, Mira 8', 'BY, Gomel, Pobedy 18',
        '106967575', 'kapustinby', 'Great guy', TRUE, '2019-06-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address, icq,
                                   additional_data, registration_date, active)
VALUES ('2@gmail.com', '2', 'Dmitry', 'Uranou', '18.11.1981', 'BY, Gomel, Centralnaya 2, kv.9',
        'BY, Gomel, Bartashova 3', '758658210', 'Great guy too', CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   skype, additional_data, registration_date, active)
VALUES ('3@gmail.com', '3', 'Alena', 'Lebedzeva', '04.06.1989', 'BY, Moscow, Pravdi 12, kv.5',
        'BY, Moscow, Akopova 8, kv.78', 'lebedzeval', 'Great girl', '2019-06-04', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   registration_date, active)
VALUES ('4@gmail.com', '4', 'Michael', 'Kapustsin', '28.05.2016', 'BY, Gomel, Mira 8', 'Great baby', '2019-05-28',
        TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   administrator, registration_date, active)
VALUES ('5@gmail.com', '5', 'Vladimir', 'Karavaev', '19.02.1965', 'BY, Zhlobin, Internacionalnaya 21',
        'BY, Zhlobin, Promishlennaya 13', TRUE, CURRENT_DATE, FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('6@gmail.com', '6', 'Олег', 'Павленок', '02.04.1985', 'Беларусь, Гомель, Бакунина 77',
        'Беларусь, Гомель, Речицкое шоссе 45', 'Предприниматель', FALSE, '2020-09-01', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('7@gmail.com', '7', 'Yurii', 'Korotkevich', '14.11.1982', 'BY, Minsk, Korolya 6', 'BY, Minsk, Zhlobinskaya 100',
        'Phaeton', TRUE, CURRENT_DATE, TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, additional_data,
                                   administrator, registration_date, active)
VALUES ('8@gmail.com', '8', 'Dasha', 'Loginovskaya', '20.12.1990', 'BY, Zhlobin, Mikrorajon 16, dom 29, kv.11',
        'Dont worry', FALSE, '2020-01-02', TRUE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('9@gmail.com', '9', 'Денис', 'Быстров', '28.06.1970', 'Беларусь, Гомель, Гагарина 1',
        'Беларусь, Гомель, Елисеева 12к4', 'Системный администратор', TRUE, '2019-11-15', FALSE);
INSERT INTO socialnetwork.account (email, password, first_name, last_name, birth_date, home_address, work_address,
                                   additional_data, administrator, registration_date, active)
VALUES ('10@gmail.com', '10', 'Test', 'Testov', '01.01.1987', 'BY, Test, Testovaya 1', 'BY, Test, Testovaya 2',
        'Test account', TRUE, CURRENT_DATE, TRUE);

CREATE TABLE socialnetwork.groups
(
    id                SERIAL       NOT NULL UNIQUE,
    name              VARCHAR(70)  NOT NULL UNIQUE,
    description       VARCHAR(100) NULL,
    creator_id        INTEGER      NOT NULL,
    registration_date DATE         NOT NULL,
    avatar_exists     BOOLEAN      NOT NULL DEFAULT FALSE,
    open              BOOLEAN      NOT NULL DEFAULT TRUE,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Java', 'Group about JAVA', 1, '2019-06-04', '1', '1');
INSERT INTO socialnetwork.groups (name, creator_id, registration_date, avatar_exists, open)
VALUES ('Weather in world', 1, '2019-12-12', '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, open)
VALUES ('Большие и страшные секреты', 'Группа с большими и страшными секретами', 2, CURRENT_DATE, '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Minsk news', 'Only news', 5, CURRENT_DATE, '1', '1');
INSERT INTO socialnetwork.groups (name, description, creator_id, registration_date, avatar_exists, open)
VALUES ('Test group', 'Only for tests', 10, CURRENT_DATE, '1', '1');
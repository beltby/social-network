DROP SCHEMA IF EXISTS socialnetwork cascade;

CREATE SCHEMA socialnetwork;

CREATE TABLE socialnetwork.phone
(
    id         SERIAL      NOT NULL UNIQUE,
    account_id INTEGER     NOT NULL,
    type       VARCHAR(4)  NOT NULL,
    value      VARCHAR(15) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'cell', '+375296580636');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (1, 'work', '+375233436558');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'cell', '+375447970737');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (2, 'work', '+375233423665');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'cell', '+375291989406');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (3, 'work', '+375233458969');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (4, 'cell', '+375296975577');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'cell', '+375296214536');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (5, 'work', '+375233426613');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'cell', '+375232578156');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (6, 'work', '+375445998510');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (7, 'cell', '+375172568800');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'cell', '+375233475569');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (8, 'work', '+375296201211');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'cell', '+375232450029');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (9, 'work', '+375297771512');

INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'cell', '+375233422222');
INSERT INTO socialnetwork.phone (account_id, type, value)
VALUES (10, 'work', '+375296666666');
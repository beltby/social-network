package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AccountImage;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.time.LocalDate;
import java.util.List;

@XmlRootElement
@Entity
@Table(schema = "socialnetwork")
public class
Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 45, unique = true)
    private String email;
    @JsonIgnore
    @Column(nullable = false, length = 100)
    private String password;
    @Column(nullable = false, length = 45)
    private String firstName;
    @Column(nullable = false, length = 45)
    private String lastName;
    @Column(nullable = false, length = 10)
    private String birthDate;
    @JsonIgnore
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(
            schema = "socialnetwork",
            name = "phone",
            joinColumns = @JoinColumn(name = "account_id")
    )
    @OrderBy(value = "type")
    private List<Phone> phones;
    @Column(length = 80)
    private String homeAddress;
    @Column(length = 80)
    private String workAddress;
    @Column(length = 15)
    private String icq;
    @Column(length = 45)
    private String skype;
    @Column(length = 200)
    private String additionalData;
    @Column(nullable = false)
    private boolean administrator;
    @JsonIgnore
    @Column(updatable = false)
    private LocalDate registrationDate;
    @Column(nullable = false)
    private boolean avatarExists;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id")
    private AccountImage photo;
    @Column(nullable = false)
    private boolean active;

    public int getId() {
        return id;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    @XmlTransient
    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> cellPhones) {
        this.phones = cellPhones;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    @XmlTransient
    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    @XmlTransient
    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public boolean isAvatarExists() {
        return avatarExists;
    }

    @XmlTransient
    public void setAvatarExists(boolean avatarExists) {
        this.avatarExists = avatarExists;
    }

    public AccountImage getPhoto() {
        return photo;
    }

    @XmlTransient
    public void setPhoto(AccountImage photo) {
        this.photo = photo;
    }

    public boolean isActive() {
        return active;
    }

    @XmlTransient
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", phones='" + phones + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionalData='" + additionalData + '\'' +
                ", administrator=" + administrator +
                ", registrationDate=" + registrationDate +
                ", avatarExists=" + avatarExists +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return email.equals(account.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}
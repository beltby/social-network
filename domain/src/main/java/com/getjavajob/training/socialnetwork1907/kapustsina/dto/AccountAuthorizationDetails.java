package com.getjavajob.training.socialnetwork1907.kapustsina.dto;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountAuthorizationDetails implements UserDetails {
    private final String username;
    private final String password;
    private final boolean accountNonLocked;
    private final boolean isEnabled;
    private final boolean isAdministrator;

    public AccountAuthorizationDetails(Account account) {
        this.username = account.getEmail();
        this.password = account.getPassword();
        this.accountNonLocked = account.isActive();
        this.isEnabled = account.isActive();
        this.isAdministrator = account.isAdministrator();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (isAdministrator) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else {
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public String toString() {
        return "AccountAuthorizationDetails{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", accountNonLocked=" + accountNonLocked +
                ", isEnabled=" + isEnabled +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountAuthorizationDetails that = (AccountAuthorizationDetails) o;

        return getUsername().equals(that.getUsername());
    }

    @Override
    public int hashCode() {
        return getUsername().hashCode();
    }
}
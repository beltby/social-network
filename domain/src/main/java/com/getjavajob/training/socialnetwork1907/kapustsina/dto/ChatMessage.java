package com.getjavajob.training.socialnetwork1907.kapustsina.dto;

public class ChatMessage {
    private int senderId;
    private int recipientId;
    private String text;
    private String creationTime;
    private boolean imageExists;

    public ChatMessage(MessageDto messageDto) {
        this.senderId = messageDto.getSenderId();
        this.recipientId = messageDto.getRecipientId();
        this.text = messageDto.getText();
        this.imageExists = messageDto.isImageExists();
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(int recipientId) {
        this.recipientId = recipientId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public boolean isImageExists() {
        return imageExists;
    }

    public void setImageExists(boolean imageExists) {
        this.imageExists = imageExists;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "senderId=" + senderId +
                ", recipientId=" + recipientId +
                ", text='" + text + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", imageExists=" + imageExists +
                '}';
    }
}
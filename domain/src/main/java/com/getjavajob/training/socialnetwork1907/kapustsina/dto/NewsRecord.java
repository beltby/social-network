package com.getjavajob.training.socialnetwork1907.kapustsina.dto;

import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;

import java.time.LocalDateTime;

public class NewsRecord {
    private int id;
    private int senderId;
    private String sender;
    private boolean senderAvatarExists;
    private int recipientId;
    private String recipient;
    private String text;
    private boolean imageExists;
    private LocalDateTime creationTime;

    public NewsRecord(ProfilePost profilePost) {
        this.id = profilePost.getId();
        this.senderId = profilePost.getSender().getId();
        this.sender = profilePost.getSender().getFirstName() + ' ' + profilePost.getSender().getLastName();
        this.senderAvatarExists = profilePost.getSender().isAvatarExists();
        this.recipientId = profilePost.getRecipient().getId();
        this.recipient = profilePost.getRecipient().getFirstName() + ' ' + profilePost.getRecipient().getLastName();
        this.text = profilePost.getText();
        this.imageExists = profilePost.isImageExists();
        this.creationTime = profilePost.getCreationTime();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isSenderAvatarExists() {
        return senderAvatarExists;
    }

    public void setSenderAvatarExists(boolean senderAvatarExists) {
        this.senderAvatarExists = senderAvatarExists;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(int recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isImageExists() {
        return imageExists;
    }

    public void setImageExists(boolean imageExists) {
        this.imageExists = imageExists;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NewsRecord that = (NewsRecord) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public String toString() {
        return "NewsRecord{" +
                "id=" + id +
                ", senderId=" + senderId +
                ", sender='" + sender + '\'' +
                ", senderAvatarExists=" + senderAvatarExists +
                ", recipientId=" + recipientId +
                ", recipient='" + recipient + '\'' +
                ", text='" + text + '\'' +
                ", imageExists=" + imageExists +
                ", creationTime=" + creationTime +
                '}';
    }
}
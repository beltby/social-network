package com.getjavajob.training.socialnetwork1907.kapustsina.dto;

public class SearchResult {
    private int id;
    private String value;
    private String scope;

    public SearchResult(int id, String value, String scope) {
        this.id = id;
        this.value = value;
        this.scope = scope;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }
}
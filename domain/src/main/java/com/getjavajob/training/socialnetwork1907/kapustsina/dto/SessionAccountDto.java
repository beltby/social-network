package com.getjavajob.training.socialnetwork1907.kapustsina.dto;

import java.io.Serializable;

public class SessionAccountDto implements Serializable {
    private int id;
    private String email;
    private String firstName;
    private String lastName;
    private boolean administrator;
    private boolean avatarExists;
    private boolean active;

    public SessionAccountDto(int id, String email, String firstName, String lastName, boolean administrator,
            boolean avatarExists, boolean active) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.administrator = administrator;
        this.avatarExists = avatarExists;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public boolean isAvatarExists() {
        return avatarExists;
    }

    public void setAvatarExists(boolean avatarExists) {
        this.avatarExists = avatarExists;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SessionAccountDto that = (SessionAccountDto) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "SessionDTO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", administrator=" + administrator +
                ", avatarExists=" + avatarExists +
                ", active=" + active +
                '}';
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.image;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "socialnetwork", name = "image_account")
public class AccountImage extends AbstractImage {
}
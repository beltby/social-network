package com.getjavajob.training.socialnetwork1907.kapustsina.message;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.MessageImage;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Inheritance
@DiscriminatorColumn(name = "msg_scope")
@Table(schema = "socialnetwork", name = "message")
public class AbstractMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id", nullable = false)
    private Account sender;
    @Column(name = "msg_text", length = 500)
    private String text;
    @Column(insertable = false, updatable = false, name = "msg_scope", length = 15)
    private String scope;
    @Column(updatable = false)
    private LocalDateTime creationTime;
    @Column(name = "photo_exists", nullable = false)
    private boolean imageExists;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id")
    private MessageImage image;

    public AbstractMessage() {
    }

    public AbstractMessage(Account sender, String text, String scope, LocalDateTime creationTime, boolean imageExists,
            MessageImage image) {
        this.sender = sender;
        this.text = text;
        this.scope = scope;
        this.creationTime = creationTime;
        this.imageExists = imageExists;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public boolean isImageExists() {
        return imageExists;
    }

    public void setImageExists(boolean imageExists) {
        this.imageExists = imageExists;
    }

    public MessageImage getImage() {

        return image;
    }

    public void setImage(MessageImage image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender=" + sender.getId() + '\'' +
                ", id=" + id +
                ", scope='" + scope + '\'' +
                ", text='" + text + '\'' +
                ", creationTime=" + creationTime +
                ", imageExists=" + imageExists +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractMessage)) {
            return false;
        }
        AbstractMessage that = (AbstractMessage) o;
        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }
}
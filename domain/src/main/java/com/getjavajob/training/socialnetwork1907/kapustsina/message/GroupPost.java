package com.getjavajob.training.socialnetwork1907.kapustsina.message;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.MessageImage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue("group")
public class GroupPost extends AbstractMessage {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id", nullable = false)
    private Group recipient;

    public GroupPost() {
    }

    public GroupPost(Account sender, String text, String scope, LocalDateTime creationTime, boolean imageExists,
            MessageImage image, Group recipient) {
        super(sender, text, scope, creationTime, imageExists, image);
        this.recipient = recipient;
    }

    public Group getRecipient() {
        return recipient;
    }

    public void setRecipient(Group recipient) {
        this.recipient = recipient;
    }

    @Override
    public String toString() {
        return "GroupPost{" +
                "recipient group=" + recipient.getId() +
                "} " + super.toString();
    }
}
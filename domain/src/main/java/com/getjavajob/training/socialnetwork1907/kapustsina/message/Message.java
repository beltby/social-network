package com.getjavajob.training.socialnetwork1907.kapustsina.message;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.MessageImage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue("chat")
public class Message extends AbstractMessage {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id", nullable = false)
    private Account recipient;

    public Message() {
    }

    public Message(Account sender, String text, String scope, LocalDateTime creationTime, boolean imageExists,
            MessageImage image, Account recipient) {
        super(sender, text, scope, creationTime, imageExists, image);
        this.recipient = recipient;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }

    @Override
    public String toString() {
        return "Message{" +
                "account recipient=" + recipient.getId() +
                "} " + super.toString();
    }
}
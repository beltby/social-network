package com.getjavajob.training.socialnetwork1907.kapustsina.relation;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

import static java.util.Objects.hash;

@Entity
@Table(schema = "socialnetwork")
@IdClass(Friendship.FriendshipId.class)
public class Friendship implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "sender")
    private Account sender;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "recipient")
    private Account recipient;

    @Column(nullable = false, length = 15)
    private String status;

    public Friendship() {
    }

    public Friendship(Account firstId, Account secondId, String status) {
        this.sender = firstId;
        this.recipient = secondId;
        this.status = status;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "sender=" + sender.getId() +
                ", recipient=" + recipient.getId() +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Friendship)) {
            return false;
        }
        Friendship that = (Friendship) o;
        return sender.equals(that.sender) && recipient.equals(that.recipient);
    }

    @Override
    public int hashCode() {
        return hash(sender, recipient);
    }

    public static class FriendshipId implements Serializable {
        private int sender;
        private int recipient;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            FriendshipId that = (FriendshipId) o;

            if (sender != that.sender) {
                return false;
            }
            return recipient == that.recipient;
        }

        @Override
        public int hashCode() {
            int result = sender;
            result = 31 * result + recipient;
            return result;
        }
    }
}
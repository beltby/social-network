package com.getjavajob.training.socialnetwork1907.kapustsina.relation;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(schema = "socialnetwork", name = "group_members")
@IdClass(GroupMembership.GroupMembershipId.class)
public class GroupMembership implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private Account user;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_group")
    private Group group;

    @Column(nullable = false, length = 10)
    private String role;

    public GroupMembership() {
    }

    public GroupMembership(Account user, Group group, String role) {
        this.user = user;
        this.group = group;
        this.role = role;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "GroupMembership{" +
                "user=" + user +
                ", group=" + group +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GroupMembership that = (GroupMembership) o;

        if (!user.equals(that.user)) {
            return false;
        }
        if (!group.equals(that.group)) {
            return false;
        }
        return role.equals(that.role);
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + group.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }

    public static class GroupMembershipId implements Serializable {

        private int user;
        private int group;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            GroupMembershipId that = (GroupMembershipId) o;

            if (user != that.user) {
                return false;
            }
            return group == that.group;
        }

        @Override
        public int hashCode() {
            int result = user;
            result = 31 * result + group;
            return result;
        }
    }
}
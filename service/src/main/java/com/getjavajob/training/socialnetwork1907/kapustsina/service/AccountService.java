package com.getjavajob.training.socialnetwork1907.kapustsina.service;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.AccountAuthorizationDetails;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.FriendshipRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

@Service
public class AccountService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private static final Logger authLogger = LoggerFactory.getLogger("authorization");

    private final AccountRepository accountRepository;
    private final FriendshipRepository friendshipRepository;

    private final NotificationService notificationService;

    public AccountService(AccountRepository accountRepository, FriendshipRepository friendshipRepository,
            NotificationService notificationService) {
        this.accountRepository = accountRepository;
        this.friendshipRepository = friendshipRepository;
        this.notificationService = notificationService;
    }

    @Transactional
    public int create(Account account) {
        logger.debug("Create new account. Email= {}.", account.getEmail());
        return accountRepository.create(account).getId();
    }

    @Transactional(readOnly = true)
    public Account get(int id) {
        logger.debug("Get account. id = {}.", id);
        return accountRepository.get(id);
    }

    @Transactional(readOnly = true)
    public Optional<SessionAccountDto> get(String email, String password) {
        logger.debug("Attempt to authorize. email = {}.", email);
        return accountRepository.get(email, password);
    }

    @Transactional(readOnly = true)
    public Account get(String email) {
        logger.debug("Get account by email. email = {}.", email);
        return accountRepository.get(email);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        authLogger.debug("Get authorization details by email = {}.", email);
        Account account = accountRepository.get(email);
        if (isNull(account)) {
            authLogger.warn("User {} - authorization failed. User not found.", email);
            throw new UsernameNotFoundException(email);
        }
        return new AccountAuthorizationDetails(account);
    }

    @Transactional(readOnly = true)
    public Account getFullAccount(int id) {
        logger.debug("Get full account. id = {}.", id);
        return accountRepository.getWithLazyFields(id);
    }

    @Transactional
    public boolean update(Account account) {
        logger.debug("Update account. id = {}.", account.getId());
        accountRepository.update(account);
        return true;
    }

    @Transactional(readOnly = true)
    public List<Account> getAll() {
        return accountRepository.getAll();
    }

    @Transactional(readOnly = true)
    public AbstractImage getPhoto(int id) {
        logger.debug("Get photo. id = {}.", id);
        return accountRepository.getPhoto(id);
    }

    @Transactional(readOnly = true)
    public List<Account> getFriends(int userId) {
        logger.debug("Getting the list of friends of the user id = {}.", userId);
        List<Friendship> friendships = friendshipRepository.getFriends(accountRepository.get(userId));
        List<Account> friends = new ArrayList<>();
        for (Friendship friendship : friendships) {
            if (friendship.getSender().getId() == userId) {
                friends.add(friendship.getRecipient());
            } else {
                friends.add(friendship.getSender());
            }
        }
        return friends;
    }

    @Transactional(readOnly = true)
    public String getFriendshipStatus(int userId, int friendId) {
        logger.debug("Getting the friendship status between users id = {} and id = {}.", userId, friendId);
        Friendship friendship = friendshipRepository.get(accountRepository.get(userId),
                accountRepository.get(friendId));
        if (isNull(friendship)) {
            return "stranger";
        } else {
            if ("friend".equals(friendship.getStatus())) {
                return "friend";
            } else if (friendship.getSender().getId() == userId) {
                return "outgoing";
            } else {
                return "received";
            }
        }
    }

    @Transactional(readOnly = true)
    public List<Friendship> getFriendshipRequests(int id) {
        logger.debug("Getting a list of friendship requests by user id = {}.", id);
        return friendshipRepository.getFriendshipRequests(accountRepository.get(id));
    }

    @Transactional
    public void processFriendshipRequest(int userId, int friendId, String status) {
        logger.debug("Changing the friendship status between user id = {} and id = {}. Status = {}",
                userId, friendId, status);
        Friendship friendship;
        switch (status) {
            case "offer":
                Account user = accountRepository.get(userId);
                Account friend = accountRepository.get(friendId);
                friendship = new Friendship(user, friend, "sent");
                friendshipRepository.create(friendship);
                notificationService.processFriendshipNotification(user.getId(), user.getFirstName(),
                        user.getLastName(), friend.getEmail());
                break;
            case "delete":
            case "cancel":
            case "reject":
                friendship = friendshipRepository.get(accountRepository.get(userId), accountRepository.get(friendId));
                friendshipRepository.remove(friendship);
                break;
            case "accept":
                friendship = friendshipRepository.get(accountRepository.get(userId), accountRepository.get(friendId));
                friendship.setStatus("friend");
                friendshipRepository.update(friendship);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }
    }

    @Transactional
    public void remove(Account account) {
        logger.debug("Deleting an account with id = {}", account.getId());
        accountRepository.remove(account);
    }

    @Transactional(readOnly = true)
    public Page<Account> search(String query, int size, int offset, boolean showBlocked) {
        logger.debug("Searching accounts for query = {}, limit = {}, offset = {}, showBlocked = {}.", query, size,
                offset, showBlocked);
        return accountRepository.search(query, size, offset);
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.service;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;

@Service
public class CacheService {
    private static final int FEED_CACHE_MAX_SIZE = 5;
    private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private final RedisTemplate<String, Integer> redisTemplate;

    @Autowired
    public CacheService(RedisTemplate<String, Integer> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void addWallPostCache(int senderId, LocalDateTime creationTime, int postId) {
        String feedKey = "feed:" + senderId;
        redisTemplate.opsForZSet().add(feedKey, postId, getEpochMilli(creationTime));

        long feedCacheSize = ofNullable(redisTemplate.opsForZSet().zCard(feedKey)).orElse(0L);
        if (feedCacheSize > FEED_CACHE_MAX_SIZE) {
            logger.debug("Trim {}, size before = {}.", feedKey, feedCacheSize);
            trimCache(feedKey, feedCacheSize, FEED_CACHE_MAX_SIZE);
        }
    }

    public List<Integer> getWallPostCache(int userId, int page, int newsPageSize) {
        int start = newsPageSize * (page - 1);
        int end = start + newsPageSize - 1;
        return new ArrayList<>(ofNullable(redisTemplate.opsForZSet().reverseRange(getNewsKey(userId), start, end))
                .orElse(emptySet()));
    }

    public long getWallPostCacheSize(int userId) {
        return ofNullable(redisTemplate.opsForZSet().zCard(getNewsKey(userId))).orElse(0L);
    }

    public long createWallPostCache(int userId, List<Account> friends, int newsPageMaxSize) {
        String newsKey = getNewsKey(userId);
        long newsCacheSize;
        int size = friends.size();
        if (size == 0) {
            logger.debug("Number of friends = {}.", size);
            newsCacheSize = 0L;
        } else if (size == 1) {
            logger.debug("Number of friends = {}.", size);
            newsCacheSize = ofNullable(redisTemplate.opsForZSet()
                    .unionAndStore("feed:" + friends.get(0).getId(), "", newsKey)).orElse(0L);
        } else if (size == 2) {
            logger.debug("Number of friends = {}.", size);
            newsCacheSize = ofNullable(redisTemplate.opsForZSet()
                    .unionAndStore("feed:" + friends.get(0).getId(), "feed:" + friends.get(1).getId(),
                            newsKey)).orElse(0L);
        } else {
            logger.debug("Number of friends = {}.", size);
            List<String> friendKeys = new ArrayList<>();
            for (int i = 1; i < size; i++) {
                friendKeys.add("feed:" + friends.get(i).getId());
            }
            newsCacheSize = ofNullable(redisTemplate.opsForZSet()
                    .unionAndStore("feed:" + friends.get(0).getId(), friendKeys, newsKey)).orElse(0L);
        }
        if (newsCacheSize > newsPageMaxSize) {
            logger.debug("Trim {}, size before = {}.", newsKey, newsCacheSize);
            trimCache(newsKey, newsCacheSize, newsPageMaxSize);
            return newsPageMaxSize;
        }
        return newsCacheSize;
    }

    private void trimCache(String keyName, long cacheSize, int cacheMaxSize) {
        redisTemplate.opsForZSet().removeRange(keyName, 0, cacheSize - cacheMaxSize - 1);
    }

    private long getEpochMilli(LocalDateTime creationTime) {
        return creationTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    private String getNewsKey(int userId) {
        return "news:" + userId;
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.service;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupMembershipRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class GroupService {
    private static final Logger logger = LoggerFactory.getLogger(GroupService.class);

    private final GroupRepository groupRepository;
    private final AccountRepository accountRepository;
    private final GroupMembershipRepository groupMembershipRepository;

    public GroupService(AccountRepository accountRepository, GroupRepository groupRepo,
            GroupMembershipRepository groupMembershipRepository) {
        this.accountRepository = accountRepository;
        this.groupRepository = groupRepo;
        this.groupMembershipRepository = groupMembershipRepository;
    }

    @Transactional
    public int create(Group group) {
        int groupId = groupRepository.create(group).getId();
        groupMembershipRepository.create(new GroupMembership(group.getCreator(), group, "owner"));
        return groupId;
    }

    @Transactional(readOnly = true)
    public Group get(int id) {
        return groupRepository.get(id);
    }

    @Transactional(readOnly = true)
    public Group get(String name) {
        return groupRepository.get(name);
    }

    @Transactional(readOnly = true)
    public Group getGroupWithLazyFields(int id) {
        return groupRepository.getWithLazyFields(id);
    }

    @Transactional(readOnly = true)
    public List<Group> getAll() {
        return groupRepository.getAll();
    }

    @Transactional(readOnly = true)
    public AbstractImage getPhoto(int id) {
        return groupRepository.getPhoto(id);
    }

    @Transactional(readOnly = true)
    public List<Account> getIncomingGroupJoinRequests(int groupId) {
        return groupMembershipRepository.getIncomingJoiningRequests(groupRepository.get(groupId));
    }

    @Transactional(readOnly = true)
    public List<Group> getUserGroups(int userId) {
        return groupMembershipRepository.getUserGroups(accountRepository.get(userId));
    }

    @Transactional(readOnly = true)
    public List<GroupMembership> getUserGroupsWithRoles(int userId) {
        return groupMembershipRepository.getUserGroupsWithRoles(accountRepository.get(userId));
    }

    @Transactional(readOnly = true)
    public List<GroupMembership> getGroupMembershipUserRequests(int userId) {
        return groupMembershipRepository.getGroupMembershipUserRequests(accountRepository.get(userId));
    }

    @Transactional(readOnly = true)
    public List<GroupMembership> getGroupFollowers(int groupId) {
        return groupMembershipRepository.getGroupFollowers(groupRepository.get(groupId));
    }

    @Transactional(readOnly = true)
    public String getUserRole(int userId, int groupId) {
        Optional<String> role = groupMembershipRepository.getUserRole(accountRepository.get(userId), get(groupId));
        return role.orElse("stranger");
    }

    @Transactional
    public void processMembership(int userId, int groupId, String action) {
        GroupMembership groupMembership;
        switch (action) {
            case "follow":
                groupMembership = new GroupMembership(accountRepository.get(userId), groupRepository.get(groupId),
                        "request");
                groupMembershipRepository.create(groupMembership);
                break;
            case "delete":
            case "reject":
            case "unfollow":
                groupMembership = getRelationship(userId, groupId);
                groupMembershipRepository.remove(groupMembership);
                break;
            case "accept":
            case "setMember":
                groupMembership = getRelationship(userId, groupId);
                groupMembership.setRole("member");
                groupMembershipRepository.update(groupMembership);
                break;
            case "setModerator":
                groupMembership = getRelationship(userId, groupId);
                groupMembership.setRole("moderator");
                groupMembershipRepository.update(groupMembership);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + action);
        }
    }

    private GroupMembership getRelationship(int userId, int groupId) {
        return groupMembershipRepository.get(accountRepository.get(userId), groupRepository.get(groupId));
    }

    @Transactional
    public void remove(Group group) {
        groupRepository.remove(group);
    }

    @Transactional(readOnly = true)
    public Page<Group> search(String query, int size, int offset) {
        logger.debug("Searching groups for query = {}. Limit = {}, offset = {}.", query, size, offset);
        return groupRepository.search(query, size, offset);
    }

    @Transactional
    public void update(Group group) {
        groupRepository.update(group);
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.service;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.MessageDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.NewsRecord;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.MessageImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.GroupRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.MessageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.reverseOrder;
import static org.hibernate.Hibernate.initialize;

@Service
public class MessageService {
    private final MessageRepository messageRepository;
    private final AccountRepository accountRepository;
    private final GroupRepository groupRepository;
    private final CacheService cacheService;

    public MessageService(MessageRepository messageRepository, AccountRepository accountRepository,
            GroupRepository groupRepository, CacheService cacheService) {
        this.messageRepository = messageRepository;
        this.accountRepository = accountRepository;
        this.groupRepository = groupRepository;
        this.cacheService = cacheService;
    }

    @Transactional
    public void create(MessageDto messageDto) {
        switch (messageDto.getScope()) {
            case "chat":
                Message message = new Message(accountRepository.get(messageDto.getSenderId()), messageDto.getText(),
                        messageDto.getScope(), messageDto.getCreationTime(), false, new MessageImage(),
                        accountRepository.get(messageDto.getRecipientId()));
                messageRepository.create(message);
                break;
            case "wall":
                int senderId = messageDto.getSenderId();
                LocalDateTime creationTime = messageDto.getCreationTime();
                ProfilePost profilePost =
                        new ProfilePost(accountRepository.get(senderId), messageDto.getText(), messageDto.getScope(),
                                creationTime, messageDto.isImageExists(),
                                convertMultipartToImage(messageDto.getImage()),
                                accountRepository.get(messageDto.getRecipientId()));
                int postId = messageRepository.create(profilePost);
                cacheService.addWallPostCache(senderId, creationTime, postId);
                break;
            case "group":
                GroupPost groupPost = new GroupPost(accountRepository.get(messageDto.getSenderId()),
                        messageDto.getText(), messageDto.getScope(), messageDto.getCreationTime(),
                        messageDto.isImageExists(), convertMultipartToImage(messageDto.getImage()),
                        groupRepository.get(messageDto.getRecipientId()));
                messageRepository.create(groupPost);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + messageDto.getScope());
        }

    }

    @Transactional(readOnly = true)
    public AbstractImage getPhoto(int id) {
        return messageRepository.getPhoto(id);
    }

    @Transactional(readOnly = true)
    public List<Message> getLastMessages(int userId) {
        List<Message> lastMessages = messageRepository.getLastChatMessages(userId);
        for (Message lastMessage : lastMessages) {
            initialize(lastMessage.getRecipient());
            initialize(lastMessage.getSender());
        }
        return lastMessages;
    }

    @Transactional(readOnly = true)
    public List<Message> getMessages(int senderId, int recipientId) {
        return messageRepository.getChat(accountRepository.get(senderId), accountRepository.get(recipientId));
    }

    @Transactional(readOnly = true)
    public List<GroupPost> getGroupPosts(int recipientId) {
        return messageRepository.getGroupPosts(groupRepository.get(recipientId));
    }

    @Transactional(readOnly = true)
    public List<ProfilePost> getProfilePosts(int recipientId) {
        return messageRepository.getProfilePosts(accountRepository.get(recipientId));
    }

    @Transactional(readOnly = true)
    public List<Integer> getUncachedProfilePostIds(List<Account> friends, int lastCachedProfilePostId) {
        List<Integer> ids = new ArrayList<>();
        for (Account friend : friends) {
            ids.addAll(messageRepository.getUncachedProfilePostIds(friend.getId(), lastCachedProfilePostId));
        }
        ids.sort(reverseOrder());
        return ids;
    }

    @Transactional(readOnly = true)
    public List<NewsRecord> getNews(List<Integer> newsIds) {
        if (newsIds.isEmpty()) {
            return emptyList();
        } else {
            List<NewsRecord> posts = new ArrayList<>();
            for (Integer id : newsIds) {
                posts.add(new NewsRecord(messageRepository.get(id)));
            }
            return posts;
        }
    }

    private MessageImage convertMultipartToImage(MultipartFile file) {
        MessageImage image = new MessageImage();
        if (file.getSize() > 0) {
            image.setSize((int) file.getSize());
            image.setContentType(file.getContentType());
            try {
                image.setData(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return image;
    }
}
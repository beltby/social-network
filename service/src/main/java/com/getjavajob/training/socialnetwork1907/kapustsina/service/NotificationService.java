package com.getjavajob.training.socialnetwork1907.kapustsina.service;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.Notification;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class NotificationService {
    private final RabbitTemplate rabbitTemplate;

    @Value("${queue.name}")
    private String destination;

    public NotificationService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void processFriendshipNotification(int userId, String userFirstName, String userLastName,
            String friendEmail) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        String message = "User id=" + userId + ':' + userFirstName + ' ' + userLastName +
                " has sent you a friend request at " + formatter.format(date);
        String subject = "Socialnetwork notification: New friendship request from " +
                userFirstName + ' ' + userLastName;

        send(new Notification(friendEmail, subject, message));
    }

    private void send(Notification notification) {
        rabbitTemplate.convertAndSend(destination, notification);
    }
}
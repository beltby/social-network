package com.getjavajob.training.socialnetwork1907.kapustsina;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AccountImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.AccountRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.repository.FriendshipRepository;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountDaoMock;
    @Mock
    private FriendshipRepository friendshipDaoMock;
    @InjectMocks
    private AccountService accountService;
    private Account actual;

    @Before
    public void init() {
        actual = new Account();
        actual.setId(10);
        actual.setFirstName("Dmitry");
        actual.setLastName("Uranou");
        actual.setBirthDate("18.11.1981");
        actual.setHomeAddress("BY, Gomel, Centralnaya 2, kv.9");
        actual.setWorkAddress("BY, Gomel, Bartashova 3");
        actual.setEmail("flostongear@gmail.com");
        actual.setIcq("758658210");
        actual.setAdditionalData("Great guy too");
        actual.setPhoto(new AccountImage());
    }

    @After
    public void clean() {
        actual = null;
    }

    @Test
    public void get() {
        when(accountDaoMock.get("flostongear@gmail.com")).thenReturn(actual);
        assertEquals(10, accountService.get("flostongear@gmail.com").getId());
        verify(accountDaoMock).get("flostongear@gmail.com");
    }

    @Test
    public void getWithLazyFields() {
        Phone cellPhone = new Phone();
        Phone workPhone = new Phone();
        cellPhone.setType("cell");
        cellPhone.setValue("+375447970737");
        workPhone.setType("work");
        workPhone.setValue("+375233423665");
        List<Phone> phones = new ArrayList<>();
        phones.add(cellPhone);
        phones.add(workPhone);
        actual.setPhones(phones);
        actual.setPhones(phones);
        when(accountDaoMock.getWithLazyFields(10)).thenReturn(actual);
        Account fullAccount = accountService.getFullAccount(10);
        assertEquals("+375447970737", fullAccount.getPhones().get(0).getValue());
        assertEquals("work", fullAccount.getPhones().get(1).getType());
        verify(accountDaoMock).getWithLazyFields(10);
    }

    @Test
    public void getByEmail() {
        when(accountDaoMock.get(1)).thenReturn(actual);
        assertEquals("flostongear@gmail.com", accountService.get(1).getEmail());
        verify(accountDaoMock).get(1);
    }

    @Test
    public void getByAuthData() {
        SessionAccountDto dto = new SessionAccountDto(actual.getId(), actual.getEmail(), actual.getFirstName(),
                actual.getLastName(), actual.isAdministrator(), actual.isAvatarExists(), actual.isActive());
        when(accountDaoMock.get("flostongear@gmail.com", "111")).thenReturn(java.util.Optional.of(dto));
        Optional<SessionAccountDto> optionalAccountDto = accountService.get("flostongear@gmail.com", "111");
        assertTrue(optionalAccountDto.isPresent());
        assertEquals("flostongear@gmail.com", optionalAccountDto.get().getEmail());
        verify(accountDaoMock).get("flostongear@gmail.com", "111");
    }

    @Test
    public void create() {
        when(accountDaoMock.create(actual)).thenReturn(actual);
        assertEquals(10, accountService.create(actual));
        verify(accountDaoMock).create(actual);
    }

    @Test
    public void update() {
        accountService.update(actual);
        verify(accountDaoMock).update(actual);
    }

    @Test
    public void remove() {
        accountService.remove(actual);
        verify(accountDaoMock).remove(actual);
    }

    @Test
    public void getAll() {
        List<Account> accounts = new ArrayList<>();
        Account account1 = new Account();
        account1.setId(1);
        account1.setEmail("account1@gmail.com");
        account1.setFirstName("First");
        Account account2 = new Account();
        account2.setId(2);
        account2.setEmail("account2@gmail.com");
        account2.setFirstName("Second");
        Account account3 = new Account();
        account3.setId(3);
        account3.setEmail("account3@gmail.com");
        account3.setFirstName("Third");
        accountService.remove(actual);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        when(accountDaoMock.getAll()).thenReturn(accounts);
        List<Account> allAccounts = accountService.getAll();
        assertEquals(3, allAccounts.size());
        assertEquals("Second", allAccounts.get(1).getFirstName());
        verify(accountDaoMock).getAll();
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.configuration;

import com.getjavajob.training.socialnetwork1907.kapustsina.handler.AuthenticationFailureHandlerImpl;
import com.getjavajob.training.socialnetwork1907.kapustsina.handler.AuthenticationSuccessHandlerImpl;
import com.getjavajob.training.socialnetwork1907.kapustsina.handler.LogoutSuccessHandlerImpl;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
    private static final int ONE_DAY_SECONDS = 14 * 24 * 60 * 60;
    private static final String KEY = "verySecretKey";
    private static final String LOGIN_PAGE = "/auth";
    private static final String REMEMBER_ME_PARAMETER = "remember";
    private static final String USERNAME_PARAMETER = "email";

    private final AccountService accountService;
    private final AuthenticationSuccessHandlerImpl successHandler;
    private final AuthenticationFailureHandlerImpl failureHandler;
    private final LogoutSuccessHandlerImpl logoutHandler;

    @Autowired
    public Security(AccountService accountService, AuthenticationSuccessHandlerImpl successHandler,
            AuthenticationFailureHandlerImpl failureHandler, LogoutSuccessHandlerImpl logoutHandler) {
        this.accountService = accountService;
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
        this.logoutHandler = logoutHandler;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void registerGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountService);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(LOGIN_PAGE, "/registration").access("isAnonymous()")
                .antMatchers("/**").access("isAuthenticated()")
                .and();

        http.formLogin()
                .loginPage(LOGIN_PAGE)
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .loginProcessingUrl(LOGIN_PAGE)
                .usernameParameter(USERNAME_PARAMETER)
                .and();

        http.rememberMe()
                .rememberMeParameter(REMEMBER_ME_PARAMETER)
                .key(KEY)
                .tokenValiditySeconds(ONE_DAY_SECONDS)
                .authenticationSuccessHandler(successHandler)
                .and();

        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(logoutHandler);
    }
}
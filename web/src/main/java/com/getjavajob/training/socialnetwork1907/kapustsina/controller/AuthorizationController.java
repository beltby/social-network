package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthorizationController {
    @GetMapping(path = "/auth")
    public String checkAuthorization() {
        return "auth";
    }
}
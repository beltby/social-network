package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.NewsRecord;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.CacheService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

@Controller
public class CacheController {
    private static final int NEWS_PAGE_SIZE = 5;
    private static final int CACHED_NEWS_PAGE_COUNT = 2;
    private static final int NEWS_CACHE_MAX_SIZE = NEWS_PAGE_SIZE * CACHED_NEWS_PAGE_COUNT;
    private static final int FIRST_PAGE = 1;

    private final AccountService accountService;
    private final MessageService messageService;
    private final CacheService cacheService;

    public CacheController(AccountService accountService, MessageService messageService, CacheService cacheService) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.cacheService = cacheService;
    }


    @GetMapping(path = "/news")
    public ModelAndView getNews(@SessionAttribute(value = "user") SessionAccountDto user, HttpSession session,
            @RequestParam(required = false, name = "p", defaultValue = "1") int page) {
        ModelAndView modelAndView = new ModelAndView("news");
        if (page != FIRST_PAGE) {
            modelAndView.setViewName("redirect:/");
            return modelAndView;
        }

        int id = user.getId();
        int cacheSize = (int) cacheService.createWallPostCache(id, accountService.getFriends(id), NEWS_CACHE_MAX_SIZE);
        modelAndView.addObject("cacheSize", cacheSize);
        if (cacheSize > 0) {
            int remainingCacheRecords = cacheSize - NEWS_PAGE_SIZE * FIRST_PAGE;
            if (remainingCacheRecords > 0) {
                modelAndView.addObject("news",
                        messageService.getNews(cacheService.getWallPostCache(id, FIRST_PAGE, NEWS_PAGE_SIZE)));
                modelAndView.addObject("lastPage", false);
            } else if (remainingCacheRecords < 0) {
                modelAndView.addObject("news",
                        messageService.getNews(cacheService.getWallPostCache(id, FIRST_PAGE, NEWS_PAGE_SIZE)));
                modelAndView.addObject("lastPage", true);
            } else {
                List<Integer> ids = cacheService.getWallPostCache(id, FIRST_PAGE, NEWS_PAGE_SIZE);
                modelAndView.addObject("news", messageService.getNews(ids));

                if (cacheSize == NEWS_CACHE_MAX_SIZE) {
                    List<Integer> uncachedProfilePostIds =
                            messageService.getUncachedProfilePostIds(accountService.getFriends(id),
                                    ids.get(ids.size() - 1));
                    if (uncachedProfilePostIds.isEmpty()) {
                        modelAndView.addObject("lastPage", true);
                    } else {
                        session.setAttribute("uncachedProfilePostIds", uncachedProfilePostIds);
                        modelAndView.addObject("lastPage", false);
                    }
                } else {
                    modelAndView.addObject("lastPage", true);
                }
            }
        } else {
            modelAndView.addObject("news", emptyList());
        }
        modelAndView.addObject("page", FIRST_PAGE);
        return modelAndView;
    }

    @PostMapping(path = "/news")
    public ModelAndView getNewsPage(@SessionAttribute(value = "user") SessionAccountDto user, HttpSession session,
            @RequestParam(required = false, name = "p") int page) {
        ModelAndView modelAndView = new ModelAndView("news");
        int id = user.getId();
        if (isRecordsFromCache(page)) {
            int cacheSize = (int) cacheService.getWallPostCacheSize(id);
            int remainingCacheRecords = cacheSize - NEWS_PAGE_SIZE * page;
            if (remainingCacheRecords > 0) {
                modelAndView.addObject("news",
                        messageService.getNews(cacheService.getWallPostCache(id, page, NEWS_PAGE_SIZE)));
                modelAndView.addObject("lastPage", false);
            } else if (remainingCacheRecords < 0) {
                modelAndView.addObject("news",
                        messageService.getNews(cacheService.getWallPostCache(id, page, NEWS_PAGE_SIZE)));
                modelAndView.addObject("lastPage", true);
            } else {
                List<Integer> ids = cacheService.getWallPostCache(id, page, NEWS_PAGE_SIZE);
                modelAndView.addObject("news", messageService.getNews(ids));
                if (cacheSize == NEWS_CACHE_MAX_SIZE) {
                    if (isNull(session.getAttribute("uncachedProfilePostIds"))) {
                        List<Integer> uncachedProfilePostIds =
                                messageService.getUncachedProfilePostIds(accountService.getFriends(id),
                                        ids.get(ids.size() - 1));
                        if (uncachedProfilePostIds.isEmpty()) {
                            modelAndView.addObject("lastPage", true);
                        } else {
                            session.setAttribute("uncachedProfilePostIds", uncachedProfilePostIds);
                            modelAndView.addObject("lastPage", false);
                        }
                        modelAndView.addObject("lastPage", uncachedProfilePostIds.isEmpty());
                    } else {
                        modelAndView.addObject("lastPage", false);
                    }
                } else {
                    modelAndView.addObject("lastPage", true);
                }
            }
        } else {
            List<Integer> uncachedProfilePostIds =
                    (List<Integer>) session.getAttribute("uncachedProfilePostIds");
            int remainingDbRecords =
                    uncachedProfilePostIds.size() - (NEWS_PAGE_SIZE * page - NEWS_CACHE_MAX_SIZE);
            int offset = page * NEWS_PAGE_SIZE - NEWS_CACHE_MAX_SIZE - NEWS_PAGE_SIZE;

            List<NewsRecord> news;
            if (remainingDbRecords > 0) {
                news = messageService.getNews(
                        uncachedProfilePostIds.subList(offset, offset + NEWS_PAGE_SIZE));
                modelAndView.addObject("lastPage", false);
                modelAndView.addObject("news", news);
            } else {
                news = messageService.getNews(
                        uncachedProfilePostIds.subList(offset, uncachedProfilePostIds.size()));
                modelAndView.addObject("lastPage", true);
                modelAndView.addObject("news", news);
            }
        }
        modelAndView.addObject("page", page);
        return modelAndView;
    }

    boolean isRecordsFromCache(int page) {
        return NEWS_PAGE_SIZE * page <= NEWS_CACHE_MAX_SIZE;
    }
}
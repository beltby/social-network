package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.Friendship;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FriendshipController {
    private final AccountService accountService;

    public FriendshipController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(path = "/friends")
    public ModelAndView getFriendsList(
            @RequestParam(required = false, defaultValue = "0", name = "profileId") int profileId,
            @SessionAttribute(value = "user") SessionAccountDto user) {
        int id;
        boolean ownAccount;
        if (profileId == 0) {
            id = user.getId();
            ownAccount = true;
        } else {
            id = profileId;
            ownAccount = false;
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("ownAccount", ownAccount);
        List<Account> friends = accountService.getFriends(id);
        modelAndView.addObject("friends", friends);

        if (ownAccount) {
            List<Friendship> friendshipRequests = accountService.getFriendshipRequests(id);
            List<Account> receivedFriendshipRequests = new ArrayList<>();
            List<Account> sentFriendshipRequests = new ArrayList<>();

            for (Friendship friendship : friendshipRequests) {
                if (friendship.getSender().getId() == id) {
                    sentFriendshipRequests.add(friendship.getRecipient());
                } else {
                    receivedFriendshipRequests.add(friendship.getSender());
                }
            }
            modelAndView.addObject("receivedFriendshipRequests", receivedFriendshipRequests);
            modelAndView.addObject("sentFriendshipRequests", sentFriendshipRequests);
            modelAndView.setViewName("ownFriends");
        } else {
            modelAndView.addObject("profile", accountService.get(id));
            modelAndView.setViewName("friends");
        }
        return modelAndView;
    }

    @PostMapping(path = "/friendship")
    public String manageFriendshipRequests(@SessionAttribute(value = "user") SessionAccountDto user,
            HttpServletRequest req,
            @RequestParam(name = "id") int friendId,
            @RequestParam(name = "action") String action
    ) {
        int userId = user.getId();
        accountService.processFriendshipRequest(userId, friendId, action);
        return "redirect:" + getRedirectPath(req);
    }

    private String getRedirectPath(HttpServletRequest req) {
        switch (req.getServletPath()) {
            case "/friendship":
                return "/friends";
            case "/profile":
                return "/profile?id=" + req.getParameter("id");
            default:
                return "/";
        }
    }
}
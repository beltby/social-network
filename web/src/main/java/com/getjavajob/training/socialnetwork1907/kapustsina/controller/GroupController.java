package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.GroupImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.GroupPost;
import com.getjavajob.training.socialnetwork1907.kapustsina.relation.GroupMembership;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.GroupService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

import static java.time.LocalDate.now;

@Controller
public class GroupController {
    private static final int MAX_IMAGE_SIZE = 1024 * 1024 * 4;
    private static final String REDIRECT_TO_MAIN_PAGE = "redirect:/";

    private final AccountService accountService;
    private final GroupService groupService;
    private final MessageService messageService;

    public GroupController(AccountService accountService, GroupService groupService, MessageService messageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @GetMapping(path = "/groups")
    public ModelAndView getGroupsList(
            @RequestParam(required = false, defaultValue = "0", name = "profileId") int profileId,
            @SessionAttribute(value = "user") SessionAccountDto user) {
        int id;
        boolean ownAccount;
        if (profileId == 0) {
            id = user.getId();
            ownAccount = true;
        } else {
            id = profileId;
            ownAccount = false;
        }
        ModelAndView modelAndView = new ModelAndView();
        if (ownAccount) {
            List<GroupMembership> outgoingGroupMembershipRequests = groupService.getGroupMembershipUserRequests(id);
            List<GroupMembership> groupMemberships = groupService.getUserGroupsWithRoles(id);
            modelAndView.addObject("outgoingGroupMembershipRequests", outgoingGroupMembershipRequests);
            modelAndView.addObject("groupMemberships", groupMemberships);
            modelAndView.setViewName("ownGroups");
        } else {
            try {
                Account profile = accountService.get(id);
                modelAndView.addObject("profile", profile);
            } catch (RuntimeException e) {
                modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
                return modelAndView;
            }
            modelAndView.addObject("groups", groupService.getUserGroups(id));
            modelAndView.setViewName("groups");
        }
        return modelAndView;
    }

    @GetMapping(path = "/group-management")
    public ModelAndView getGroupData(
            @SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam(required = false, defaultValue = "0", name = "id") int id) {
        ModelAndView modelAndView = new ModelAndView("group-parameters");
        if (id > 0) {
            Group group = groupService.getGroupWithLazyFields(id);
            if (group != null && group.getCreator().getId() == user.getId()) {
                modelAndView.addObject("group", group);
            } else {
                modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
            }
        } else if (id < 0) {
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    @GetMapping(path = "/group")
    public ModelAndView viewGroup(@RequestParam(name = "id") int groupId,
            @SessionAttribute(value = "user") SessionAccountDto user) {
        Group group = groupService.getGroupWithLazyFields(groupId);
        ModelAndView modelAndView = new ModelAndView();
        if (group != null) {
            String creator = group.getCreator().getFirstName() + ' ' + group.getCreator().getLastName();
            String role = groupService.getUserRole(user.getId(), groupId);
            modelAndView.addObject("group", group);
            modelAndView.addObject("creator", creator);
            modelAndView.addObject("role", role);
            if (!"stranger".equals(role)) {
                List<GroupPost> posts = messageService.getGroupPosts(groupId);
                modelAndView.addObject("posts", posts);
            }
            modelAndView.setViewName("group");
        } else {
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    @PostMapping(path = "/group-create")
    public String createGroup(@ModelAttribute Group group, @RequestParam MultipartFile image,
            @SessionAttribute(value = "user") SessionAccountDto user) throws IOException {
        GroupImage avatar = new GroupImage();
        if (image.getSize() > 0) {
            avatar.setSize((int) image.getSize());
            avatar.setContentType(image.getContentType());
            avatar.setData(image.getBytes());
            group.setAvatarExists(true);
        }
        group.setPhoto(avatar);

        group.setOpen(true);
        group.setRegistrationDate(now());
        group.setCreator(accountService.get(user.getId()));

        if (groupService.get(group.getName()) == null) {
            int id = groupService.create(group);
            return "redirect:/group?id=" + id;
        }
        return "redirect:/group-management?id=0";
    }

    @PostMapping(path = "/group-update")
    public String updateGroup(@ModelAttribute Group newData,
            @SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam MultipartFile image,
            @RequestParam(name = "id") int id) throws IOException {
        Group editableGroup = groupService.get(newData.getId());
        if (editableGroup != null && editableGroup.getCreator().getId() == user.getId()) {
            editableGroup.setName(newData.getName());
            editableGroup.setDescription(newData.getDescription());

            if (image.getSize() > 0 && image.getSize() <= MAX_IMAGE_SIZE) {
                GroupImage avatar = new GroupImage();
                avatar.setSize((int) image.getSize());
                avatar.setContentType(image.getContentType());
                avatar.setData(image.getBytes());
                avatar.setId(editableGroup.getId());
                editableGroup.setPhoto(avatar);
                editableGroup.setAvatarExists(true);
            }

            groupService.update(editableGroup);
            return "redirect:/group?id=" + id;
        } else {
            return REDIRECT_TO_MAIN_PAGE;
        }
    }

    @GetMapping(path = "/followers")
    public ModelAndView getGroupFollowersList(
            @RequestParam(name = "groupId") int groupId,
            @SessionAttribute(value = "user") SessionAccountDto user) {
        ModelAndView modelAndView = new ModelAndView();
        String userRole = groupService.getUserRole(user.getId(), groupId);
        if (!"stranger".equals(userRole) && !"request".equals(userRole)) {
            Group group = groupService.get(groupId);
            List<GroupMembership> followers = groupService.getGroupFollowers(groupId);

            if ("owner".equals(userRole) || "moderator".equals(userRole)) {
                List<Account> incomingGroupJoinRequests = groupService.getIncomingGroupJoinRequests(groupId);
                modelAndView.addObject("incomingGroupJoinRequests", incomingGroupJoinRequests);
            }
            modelAndView.addObject("userRole", userRole);
            modelAndView.addObject("group", group);
            modelAndView.addObject("followers", followers);
            modelAndView.setViewName("followers");
        } else {
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    @PostMapping(path = "/group-membership")
    public String manageFollower(@RequestParam(name = "userId") int userId, @RequestParam(name = "groupId") int groupId,
            @RequestParam(name = "action") String action,
            @RequestParam(name = "redirectURL") String redirectURL) {
        groupService.processMembership(userId, groupId, action);
        return "redirect:" + redirectURL;
    }
}
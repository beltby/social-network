package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.image.AbstractImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.GroupService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.MessageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ImageController {
    private final AccountService accountService;
    private final GroupService groupService;
    private final MessageService messageService;

    public ImageController(AccountService accountService, GroupService groupService, MessageService messageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @GetMapping(path = "/images/uploaded/**")
    public void getImage(@RequestParam("imageScope") String imageScope, @RequestParam("id") int id,
            HttpServletResponse resp) {

        AbstractImage photo;
        switch (imageScope) {
            case "accountAvatar":
                photo = accountService.getPhoto(id);
                break;
            case "groupAvatar":
                photo = groupService.getPhoto(id);
                break;
            case "attachedImage":
                photo = messageService.getPhoto(id);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + imageScope);
        }
        resp.setContentType(photo.getContentType());
        resp.setContentLength(photo.getSize());
        try (ServletOutputStream outputStream = resp.getOutputStream()) {
            outputStream.write(photo.getData());
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
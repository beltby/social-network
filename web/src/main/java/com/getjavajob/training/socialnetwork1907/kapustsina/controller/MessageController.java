package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.ChatMessage;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.MessageDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.Message;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.MessageService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;

@Controller
public class MessageController {
    private static final DateTimeFormatter formatter = ofPattern("dd-MM-yyyy HH:mm");

    private final AccountService accountService;
    private final MessageService messageService;

    public MessageController(AccountService accountService, MessageService messageService) {
        this.accountService = accountService;
        this.messageService = messageService;
    }

    @GetMapping(path = "/messages")
    public ModelAndView getLastMessages(@SessionAttribute(value = "user") SessionAccountDto user) {
        int id = user.getId();
        List<Message> lastMessages = messageService.getLastMessages(id);
        ModelAndView modelAndView = new ModelAndView("messages");
        modelAndView.addObject("lastMessages", lastMessages);
        return modelAndView;
    }

    @GetMapping(path = "/chat")
    public ModelAndView getChat(@SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam(name = "profileId") int profileId) {
        List<Message> messages = messageService.getMessages(user.getId(), profileId);
        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("interlocutor", accountService.get(profileId));
        return modelAndView;
    }

    @MessageMapping("/chat")
    @SendTo("/topic/messages")
    public ChatMessage send(@ModelAttribute MessageDto message) {
        message.setCreationTime(now());
        message.setScope("chat");
        messageService.create(message);
        ChatMessage chatMessage = new ChatMessage(message);
        chatMessage.setCreationTime(message.getCreationTime().format(formatter));
        return chatMessage;
    }

    @PostMapping(path = {"/post"})
    public String createPost(@ModelAttribute MessageDto message) {
        if (validateMessage(message.getText(), message.getImage())) {
            if (message.getImage().getSize() > 0) {
                message.setImageExists(true);
            }
            message.setCreationTime(now());
            messageService.create(message);
        }
        return getRedirectPath(message.getRecipientId(), message.getScope());
    }

    private boolean validateMessage(String text, MultipartFile image) {
        return text.length() != 0 || !image.isEmpty();
    }

    private String getRedirectPath(int recipientId, String scope) {
        StringBuilder forwardPath = new StringBuilder("redirect:/");
        switch (scope) {
            case "chat":
                forwardPath.append("chat?profileId=");
                break;
            case "wall":
                forwardPath.append("profile?id=");
                break;
            case "group":
                forwardPath.append("group?id=");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + scope);
        }
        forwardPath.append(recipientId);
        return forwardPath.toString();
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Phone;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto;
import com.getjavajob.training.socialnetwork1907.kapustsina.image.AccountImage;
import com.getjavajob.training.socialnetwork1907.kapustsina.message.ProfilePost;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.lang.Boolean.TRUE;
import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.time.LocalDate.now;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;

@Controller
public class ProfileController {
    private static final int MAX_IMAGE_SIZE = 1024 * 1024 * 4;
    private static final String REDIRECT_TO_MAIN_PAGE = "redirect:/";
    private static final String REDIRECT_TO_REGISTRATION_FAILED = "redirect:/auth?regFailed=true";
    private static final String REDIRECT_TO_REGISTRATION_SUCCESS = "redirect:/auth?regSuccess=true";

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
    private static final JAXBContext jaxbContext = createContextForAccount();
    private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private final AccountService accountService;
    private final MessageService messageService;

    public ProfileController(AccountService accountService, MessageService messageService) {
        this.accountService = accountService;
        this.messageService = messageService;
    }

    private static JAXBContext createContextForAccount() {
        try {
            return JAXBContext.newInstance(Account.class);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping(path = {"/profile", "/"})
    public ModelAndView showProfile(@SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam(required = false, defaultValue = "0", name = "id") int id) {
        if (id <= 0) {
            logger.info("User id = {} view own page.", user.getId());
        } else {
            logger.info("User id = {} view profile of user id = {}", user.getId(), id);
        }
        int userId = user.getId();
        int profileId;
        if (id == 0) {
            profileId = userId;
        } else {
            profileId = id;
        }
        Account profile = accountService.getFullAccount(profileId);
        ModelAndView modelAndView = new ModelAndView();
        if (profile != null) {
            modelAndView.addObject("profile", profile);
            List<ProfilePost> posts = messageService.getProfilePosts(profile.getId());
            modelAndView.addObject("posts", posts);
            if (userId == profileId) {
                modelAndView.setViewName("index");
            } else {
                modelAndView.addObject("friendshipStatus", accountService.getFriendshipStatus(userId, profileId));
                modelAndView.setViewName("profile");
            }
        } else {
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    @GetMapping(path = "/registration")
    public String getProfileRegistrationForm() {
        logger.info("Request for a new user registration page.");
        return "registration";
    }

    @PostMapping(path = "/registration")
    public String createProfile(@ModelAttribute Account user, @RequestParam MultipartFile image) throws IOException {
        AccountImage avatar = new AccountImage();
        if (image.getSize() > 0 && image.getSize() <= MAX_IMAGE_SIZE) {
            avatar.setSize((int) image.getSize());
            avatar.setContentType(image.getContentType());
            avatar.setData(image.getBytes());
            user.setAvatarExists(true);
        }
        user.setPhoto(avatar);

        user.setRegistrationDate(now());
        user.setActive(true);
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setAdministrator(false);
        if (accountService.get(user.getEmail()) == null) {
            int newUserId = accountService.create(user);
            logger.info("User with id = {} has been successfully registered.", newUserId);
            return REDIRECT_TO_REGISTRATION_SUCCESS;
        } else {
            logger.info("New user registration failed. Used email : {}", user.getEmail());
            return REDIRECT_TO_REGISTRATION_FAILED;
        }
    }

    @GetMapping(path = "/edit")
    public ModelAndView getProfileUpdateForm(
            @RequestParam(required = false, defaultValue = "0", name = "profileId") int profileId,
            @SessionAttribute(value = "user") SessionAccountDto user) {
        if (profileId == 0) {
            logger.info("Request from user id = {} to change the own profile.", user.getId());
        } else {
            logger.info("Request from administrator id = {} to change the profile of user id = {}", user.getId(),
                    profileId);
        }
        Account editableUser = null;
        if (user.isAdministrator() || profileId == 0 || user.getId() == profileId) {
            if (profileId > 0) {
                editableUser = accountService.getFullAccount(profileId);
            } else {
                editableUser = accountService.getFullAccount(user.getId());
            }
        }

        ModelAndView modelAndView = new ModelAndView();
        if (editableUser != null) {
            modelAndView.addObject("editableUser", editableUser);
            modelAndView.setViewName("profile-update");
        } else {
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    @PostMapping(path = "/edit")
    public String updateProfile(@ModelAttribute Account newData,
            @SessionAttribute(value = "user") SessionAccountDto user, @RequestParam MultipartFile image,
            HttpSession session) throws IOException {

        boolean ownProfile = user.getId() == newData.getId();
        if (ownProfile || user.isAdministrator()) {
            Account editableUser = accountService.get(newData.getId());
            removeEmptyPhones(newData.getPhones());
            newData.setActive(editableUser.isActive());
            newData.setAdministrator(editableUser.isAdministrator());
            newData.setAvatarExists(editableUser.isAvatarExists());

            if (image.getSize() > 0 && image.getSize() <= MAX_IMAGE_SIZE) {
                AccountImage avatar = new AccountImage();
                avatar.setSize((int) image.getSize());
                avatar.setContentType(image.getContentType());
                avatar.setData(image.getBytes());
                avatar.setId(newData.getId());
                newData.setPhoto(avatar);
                newData.setAvatarExists(true);
            }

            if (accountService.update(newData)) {
                session.setAttribute("updateResult", true);
                if (ownProfile) {
                    newData.setPhoto(null);
                    Optional<SessionAccountDto> optionalSessionAccountDto = accountService.get(newData.getEmail(),
                            newData.getPassword());
                    if (optionalSessionAccountDto.isPresent()) {
                        session.setAttribute("user", optionalSessionAccountDto.get());
                        logger.info("User id = {} successfully updated their profile.", user.getId());
                    } else {
                        logger.warn("User id = {} update session DTO failed. Logout.", user.getId());
                        return "redirect:logout";
                    }
                } else {
                    logger.info("Administrator id = {} successfully updated profile id = {}.", user.getId(),
                            newData.getId());
                }
            } else {
                session.setAttribute("updateResult", false);
                logger.info("Profile id = {} update failed. The process was executed by a user with id = {}.",
                        newData.getId(), user.getId());
            }
        }
        return "redirect:profile?id=" + newData.getId();
    }

    private void removeEmptyPhones(List<Phone> phones) {
        phones.removeIf(phone -> phone.getType() == null);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping(path = "/profile")
    public String editProfileStatus(HttpSession session, @RequestParam(name = "id") int id,
            @SessionAttribute(value = "user") SessionAccountDto user, @RequestParam(name = "action") String action) {
        Account editableUser = accountService.get(id);
        switch (action) {
            case "setAdministrator":
                editableUser.setAdministrator(true);
                logger.info("Administrator with id = {} assigned user with id = {} as administrator.", user.getId(),
                        id);
                break;
            case "changeActivity":
                editableUser.setActive(!editableUser.isActive());
                logger.info("Administrator with id = {} change activity of user with id = {}. New status: active = {}",
                        user.getId(), id, editableUser.isActive());
                break;
            default:
                break;
        }
        session.setAttribute("updateResult", accountService.update(editableUser));
        return "redirect:/profile?id=" + id;
    }

    @GetMapping(path = "/exportToXml")
    public void exportAccountDataToXml(HttpServletResponse response, HttpServletRequest request,
            @SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam(name = "id") int id) throws IOException, JAXBException {
        if (user.isAdministrator() || user.getId() == id) {
            logger.info("Exporting an account id = {} to xml file. Request from user with id = {}", id, user.getId());
            response.setContentType("text/xml");
            response.setHeader("Content-disposition", "attachment; filename=account_" + id + ".xml");
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(JAXB_FORMATTED_OUTPUT, TRUE);
            String pathname;
            if ("false".equals(System.getenv("IS_REMOTE"))) {
                pathname = System.getenv("XML_PATH") + "/account_" + id + ".xml";
            } else {
                pathname = "account_" + id + ".xml";
            }
            File file = new File(pathname);
            marshaller.marshal(accountService.getFullAccount(id), file);
            try (OutputStream outputStream = response.getOutputStream()) {
                Path path = get(pathname);
                copy(path, outputStream);
                outputStream.flush();
            } catch (IOException e) {
                logger.info("XML export failed. Request from user {}. Profile id = {}. Error message - {} ", id,
                        user.getId(), e.getMessage());
            }
        } else {
            logger.warn(
                    "Unauthorized attempt to export a user with id = {} to xml file. Request from user with id = {}",
                    id, user.getId());
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
        }
    }

    @PostMapping(path = "/importXml")
    public ModelAndView importAccountDataFromXml(@RequestParam("xmlFile") MultipartFile xmlFile,
            @SessionAttribute(value = "user") SessionAccountDto user,
            @RequestParam(name = "id") int id) throws JAXBException, IOException {
        ModelAndView modelAndView = new ModelAndView("profile-update");
        if (user.isAdministrator() || user.getId() == id) {
            logger.info("Importing an account id = {} from xml file. Request from user with id = {}", id, user.getId());
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            InputStream inputStream = new ByteArrayInputStream(xmlFile.getBytes());
            Account newData = (Account) unmarshaller.unmarshal(inputStream);
            Account editable = accountService.getFullAccount(id);
            if (editable.getId() == newData.getId()) {
                processNewData(newData, editable);
                modelAndView.addObject("editableUser", editable);
                modelAndView.setViewName("profile-update");
            } else {
                logger.warn(
                        "Profile update failed. XML file wrong-id={} in XML does not match the requested user id = {}.",
                        newData.getId(), editable.getId());
                modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
            }

        } else {
            logger.warn("Unauthorized attempt to import account id = {} from xml file. Request from user with id = {}",
                    id, user.getId());
            modelAndView.setViewName(REDIRECT_TO_MAIN_PAGE);
        }
        return modelAndView;
    }

    private void processNewData(Account newData, Account editable) {
        if (!editable.getEmail().equals(newData.getEmail())) {
            if (newData.getEmail() != null && accountService.get(newData.getEmail()) == null) {
                editable.setEmail(newData.getEmail());
                logger.info("XML import. Profile id = {}. Email update successfully. New email = {}.", editable.getId(),
                        newData.getEmail());
            } else {
                logger.warn(
                        "Profile id = {}. Email update failed. This email is in use or the file is damaged. Email = {}",
                        editable.getId(), newData.getEmail());
            }
        }

        if (newData.getFirstName() != null && (!Objects.equals(editable.getFirstName(), newData.getFirstName()))) {
            editable.setFirstName(newData.getFirstName());
            logger.info("XML import. Profile id = {}. First name update successfully. New first name = {}.",
                    editable.getId(), newData.getFirstName());
        }

        if (newData.getLastName() != null && (!Objects.equals(editable.getLastName(), newData.getLastName()))) {
            editable.setLastName(newData.getLastName());
            logger.info("XML import. Profile id = {}. Last name update successfully. New last name = {}.",
                    editable.getId(), newData.getLastName());
        }

        if (newData.getBirthDate() != null && (!Objects.equals(editable.getBirthDate(), newData.getBirthDate()))) {
            editable.setBirthDate(newData.getBirthDate());
            logger.info("XML import. Profile id = {}. Birth date update successfully. New birth date = {}.",
                    editable.getId(), newData.getBirthDate());
        }

        if (newData.getPhones() != null && !newData.getPhones().isEmpty() && !newData.getPhones().containsAll(
                editable.getPhones())) {
            editable.setPhones(newData.getPhones());
            logger.info("XML import. Profile id = {}. Phones update successfully.", editable.getId());
        }

        if (newData.getHomeAddress() != null && (!Objects.equals(editable.getHomeAddress(),
                newData.getHomeAddress()))) {
            editable.setHomeAddress(newData.getHomeAddress());
            logger.info("XML import. Profile id = {}. Home address update successfully. New home address = {}.",
                    editable.getId(), newData.getHomeAddress());
        }

        if (newData.getWorkAddress() != null && (!Objects.equals(editable.getWorkAddress(),
                newData.getWorkAddress()))) {
            editable.setWorkAddress(newData.getWorkAddress());
            logger.info("XML import. Profile id = {}. Work address update successfully. New work address = {}.",
                    editable.getId(), newData.getWorkAddress());
        }

        if (newData.getIcq() != null && (!Objects.equals(editable.getIcq(), newData.getIcq()))) {
            editable.setIcq(newData.getIcq());
            logger.info("XML import. Profile id = {}. ICQ update successfully. New ICQ = {}.", editable.getId(),
                    newData.getIcq());
        }

        if (newData.getSkype() != null && (!Objects.equals(editable.getSkype(), newData.getSkype()))) {
            editable.setSkype(newData.getSkype());
            logger.info("XML import. Profile id = {}. Skype update successfully. New skype = {}.", editable.getId(),
                    newData.getIcq());
        }

        if (newData.getAdditionalData() != null && (!Objects.equals(editable.getAdditionalData(),
                newData.getAdditionalData()))) {
            editable.setAdditionalData(newData.getAdditionalData());
            logger.info("XML import. Profile id = {}. Additional data update successfully. New additional data = {}.",
                    editable.getId(), newData.getAdditionalData());
        }
    }
}
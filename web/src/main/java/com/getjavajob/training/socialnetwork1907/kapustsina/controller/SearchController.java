package com.getjavajob.training.socialnetwork1907.kapustsina.controller;

import com.getjavajob.training.socialnetwork1907.kapustsina.Account;
import com.getjavajob.training.socialnetwork1907.kapustsina.Group;
import com.getjavajob.training.socialnetwork1907.kapustsina.dto.SearchResult;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.GroupService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {
    private static final int SEARCH_LIMIT = 2;

    private final AccountService accountService;
    private final GroupService groupService;

    public SearchController(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @GetMapping(path = "/search")
    public ModelAndView search(@RequestParam(name = "q") String query,
            @RequestParam(name = "showBlocked", required = false) String showBlocked,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page
    ) {
        Page<Account> accounts = accountService.search(query, SEARCH_LIMIT, page, showBlocked != null);
        Page<Group> groups = groupService.search(query, SEARCH_LIMIT, page);
        boolean lastPage = !accounts.hasNext() && !groups.hasNext();

        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("accounts", accounts.getContent());
        modelAndView.addObject("groups", groups.getContent());
        modelAndView.addObject("accountsLastPage", accounts.getTotalPages() - 1);
        modelAndView.addObject("groupsLastPage", groups.getTotalPages() - 1);
        modelAndView.addObject("page", page);
        modelAndView.addObject("q", query);
        modelAndView.addObject("lastPage", lastPage);
        modelAndView.addObject("limit", SEARCH_LIMIT);
        return modelAndView;
    }

    @GetMapping(path = "/searchAccountPaginated")
    @ResponseBody
    public List<Account> searchAccountPaginated(@RequestParam(name = "q") String query,
            @RequestParam(name = "page") int page) {
        return accountService.search(query, SEARCH_LIMIT, page, false).getContent();
    }

    @GetMapping(path = "/searchGroupPaginated")
    @ResponseBody
    public List<Group> searchGroupPaginated(@RequestParam(name = "q") String query,
            @RequestParam(name = "page") int page) {
        return groupService.search(query, SEARCH_LIMIT, page).getContent();
    }

    @GetMapping(path = "/searchAjax")
    @ResponseBody
    public List<SearchResult> searchAjax(@RequestParam(name = "q") String query,
            @RequestParam(name = "showBlocked", required = false) String showBlocked
    ) {
        List<SearchResult> results = new ArrayList<>();
        Page<Account> accounts = accountService.search(query, SEARCH_LIMIT + 3, 0, showBlocked != null);
        if (!accounts.isEmpty()) {
            for (Account account : accounts) {
                results.add(new SearchResult(account.getId(), account.getFirstName() + " " + account.getLastName(),
                        "profile"));
            }
        }
        Page<Group> groups = groupService.search(query, SEARCH_LIMIT + 3, 0);
        if (!groups.isEmpty()) {
            for (Group group : groups) {
                results.add(new SearchResult(group.getId(), group.getName(), "group"));
            }
        }
        return results;
    }
}
package com.getjavajob.training.socialnetwork1907.kapustsina.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {
    private static final String PARAM_EMAIL = "email";
    private static final Logger authLogger = LoggerFactory.getLogger("authorization");

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException {
        if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
            authLogger.warn("User {} - authorization failed. Incorrect auth data.", request.getParameter(PARAM_EMAIL));
            response.sendRedirect("/auth?credentialsError=true");
        } else if (exception.getClass().isAssignableFrom(LockedException.class) ||
                exception.getClass().isAssignableFrom(DisabledException.class)) {
            authLogger.warn("User {} - authorization failed. Account is blocked.", request.getParameter(PARAM_EMAIL));
            response.sendRedirect("/auth?blockedError=true");
        } else {
            authLogger.warn("User {} - authorization failed. Other reason", request.getParameter(PARAM_EMAIL));
            response.sendRedirect("/auth?error=true");
        }
    }
}
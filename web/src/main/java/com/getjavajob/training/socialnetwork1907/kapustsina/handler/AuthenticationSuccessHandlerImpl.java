package com.getjavajob.training.socialnetwork1907.kapustsina.handler;

import com.getjavajob.training.socialnetwork1907.kapustsina.dto.AccountAuthorizationDetails;
import com.getjavajob.training.socialnetwork1907.kapustsina.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
    private static final Logger authLogger = LoggerFactory.getLogger("authorization");

    private final AccountService accountService;

    public AuthenticationSuccessHandlerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            Authentication authentication) throws IOException {
        AccountAuthorizationDetails authorizationDetails = (AccountAuthorizationDetails) authentication.getPrincipal();
        httpServletRequest.getSession().setAttribute("user", accountService.get(authorizationDetails.getUsername(),
                authorizationDetails.getPassword()).orElse(null));
        authLogger.info("User {} - successful authorization.  ", authorizationDetails.getUsername());
        httpServletResponse.sendRedirect("/");
    }
}
<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
	      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<title><spring:message code="auth.title"/></title>
</head>

<body style="background-color: #F9F9F9">
<div class="text-center">
	<c:choose>
		<c:when test="${param.error==null&&param.credentialsError==null&&param.blockedError==null&&param.logout==null&&
		param.regSuccess==null&&param.regFailed==null}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#000000;"><spring:message
					code="auth.message"/></p></div>
		</c:when>
		<c:when test="${param.credentialsError!=null}">
			<div><p class="float-none h5 mb-4 pt-3 font-weight-bold" style="color:#ff0000;"><spring:message
					code="auth.message.err.credentials"/></p></div>
		</c:when>
		<c:when test="${param.blockedError!=null}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#ff0000;"><spring:message
					code="auth.message.err.blocked"/></p></div>
		</c:when>
		<c:when test="${param.error!=null}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#ff0000;"><spring:message
					code="auth.message.err.other"/></p></div>
		</c:when>
		<c:when test="${param.regSuccess == true}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#00bc12;"><spring:message
					code="auth.message.registration.done"/></p>
			</div>
		</c:when>
		<c:when test="${param.regFailed == true}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#ff0000;"><spring:message
					code="auth.message.registration.failed"/></p>
			</div>
		</c:when>
		<c:when test="${param.logout!=null}">
			<div><p class="h5 mb-4 pt-3 font-weight-bold" style="color:#ff0000;"><spring:message
					code="auth.message.logout"/></p></div>
		</c:when>
	</c:choose>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-4 offset-md-4 align-self-center">
			<form class="form-signin" method="post">
				<label for="email" class="sr-only">Email address</label>
				<input type="email" id="email" name="email" class="form-control" placeholder="<spring:message
						code="auth.form.placeholder.email"/>" required autofocus>
				<label for="password" class="sr-only">Password</label>
				<input type="password" id="password" name="password" class="form-control mt-3" placeholder="<spring:message
						code="auth.form.placeholder.password"/>" required>
				<div class="checkbox mb-2">
					<label>
						<input type="checkbox" name="remember" class="mt-2" value="true" checked>${' '}<spring:message
							code="auth.form.checkbox.rememberMe"/><br>
					</label>
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message
						code="auth.form.button.signIn"/></button>
				<spring:message code="auth.form.button.registration" var="registration"/>
				<label class="h5"><a href="${pageContext.request.contextPath}registration">${registration}</a></label>

				<spring:message code="main.language.current" var="currentLanguage"/>
				<c:choose>
					<c:when test="${currentLanguage.equals('ru')}">
						<c:set var="switchLangKey" scope="page" value="?lang=en"/>
					</c:when>
					<c:when test="${currentLanguage.equals('en')}">
						<c:set var="switchLangKey" scope="page" value="?lang=ru"/>
					</c:when></c:choose>

				<spring:message code="auth.switch.language" var="language"/>
				<label class="h5 float-right"><a
						href="${pageContext.request.contextPath}${switchLangKey}">${language}</a></label>
			</form>
		</div>
	</div>
</div>
</body>
</html>
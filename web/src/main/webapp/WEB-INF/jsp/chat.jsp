<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="chat.title"/></title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<jsp:useBean id="messages" scope="request" type="java.util.List"/>
<jsp:useBean id="interlocutor" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>
<c:set var="active" scope="page" value="${interlocutor.active}"/>
<body onload="jumpToForm()">
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
		<div class="col-md-6">
			<br>
			<table class="table result-table" id="resultTable">
				<caption><strong><spring:message
						code="chat.table.title"/>${' '}${interlocutor.firstName}${' '}${interlocutor.lastName}</strong>
				</caption>
				<tr>
					<th><spring:message code="chat.table.column.sender"/></th>
					<th><spring:message code="chat.table.column.message"/></th>
					<th><spring:message code="chat.table.column.dateAndTime"/></th>
				</tr>

				<c:forEach items="${messages}" var="message" varStatus="status">
					<c:if test="${message.imageExists}">
						<c:set var="imageURL" scope="page"
						       value="/images/uploaded?imageScope=attachedImage&id=${message.id}"/>
					</c:if>

					<c:choose>
						<c:when test="${message.sender.id==user.id}">
							<c:set var="senderFirstName" scope="page" value="${user.firstName}"/>
							<c:set var="senderLastName" scope="page" value="${user.lastName}"/>
							<c:set var="senderId" scope="page" value="${user.id}"/>
						</c:when>

						<c:otherwise>
							<c:set var="senderFirstName" scope="page" value="${interlocutor.firstName}"/>
							<c:set var="senderLastName" scope="page" value="${interlocutor.lastName}"/>
							<c:set var="senderId" scope="page" value="${interlocutor.id}"/>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${message.sender.id==user.id&&user.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${user.id}"/>
						</c:when>
						<c:when test="${message.sender.id==interlocutor.id&&interlocutor.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${interlocutor.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr id="message">
						<td id="chat-photo"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                         alt="Sender photo"
						                         width="60"> <br/>
							<a href="${pageContext.request.contextPath}profile?id=${senderId}">${senderFirstName}${' '} ${senderLastName}</a>
						</td>
						<td id="chat-left-align-1">
							<c:if test="${message.imageExists}">
								<img src="${pageContext.request.contextPath} /images/uploaded?imageScope=attachedImage&id=${message.id}"
								     alt="Attached image" width="100"
								     onclick="window.open('/images/uploaded?imageScope=attachedImage&id='+${message.id}, '_blank');">
							</c:if>
							<br/> ${message.text}
						</td>
						<td id="chat-center-align-1">
							<fmt:parseDate value="${message.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
							               var="parsedDateTime"
							               type="both"/>
							<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
						</td>
					</tr>
				</c:forEach>
			</table>
			<p id="response"></p> <br/>
			<c:if test="${active}">
				<a id="form"></a>
				<%--<form method="post" action="${pageContext.request.contextPath}/chat" enctype="multipart/form-data">--%>
				<p><label for="text"></label>
					<textarea name="text" id="text" cols="48" rows="8"></textarea>
				</p>
				<script type="text/javascript">
                    let alertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
				</script>
				<input type="file" name="image" id="image" accept="image/*" onchange="validatePhoto(this,alertMessage)">
				<input type="hidden" name="senderId" id="senderId" value="${user.id}"><br/>
				<input type="hidden" name="recipientId" id="recipientId" value="${interlocutor.id}"><br/>
				<input type="hidden" name="scope" id="scope" value="chat">
				<button class="btn btn-primary" id="sendMessage" type="submit"><spring:message
						code="chat.button.send"/></button>
				<%--</form>--%>
				<br/>
			</c:if>
			<br>
			<p>
				<button class="btn btn-primary" onClick="window.location.reload();"><spring:message
						code="buttons.refresh"/></button>
			</p>
		</div>

		<script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
		<script src="${pageContext.request.contextPath}/js/messagingRealTime.js"></script>
		<script>
            var contextPath = "${pageContext.request.contextPath}";

            var senderId = "${pageContext.session.getAttribute('user').id}";
            var recipientId = "${interlocutor.id}";

            var senderName = "${pageContext.session.getAttribute('user').firstName}${' '} ${pageContext.session.getAttribute('user').lastName}";
            var recipientName = "${interlocutor.firstName}${' '} ${interlocutor.lastName}";

            var senderAvatarExists = "${pageContext.session.getAttribute('user').avatarExists}";
            var recipientAvatarExists = "${interlocutor.avatarExists}";

		</script>

	</div>
</div>
</body>
</html>
<div class="col-md-3 mr-4">
	<c:choose>
		<c:when test="${user.avatarExists}">
			<c:set var="avatarURL" scope="page" value="/images/uploaded?imageScope=accountAvatar&id=${user.id}"/>
		</c:when>
		<c:otherwise>
			<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
		</c:otherwise>
	</c:choose>

	<div class="left-block-image img-fluid">
		<img src="${pageContext.request.contextPath} ${avatarURL}" alt="User photo" width="200">
		<c:choose>
			<c:when test="${user.administrator}">
				<span class="status-text"><spring:message
						code="leftContent.status.administrator"/></span>
			</c:when>
			<c:otherwise><br/><br/></c:otherwise>
		</c:choose>
	</div>


	<div class="left-nav-bar">
		<ul>
			<li><a href="${pageContext.request.contextPath}/"><spring:message
					code="leftContent.button.myPage"/> </a></li>
			<li><a href="${pageContext.request.contextPath}news"><spring:message
					code="leftContent.button.news"/> </a></li>
			<li><a href="${pageContext.request.contextPath}friends"><spring:message
					code="leftContent.button.friends"/> </a></li>
			<li><a href="${pageContext.request.contextPath}groups"><spring:message
					code="leftContent.button.groups"/> </a></li>
			<li><a href="${pageContext.request.contextPath}messages"><spring:message
					code="leftContent.button.messages"/></a></li>
			<li><a href="${pageContext.request.contextPath}edit"><spring:message
					code="leftContent.button.edit"/> </a></li>
		</ul>
	</div>
</div>
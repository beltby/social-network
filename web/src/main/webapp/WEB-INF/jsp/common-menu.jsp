<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}js/searchAjax.js"></script>

<nav class="navbar navbar-dark navbar-expand-md px-1 mb-3" style="background-color: #333333;">
	<a class="navbar-brand" href="${pageContext.request.contextPath}/">${user.firstName} ${' '} ${user.lastName}</a>

	<button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse"><span
			class="navbar-toggler-icon"></span></button>

	<div id="my-nav" class="collapse navbar-collapse justify-content-end">
		<ul class="navbar-nav ">
			<li class="nav-item">
				<form class="search" action="${pageContext.request.contextPath}search" method="get"
				      style="margin:5px;max-width:350px">
					<input id="search" type="text" placeholder="<spring:message code="header.search.placeholder" />"
					       aria-label="Search" name="q">
					<button type="submit"><i class="fa fa-search"></i></button>
				</form>
			</li>

			<spring:message code="main.language.current" var="currentLanguage"/>
			<c:choose>
				<c:when test="${currentLanguage.equals('ru')}">
					<c:set var="switchLangKey" scope="page" value="en"/>
				</c:when>
				<c:when test="${currentLanguage.equals('en')}">
					<c:set var="switchLangKey" scope="page" value="ru"/>
				</c:when></c:choose>
			<c:choose>

				<c:when test="${param.size()==0||(param.size()==1 && param.get('lang')!=null)}">
					<c:set var="switchLangUrl" scope="page" value="?lang=${switchLangKey}"/>
				</c:when>
				<c:otherwise>
					<c:url var="urlWithParams" value="">
						<c:forEach items="${param}" var="entry">
							<c:if test="${entry.key != 'lang'}">
								<c:param name="${entry.key}" value="${entry.value}"/>
							</c:if>
						</c:forEach>
					</c:url>
					<c:set var="switchLangUrl" scope="page" value="${urlWithParams}&lang=${switchLangKey}"/>
				</c:otherwise>
			</c:choose>
			<spring:message code="auth.switch.language" var="language"/>
			<li class="nav-item"><a href="${pageContext.request.contextPath}${switchLangUrl}">
				<spring:message code="header.switch.language"/> </a></li>

			<li class="nav-item"><a href="${pageContext.request.contextPath}/logout">
				<spring:message code="header.logout.button"/> </a></li>
		</ul>
	</div>
</nav>
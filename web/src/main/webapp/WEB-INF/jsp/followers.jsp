<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="followers.title"/></title>
</head>
<jsp:useBean id="group" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Group"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<div class="col-md-3 mr-4">

			<c:choose>
				<c:when test="${group.avatarExists}">
					<c:set var="avatarURL" scope="page" value="/images/uploaded?imageScope=groupAvatar&id=${group.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-nav-bar">
				<spring:message code="followers.role.prefix" var="rolePrefix"/>
				<c:choose>
					<c:when test="${requestScope.userRole.equals('owner')}">
						<spring:message code="followers.role.owner" var="userRole"/>
					</c:when>
					<c:when test="${requestScope.userRole.equals('moderator')}">
						<spring:message code="followers.role.moderator" var="userRole"/>
					</c:when>
					<c:when test="${requestScope.userRole.equals('member')}">
						<spring:message code="followers.role.member" var="userRole"/>
					</c:when>
				</c:choose>

				<spring:message code="followers.role.prefix" var="rolePrefix"/>
				<c:set var="status" scope="page" value="${rolePrefix} ${userRole}"/>

				<div class="left-block-image img-fluid">
					<img src="${pageContext.request.contextPath} ${avatarURL}" alt="Group photo" width="200px"/>
					<span class="status-text">${status}</span>

					<c:choose>
						<c:when test="${requestScope.userRole eq 'owner'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-management"
							      method="get">
								<input type="hidden" name="id" value=${group.id}>
								<button class="btn btn-primary" type="submit"><spring:message
										code="followers.button.edit"/></button>
							</form>
						</c:when>

						<c:when test="${requestScope.userRole eq 'member' || requestScope.userRole eq 'moderator'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-membership"
							      method="post">
								<input type="hidden" name="action" value='unfollow'>
								<input type="hidden" name="userId" value=${user.id}>
								<input type="hidden" name="groupId" value=${group.id}>
								<input type="hidden" name="redirectURL" value="group?id=${group.id}">
								<button class="btn btn-primary" type="submit"><spring:message
										code="followers.button.unfollow"/></button>
							</form>
						</c:when>
					</c:choose>
				</div>

				<ul class="left-nav-bar">
					<li><a href="${pageContext.request.contextPath}group?id=${group.id}"><spring:message
							code="followers.button.wall"/></a></li>
					<li><a href="${pageContext.request.contextPath}groups"><spring:message
							code="followers.button.return"/></a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-6">
			<c:set var="URL" scope="page" value="followers?groupId=${group.id}"/>

			<div style="text-align: center"><strong><spring:message
					code="followers.table.prefix.title"/>${' '}${group.name}</strong></div>
			<c:if test="${requestScope.userRole eq 'owner' ||requestScope.userRole eq 'moderator'}">

				<c:if test="${not empty requestScope.incomingGroupJoinRequests}">
					<jsp:useBean id="incomingGroupJoinRequests" scope="request" type="java.util.List"/>

					<table class="table result-table">
						<caption><strong><spring:message code="followers.table.request.title"/></strong></caption>
						<tr>
							<th><spring:message code="followers.table.request.column.name"/></th>
							<th><spring:message code="followers.table.request.column.email"/></th>
							<th><spring:message code="followers.table.request.column.photo"/></th>
							<th><spring:message code="followers.table.request.column.status"/></th>
						</tr>

						<c:forEach items="${incomingGroupJoinRequests}" var="account" varStatus="status">
							<c:choose>
								<c:when test="${account.avatarExists}">
									<c:set var="avatarURL" scope="page"
									       value="/images/uploaded?imageScope=accountAvatar&id=${account.id}"/>
								</c:when>
								<c:otherwise>
									<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
								</c:otherwise>
							</c:choose>
							<tr>
								<td id="friends-center-align-1"><a
										href="${pageContext.request.contextPath}profile?id=${account.id}">${account.firstName}${' '}${account.lastName}</a>
								</td>
								<td id="friends-center-align-2">${account.email}</td>
								<td id="photo-friends-1"><img src="${pageContext.request.contextPath} ${avatarURL}"
								                              alt="User photo"
								                              width="130"></td>
								<td id="friends-center-align-3">

									<div class="blink"><p><strong><spring:message
											code="followers.table.request.message.newRequest"/></strong></p></div>
									<form action="${pageContext.request.contextPath}group-membership"
									      method="post">
										<input type="hidden" name="action" value='accept'>
										<input type="hidden" name="userId" value=${account.id}>
										<input type="hidden" name="groupId" value=${group.id}>
										<input type="hidden" name="redirectURL" value=${URL}>
										<button class="btn btn-primary" type="submit"><spring:message
												code="followers.table.request.button.accept"/></button>
									</form>
									<br/>
									<form action="${pageContext.request.contextPath}group-membership"
									      method="post">
										<input type="hidden" name="action" value='reject'>
										<input type="hidden" name="userId" value=${account.id}>
										<input type="hidden" name="groupId" value=${group.id}>
										<input type="hidden" name="redirectURL" value=${URL}>
										<button class="btn btn-primary" type="submit"><spring:message
												code="followers.table.request.button.reject"/></button>
									</form>

									<c:if test="${not account.active}">
										<p id="red_text"><strong><spring:message
												code="followers.table.request.message.blocked"/></strong></p>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</c:if>

			<table class="table result-table">
				<caption><strong><spring:message code="followers.table.followers.title"/></strong></caption>
				<tr>
					<th><spring:message code="followers.table.followers.column.name"/></th>
					<th><spring:message code="followers.table.followers.column.email"/></th>
					<th><spring:message code="followers.table.followers.column.photo"/></th>
					<th><spring:message code="followers.table.followers.column.status"/></th>
				</tr>
				<jsp:useBean id="followers" scope="request" type="java.util.List"/>
				<c:forEach items="${followers}" var="follower">
					<c:choose>
						<c:when test="${follower.user.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${follower.user.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="friends-center-align-4"><a
								href="${pageContext.request.contextPath}profile?id=${follower.user.id}">${follower.user.firstName}${' '} ${follower.user.lastName}</a>
						</td>
						<td id="friends-left-align-5"> ${follower.user.email}</td>
						<td id="photo-friends-2"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                              alt="User photo"
						                              width="130"></td>
						<td id="friends-center-align-6">
							<c:set var="role" scope="page" value="${follower.role}"/>

							<c:choose>
								<c:when test="${role.equals('owner')}">
									<div><p><strong><spring:message
											code="followers.table.followers.column.action.status.owner"/></strong></p>
									</div>
								</c:when>
								<c:when test="${role.equals('moderator')}">
									<div><p><strong><spring:message
											code="followers.table.followers.column.action.status.moderator"/></strong>
									</p>
									</div>
								</c:when>
								<c:when test="${role.equals('member')}">
									<div><p><strong><spring:message
											code="followers.table.followers.column.action.status.member"/></strong></p>
									</div>
								</c:when>
							</c:choose>

							<c:choose>
								<c:when test="${requestScope.userRole.equals('owner')}">
									<c:choose>
										<c:when test="${role eq 'member'}">
											<form action="${pageContext.request.contextPath}group-membership"
											      method="post">
												<input type="hidden" name="action" value='setModerator'>
												<input type="hidden" name="userId" value=${follower.user.id}>
												<input type="hidden" name="groupId" value=${group.id}>
												<input type="hidden" name="redirectURL" value=${URL}>
												<button class="btn btn-primary" type="submit"><spring:message
														code="followers.table.followers.button.setModerator"/></button>
											</form>
											<br/>
										</c:when>
										<c:when test="${role eq 'moderator'}">
											<form action="${pageContext.request.contextPath}group-membership"
											      method="post">
												<input type="hidden" name="action" value='setMember'>
												<input type="hidden" name="userId" value=${follower.user.id}>
												<input type="hidden" name="groupId" value=${group.id}>
												<input type="hidden" name="redirectURL" value=${URL}>
												<button class="btn btn-primary" type="submit"><spring:message
														code="followers.table.followers.button.setMember"/></button>
											</form>
											<br/>
										</c:when>
									</c:choose>
								</c:when>
							</c:choose>

							<c:if test="${(requestScope.userRole eq 'owner' && role ne 'owner')||(requestScope.userRole eq 'moderator'&& role eq 'member')}">
								<form action="${pageContext.request.contextPath}group-membership"
								      method="post">
									<input type="hidden" name="action" value='delete'>
									<input type="hidden" name="userId" value=${follower.user.id}>
									<input type="hidden" name="groupId" value=${group.id}>
									<input type="hidden" name="redirectURL" value=${URL}>
									<button class="btn btn-primary" type="submit"><spring:message
											code="followers.table.followers.button.delete"/></button>
								</form>
								<br/>
							</c:if>

							<c:if test="${not follower.user.active}">
								<p id="red_text"><strong><spring:message
										code="followers.table.followers.message.blocked"/></strong></p>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
</body>
</html>
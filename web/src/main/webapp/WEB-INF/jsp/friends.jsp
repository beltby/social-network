<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title>${profile.firstName}${' '}${profile.lastName}${' '}<spring:message code="friends.title.postfix"/></title>
</head>
<jsp:useBean id="profile" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>
<c:set var="active" scope="page" value="${profile.active}"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<div class="col-md-3 mr-4">
			<c:choose>
				<c:when test="${profile.avatarExists}">
					<c:set var="avatarURL" scope="page"
					       value="/images/uploaded?imageScope=accountAvatar&id=${profile.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-block-image img-fluid">
				<img src="${pageContext.request.contextPath} ${avatarURL}" alt="User photo" width="200">
				<c:if test="${not active}">
					<span class="status-text"><spring:message code="friends.status.blocked"/></span>
				</c:if>
				<c:if test="${profile.administrator}">
					<span class="status-text"><spring:message code="friends.status.administrator"/></span>
				</c:if>
			</div>

			<c:if test="${not profile.administrator && active}">
				<br/> <br/>
			</c:if>

			<div class="left-nav-bar">
				<ul>
					<li><a href="${pageContext.request.contextPath}/"><spring:message
							code="friends.button.return"/> </a>
					</li>
					<c:if test="${active}">
						<li><a href="${pageContext.request.contextPath}chat?profileId=${profile.id}"><spring:message
								code="friends.button.sendMessage"/></a></li>
					</c:if>
					<li><a href="${pageContext.request.contextPath}profile?id=${profile.id}"><spring:message
							code="friends.button.toUserProfile"/> </a></li>
					<li><a href="${pageContext.request.contextPath}friends?profileId=${profile.id}"><spring:message
							code="friends.button.toUserFriends"/> </a></li>
					<li><a href="${pageContext.request.contextPath}groups?profileId=${profile.id}"><spring:message
							code="friends.button.toUserGroups"/> </a>
					</li>
				</ul>

				<c:choose>
					<c:when test="${user.administrator}">
						<form action="${pageContext.request.contextPath}edit"
						      method="get">
							<input type="hidden" name="profileId" value=${profile.id}>
							<button class="btn btn-primary" type="submit"><spring:message code="friends.button.editProfile"/></button>
						</form>
						<br/>
						<c:choose>
							<c:when test="${profile.active}">
								<spring:message code="friends.button.blockProfile" var="actionName"/>
							</c:when>
							<c:otherwise>
								<spring:message code="friends.button.unblockProfile" var="actionName"/>
							</c:otherwise>
						</c:choose>

						<form action="${pageContext.request.contextPath}profile?action=changeActivity&id=${profile.id}"
						      method="post">
							<button class="btn btn-primary" type="submit">${actionName}</button>
						</form>
						<br/>

						<c:choose>
							<c:when test="${not profile.administrator}">
								<form action="${pageContext.request.contextPath}profile?action=setAdministrator&id=${profile.id}"
								      method="post">
									<button class="btn btn-primary" type="submit"><spring:message
											code="friends.button.setAdministrator"/></button>
								</form>
							</c:when>
						</c:choose>
					</c:when>
				</c:choose>
			</div>
		</div>
		<div class="col-md-6">

			<table class="table result-table">
				<caption><strong>${profile.firstName}${" "}${profile.lastName}${" "}<spring:message
						code="friends.table.title.postfix"/></strong></caption>
				<tr>
					<th><spring:message code="friends.table.column.name"/></th>
					<th><spring:message code="friends.table.column.email"/></th>
					<th><spring:message code="friends.table.column.photo"/></th>
				</tr>
				<jsp:useBean id="friends" scope="request" type="java.util.List"/>
				<c:forEach items="${friends}" var="friend" varStatus="status">
					<c:choose>
						<c:when test="${friend.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${friend.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="friends-center-align-4"><a
								href="${pageContext.request.contextPath}profile?id=${friend.id}">${friend.firstName}${' '} ${friend.lastName}</a>
						</td>
						<td id="friends-center-align-7"> ${friend.email}</td>
						<td id="photo-friends-2"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                              alt="User photo"
						                              width="130"></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
</body>
</html>
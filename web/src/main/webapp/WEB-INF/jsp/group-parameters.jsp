<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:useBean id="user" scope="session"
             type="com.getjavajob.training.socialnetwork1907.kapustsina.dto.SessionAccountDto"/>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
	      integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">

	<c:choose>
		<c:when test="${empty requestScope.group}">
			<spring:message code="group-parameters.var.title.create" var="title" scope="page"/>
			<spring:message code="group-parameters.var.button.submit" var="button" scope="page"/>
			<spring:message code="group-parameters.var.name.empty" var="name" scope="page"/>
			<spring:message code="group-parameters.var.description.empty" var="description" scope="page"/>
			<c:set var="id" scope="page" value="0"/>
			<c:set var="open" scope="page" value="${true}"/>
			<c:set var="URL" scope="page" value="group-create"/>
		</c:when>
		<c:otherwise>
			<jsp:useBean id="group" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Group"/>
			<spring:message code="group-parameters.var.title.edit" arguments="${group.name}" var="title" scope="page"/>
			<spring:message code="group-parameters.var.button.save" var="button" scope="page"/>
			<spring:message code="group-parameters.var.name" arguments="${group.name}" var="name" scope="page"/>
			<spring:message code="group-parameters.var.description" arguments="${group.description}" var="description"
			                scope="page"/>
			<c:set var="id" scope="page" value="${group.id}"/>
			<c:set var="open" scope="page" value="${group.open}"/>
			<c:set var="URL" scope="page" value="group-update"/>
		</c:otherwise>
	</c:choose>
	<title>${title}</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>

<div class="container">
	<div class="row">
		<br>
		<div class="col-md-3 mr-4">
			<c:choose>
				<c:when test="${group.avatarExists}">
					<c:set var="avatarURL" scope="page"
					       value="/images/uploaded?imageScope=groupAvatar&id=${group.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-block-image img-fluid">
				<img src="${pageContext.request.contextPath} ${avatarURL}" alt="Group avatar" width="200">
			</div>
		</div>

		<div class="col-md-6">
			<script type="text/javascript">
                let confirmationAlertMessage = "<spring:message code='js.form.confirmation.message' javaScriptEscape='true' />";
                let alertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
			</script>
			<form:form onsubmit="return formConfirmation(confirmationAlertMessage)" method="post"
			           action="${pageContext.request.contextPath}${URL}" modelAttribute="group"
			           enctype="multipart/form-data">
				<div class="table-row">
					<label for="name" style="font-weight: bold"><spring:message
							code="group-parameters.form.name"/></label>
					<input type="text" name="name" id="name" placeholder=
						<spring:message
								code="group-parameters.form.placeholder.name"/> value="${name}" required><br/><br/>

					<label for="description" style="font-weight: bold"><spring:message
							code="group-parameters.form.description"/></label>
					<input type="text" name="description" id="description" placeholder=
						<spring:message
								code="group-parameters.form.placeholder.description"/> value="${description}"><br/><br/>

					<label for="image" style="font-weight: bold"><spring:message
							code="group-parameters.form.photo"/></label>
					<input type="file" name="image" id="image" accept="image/*"
					       onchange="validatePhoto(this,alertMessage)"><br/><br/>
				</div>
				<input type="hidden" name="id" value="${id}"/>

				<button class="btn btn-primary" type="submit" style="margin:0 19px 0 0">${button}</button>
				<button class="btn btn-primary" style="margin:0 19px 0 0" onClick="window.location.reload();">
					<spring:message code="buttons.refresh"/></button>
				<input type=button class="btn btn-primary" value=
					<spring:message code="buttons.back"/> onCLick="history.back()">
			</form:form>
		</div>
	</div>
</div>
</body>
</html>
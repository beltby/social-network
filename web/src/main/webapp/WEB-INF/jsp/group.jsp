<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title>${group.name}</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<jsp:useBean id="group" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Group"/>
<body>
<a id="top"></a>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<div class="col-md-3 mr-4">
			<c:choose>
				<c:when test="${group.avatarExists}">
					<c:set var="avatarURL" scope="page" value="/images/uploaded?imageScope=groupAvatar&id=${group.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-nav-bar">
				<c:choose>
					<c:when test="${requestScope.role eq 'owner'||requestScope.role eq 'moderator'||requestScope.role eq 'member'}">
						<spring:message code="group.role.prefix" var="prefix"/>
						<c:choose>
							<c:when test="${requestScope.role.equals('owner')}">
								<spring:message code="group.role.owner" var="userRole"/>
							</c:when>
							<c:when test="${requestScope.role.equals('moderator')}">
								<spring:message code="group.role.moderator" var="userRole"/>
							</c:when>
							<c:when test="${requestScope.role.equals('member')}">
								<spring:message code="group.role.member" var="userRole"/>
							</c:when>
						</c:choose>

						<c:set var="status" scope="page" value="${prefix}${' '}${userRole}"/>
					</c:when>
					<c:when test="${requestScope.role eq 'request'}">
						<spring:message code="group.role.request" var="userRole"/>
						<c:set var="status" scope="page" value="${userRole}"/>
					</c:when>
					<c:when test="${requestScope.role eq 'stranger'}">
						<spring:message code="group.role.none" var="userRole"/>
						<c:set var="status" scope="page" value="${userRole}"/>
					</c:when>
				</c:choose>

				<div class="left-block-image img-fluid">
					<img src="${pageContext.request.contextPath} ${avatarURL}" alt="Group photo" width="200px"/>
					<span class="status-text">${status}</span>
					<c:set var="URL" scope="page" value="/group?id=${group.id}"/>

					<c:choose>
						<c:when test="${requestScope.role eq 'owner'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-management"
							      method="get">
								<input type="hidden" name="id" value=${group.id}>
								<button class="btn btn-primary" type="submit"><spring:message
										code="group.button.edit"/></button>
							</form>
						</c:when>

						<c:when test="${requestScope.role eq 'member' || requestScope.role eq 'moderator'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-membership"
							      method="post">
								<input type="hidden" name="action" value='unfollow'>
								<input type="hidden" name="userId" value=${user.id}>
								<input type="hidden" name="groupId" value=${group.id}>
								<input type="hidden" name="redirectURL" value=${URL}>
								<button class="btn btn-primary" type="submit"><spring:message
										code="group.button.unfollow"/></button>
							</form>
						</c:when>

						<c:when test="${requestScope.role eq 'stranger'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-membership"
							      method="post">
								<input type="hidden" name="action" value='follow'>
								<input type="hidden" name="userId" value=${user.id}>
								<input type="hidden" name="groupId" value=${group.id}>
								<input type="hidden" name="redirectURL" value=${URL}>
								<button class="btn btn-primary" type="submit"><spring:message
										code="group.button.follow"/></button>
							</form>
						</c:when>

						<c:when test="${requestScope.role eq 'request'}">
							<form class="buttons-block" action="${pageContext.request.contextPath}group-membership"
							      method="post">
								<input type="hidden" name="action" value='delete'>
								<input type="hidden" name="userId" value=${user.id}>
								<input type="hidden" name="groupId" value=${group.id}>
								<input type="hidden" name="redirectURL" value=${URL}>
								<button class="btn btn-primary" type="submit"><spring:message
										code="group.button.cancel"/></button>
							</form>
						</c:when>
					</c:choose>
				</div>

				<ul class="left-nav-bar">
					<c:if test="${requestScope.role ne 'stranger'&& requestScope.role ne 'request'}">
						<li><a href="${pageContext.request.contextPath}followers?groupId=${group.id}"><spring:message
								code="group.button.followers"/></a></li>
					</c:if>
					<li><a href="${pageContext.request.contextPath}groups"><spring:message
							code="group.button.return"/> </a>
					</li>
				</ul>
			</div>
		</div>


		<div class="col-md-6">
			<p><strong><spring:message code="group.description.name"/></strong> ${group.name}</p>
			<p><strong><spring:message code="group.description.description"/></strong> ${group.description}</p>
			<p><strong><spring:message code="group.description.creator"/></strong> <a
					href="${pageContext.request.contextPath}profile?id=${group.creator.id}"> ${group.creator.firstName}${' '}${group.creator.lastName}</a>
			</p>
			<p><strong><spring:message code="group.description.creation"/></strong> ${group.registrationDate}</p>

			<c:if test="${requestScope.role ne 'stranger'&& requestScope.role ne 'request'}">
				<a id="form"></a>
				<form method="post" action="${pageContext.request.contextPath}post" enctype="multipart/form-data">
					<p><label for="text"></label>
						<textarea name="text" id="text" cols="48" rows="8"></textarea>
					</p>
					<script type="text/javascript">
                        let alertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
					</script>
					<input type="file" name="image" accept="image/*" onchange="validatePhoto(this,alertMessage)">
					<input type="hidden" name="senderId" value="${user.id}"><br/>
					<input type="hidden" name="recipientId" value="${group.id}"><br/>
					<input type="hidden" name="scope" value="group">
					<button class="btn btn-primary" type="submit"><spring:message code="group.button.send"/></button>
					<br/>
				</form>

				<br/>
				<table class="table result-table">
					<tr>
						<th><spring:message code="group.table.column.sender"/></th>
						<th><spring:message code="group.table.column.message"/></th>
						<th><spring:message code="group.table.column.dateAndTime"/></th>
					</tr>

					<jsp:useBean id="posts" scope="request" type="java.util.List"/>
					<c:forEach items="${posts}" var="post" varStatus="status">
						<c:if test="${post.imageExists}">
							<c:set var="imageURL" scope="page"
							       value="/images/uploaded?imageScope=attachedImage&id=${post.id}"/>
						</c:if>

						<c:set var="author" scope="page" value="${post.sender}"/>
						<jsp:useBean id="author" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>

						<c:choose>
							<c:when test="${post.sender.id==user.id}">
								<c:set var="senderFirstName" scope="page" value="${user.firstName}"/>
								<c:set var="senderLastName" scope="page" value="${user.lastName}"/>
								<c:set var="senderId" scope="page" value="${user.id}"/>
							</c:when>

							<c:otherwise>
								<c:set var="senderFirstName" scope="page" value="${author.firstName}"/>
								<c:set var="senderLastName" scope="page" value="${author.lastName}"/>
								<c:set var="senderId" scope="page" value="${author.id}"/>
							</c:otherwise>
						</c:choose>

						<c:choose>
							<c:when test="${post.sender.id==user.id&&user.avatarExists}">
								<c:set var="avatarURL" scope="page"
								       value="/images/uploaded?imageScope=accountAvatar&id=${user.id}"/>
							</c:when>
							<c:when test="${post.sender.id==author.id&&author.avatarExists}">
								<c:set var="avatarURL" scope="page"
								       value="/images/uploaded?imageScope=accountAvatar&id=${author.id}"/>
							</c:when>
							<c:otherwise>
								<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
							</c:otherwise>
						</c:choose>

						<tr>
							<td id="profile-wall-photo"><img src="${pageContext.request.contextPath} ${avatarURL}"
							                                 alt="Sender photo"
							                                 width="60">
								<br/> <a
										href="${pageContext.request.contextPath}profile?id=${senderId}">${senderFirstName}${' '} ${senderLastName}</a>
							</td>
							<td id="profile-wall-left-align">
									${post.text}<br>
								<c:if test="${post.imageExists}">
									<img src="${pageContext.request.contextPath} /images/uploaded?imageScope=attachedImage&id=${post.id}"
									     alt="Attached image" width="100"
									     onclick="window.open('/images/uploaded?imageScope=attachedImage&id='+${post.id}, '_blank');">
								</c:if>
							</td>
							<td id="profile-wall-center-align">
								<fmt:parseDate value="${post.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
								               var="parsedDateTime"
								               type="both"/>
								<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
				<a href="#top"><spring:message code="group.button.top"/></a>
			</c:if>
		</div>
	</div>
</div>
</body>
</html>
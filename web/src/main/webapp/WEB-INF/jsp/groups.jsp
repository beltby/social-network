<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<head>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
	<title>${profile.firstName}${" "}${profile.lastName}${" "}<spring:message code="groups.title.postfix"/></title>
</head>
<jsp:useBean id="profile" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>
<jsp:useBean id="groups" scope="request" type="java.util.List"/>
<c:set var="active" scope="page" value="${profile.active}"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<div class="col-md-3 mr-4">

			<c:choose>
				<c:when test="${profile.avatarExists}">
					<c:set var="avatarURL" scope="page"
					       value="/images/uploaded?imageScope=accountAvatar&id=${profile.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-block-image img-fluid">
				<img src="${pageContext.request.contextPath} ${avatarURL}" alt="User photo" width="200">
				<c:if test="${not active}">
					<span class="status-text"><spring:message code="groups.status.blocked"/></span>
				</c:if>
				<c:if test="${profile.administrator}">
					<span class="status-text"><spring:message code="groups.status.administrator"/></span>
				</c:if>
			</div>

			<c:if test="${not profile.administrator && active}">
				<br/> <br/>
			</c:if>

			<div class="left-nav-bar">
				<ul>
					<li><a href="${pageContext.request.contextPath}/"><spring:message code="groups.button.return"/></a>
					</li>
					<c:if test="${active}">
						<li><a href="${pageContext.request.contextPath}chat?profileId=${profile.id}"><spring:message
								code="groups.button.sendMessage"/></a></li>
					</c:if>
					<li><a href="${pageContext.request.contextPath}profile?id=${profile.id}"><spring:message
							code="groups.button.toUserProfile"/> </a></li>
					<li><a href="${pageContext.request.contextPath}friends?profileId=${profile.id}"><spring:message
							code="groups.button.toUserFriends"/> </a></li>
					<li><a href="${pageContext.request.contextPath}groups?profileId=${profile.id}"><spring:message
							code="groups.button.toUserGroups"/> </a>
					</li>
				</ul>

				<c:choose>
					<c:when test="${user.administrator}">
						<form action="${pageContext.request.contextPath}edit"
						      method="get">
							<input type="hidden" name="profileId" value=${profile.id}>
							<button class="btn btn-primary" type="submit"><spring:message
									code="groups.button.editProfile"/></button>
						</form>
						<br/>
						<c:choose>
							<c:when test="${profile.active}">
								<spring:message code="groups.button.blockProfile" var="actionName" scope="page"/>
							</c:when>
							<c:otherwise>
								<spring:message code="groups.button.unblockProfile" var="actionName" scope="page"/>
							</c:otherwise>
						</c:choose>

						<form action="${pageContext.request.contextPath}profile?action=changeActivity&id=${profile.id}"
						      method="post">
							<button class="btn btn-primary" type="submit">${actionName}</button>
						</form>
						<br/>

						<c:choose>
							<c:when test="${not profile.administrator}">
								<form action="${pageContext.request.contextPath}profile?action=setAdministrator&id=${profile.id}"
								      method="post">
									<button class="btn btn-primary" type="submit"><spring:message
											code="groups.button.setAdministrator"/></button>
								</form>
							</c:when>
						</c:choose>
					</c:when>
				</c:choose>
			</div>
		</div>

		<div class="col-md-6">
			<table class="table result-table">
				<caption><strong>${profile.firstName}${" "}${profile.lastName}${" "}<spring:message
						code="groups.table.title.postfix"/></strong></caption>
				<tr>
					<th><spring:message code="groups.table.column.name"/></th>
					<th><spring:message code="groups.table.column.description"/></th>
					<th><spring:message code="groups.table.column.creator"/></th>
					<th><spring:message code="groups.table.column.photo"/></th>
				</tr>
				<c:forEach items="${groups}" var="group">
					<c:choose>
						<c:when test="${group.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=groupAvatar&id=${group.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
						</c:otherwise>
					</c:choose>
					<tr>
						<td id="groups-center-align-5"><a
								href="${pageContext.request.contextPath}group?id=${group.id}">${group.name}</a>
						</td>
						<td id="groups-center-align-6">${group.description}</td>
						<td id="groups-center-align-7">
							<a href="${pageContext.request.contextPath}profile?id=${group.creator.id}"> ${group.creator.firstName}${' '}${group.creator.lastName}</a>
						</td>
						<td id="photo-groups"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                           alt="Group picture"
						                           width="130"></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div class="column side"></div>
	</div>
</div>
</body>
</body>
</html>
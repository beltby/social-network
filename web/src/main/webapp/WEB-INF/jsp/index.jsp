<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="index.title"/></title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<jsp:useBean id="profile" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>
<jsp:useBean id="posts" scope="request" type="java.util.List"/>
<body>
<a id="top"></a>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
		<div class="col-md-6">
			<c:if test="${not empty sessionScope.updateResult}">
				<c:choose>
					<c:when test="${sessionScope.updateResult}">
						<div><p style="color:#00bc12;font-weight: bold"><spring:message
								code="index.message.updateSuccess"/></p></div>
						<c:remove var="updateResult"/>
					</c:when>
					<c:otherwise>
						<div><p style="color:#ff0000;font-weight: bold"><spring:message
								code="index.message.updateFailed"/></p></div>
						<c:remove var="updateResult"/>
					</c:otherwise>
				</c:choose>
			</c:if>

			<p><strong><spring:message code="index.description.firstName"/></strong> ${profile.firstName}</p>
			<p><strong><spring:message code="index.description.lastName"/></strong> ${profile.lastName}</p>
			<p><strong><spring:message code="index.description.email"/></strong> ${profile.email}</p>
			<p><strong><spring:message code="index.description.birthDate"/></strong> ${profile.birthDate}</p>

			<c:forEach items="${profile.phones}" var="phone">
				<c:choose>
					<c:when test="${phone.type.equals('cell')}">
						<p><strong><spring:message code="index.description.cellPhone"/></strong> ${phone.value}</p>
					</c:when>
					<c:otherwise>
						<p><strong><spring:message code="index.description.workPhone"/></strong> ${phone.value}</p>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<c:set var="none" scope="page" value="-"/>
			<p><strong><spring:message code="index.description.homeAddress"/></strong>
				<c:choose>
					<c:when test="${empty profile.homeAddress}">
						${none}
					</c:when>
					<c:otherwise>
						${profile.homeAddress}
					</c:otherwise>
				</c:choose></p>
			<p><strong><spring:message code="index.description.workAddress"/></strong>
				<c:choose>
					<c:when test="${empty profile.workAddress}">
						${none}
					</c:when>
					<c:otherwise>
						${profile.workAddress}
					</c:otherwise>
				</c:choose></p>
			<p><strong><spring:message code="index.description.icq"/></strong>
				<c:choose>
					<c:when test="${empty profile.icq}">
						${none}
					</c:when>
					<c:otherwise>
						${profile.icq}
					</c:otherwise>
				</c:choose></p>
			<p><strong><spring:message code="index.description.skype"/></strong>
				<c:choose>
					<c:when test="${empty profile.skype}">
						${none}
					</c:when>
					<c:otherwise>
						${profile.skype}
					</c:otherwise>
				</c:choose></p>
			<p><strong><spring:message code="index.description.additionalData"/></strong> <c:choose>
				<c:when test="${empty profile.additionalData}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.additionalData}
				</c:otherwise>
			</c:choose></p>

			<p><strong><spring:message code="index.description.registrationDate"/></strong> ${profile.registrationDate}
			</p>
			<a id="form"></a>
			<form method="post" action="${pageContext.request.contextPath}post" enctype="multipart/form-data">
				<div class="form-group">
					<p><label for="text"></label>
						<textarea name="text" id="text" cols="48" rows="8"></textarea>
					</p></div>
				<input type="file" name="image" accept="image/*" onchange="validatePhoto(this, alertMessage)">
				<input type="hidden" name="senderId" value="${profile.id}"><br/>
				<input type="hidden" name="recipientId" value="${profile.id}"><br/>
				<input type="hidden" name="scope" value="wall">
				<button class="btn btn-primary" type="submit"><spring:message code="index.wall.button.send"/></button>
			</form>

			<br/>
			<table class="result-table table">
				<thead>
				<tr>
					<th><spring:message code="index.table.column.sender"/></th>
					<th><spring:message code="index.table.column.message"/></th>
					<th><spring:message code="index.table.column.dateAndTime"/></th>
				</tr>
				</thead>
				<tbody>
				<c:forEach items="${posts}" var="post" varStatus="status">
					<c:if test="${post.imageExists}">
						<c:set var="imageURL" scope="page"
						       value="/images/uploaded?imageScope=attachedImage&id=${post.id}"/>
					</c:if>

					<c:set var="author" scope="page" value="${post.sender}"/>
					<jsp:useBean id="author" scope="page"
					             type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>

					<c:choose>
						<c:when test="${post.sender.id==profile.id}">
							<c:set var="senderFirstName" scope="page" value="${profile.firstName}"/>
							<c:set var="senderLastName" scope="page" value="${profile.lastName}"/>
							<c:set var="senderId" scope="page" value="${profile.id}"/>
						</c:when>

						<c:otherwise>
							<c:set var="senderFirstName" scope="page" value="${author.firstName}"/>
							<c:set var="senderLastName" scope="page" value="${author.lastName}"/>
							<c:set var="senderId" scope="page" value="${author.id}"/>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${post.sender.id==profile.id&&profile.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${profile.id}"/>
						</c:when>
						<c:when test="${post.sender.id==author.id&&author.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${author.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="photo">
							<img src="${pageContext.request.contextPath} ${avatarURL}" alt="Sender photo"
							     width="60">
							<br/> <a
								href="${pageContext.request.contextPath}profile?id=${senderId}">${senderFirstName}${' '} ${senderLastName}</a>
							<c:if test="${not author.active}">
								<br/>
								<span id="red_text"><spring:message code="index.status.blocked"/></span>
							</c:if>
						</td>
						<td id="index-left-align">
								${post.text}
							<br/>
							<c:if test="${post.imageExists}">
								<img src="${pageContext.request.contextPath} /images/uploaded?imageScope=attachedImage&id=${post.id}"
								     alt="Attached image" width="100"
								     onclick="window.open('/images/uploaded?imageScope=attachedImage&id='+${post.id}, '_blank');">
							</c:if>

						</td>
						<td id="index-center-align">
							<fmt:parseDate value="${post.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
							               var="parsedDateTime"
							               type="both"/>
							<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<a href="#top"><spring:message code="index.button.top"/></a>
		</div>
	</div>
</div>
<script type="text/javascript">
    let alertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
</script>
</body>
</html>
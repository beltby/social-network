<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="messages.title"/></title>
</head>
<jsp:useBean id="lastMessages" scope="request" type="java.util.List"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>

		<div class="col-md-6">
			<table class="result-table">
				<caption><strong><spring:message code="messages.table.title"/></strong></caption>
				<tr>
					<th><spring:message code="messages.table.column.interlocutor"/></th>
					<th><spring:message code="messages.table.column.message"/></th>
					<th><spring:message code="messages.table.column.dateAndTime"/></th>
				</tr>

				<c:forEach items="${lastMessages}" var="message" varStatus="status">
					<c:choose>
						<c:when test="${message.imageExists&&empty message.text}">
							<c:set var="lastMessage" scope="page" value="Attached image"/>
						</c:when>
						<c:otherwise>
							<c:set var="lastMessage" scope="page" value="${message.text}"/>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${message.sender.id==sessionScope.user.id}">
							<c:set var="interlocutor" scope="page" value="${message.recipient}"/>
						</c:when>
						<c:otherwise>
							<c:set var="interlocutor" scope="page" value="${message.sender}"/>
						</c:otherwise>
					</c:choose>

					<jsp:useBean id="interlocutor" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>

					<c:choose>
						<c:when test="${interlocutor.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${interlocutor.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="messages-photo">

							<img src="${pageContext.request.contextPath} ${avatarURL}"
							     alt="Interlocutor photo" width="100">

							<a href="${pageContext.request.contextPath}profile?id=${interlocutor.id}"> ${interlocutor.firstName}${' '} ${interlocutor.lastName}</a>
							<br/>
							<c:if test="${not interlocutor.active}">
								<span id="red_text"><spring:message code="messages.status.blocked"/></span>

							</c:if>
						</td>

						<td id="messages-left-align-1"><a
								href="${pageContext.request.contextPath}chat?profileId=${interlocutor.id}">${lastMessage}</a>
						</td>

						<td id="messages-center-align-1">
							<fmt:parseDate value="${message.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
							               var="parsedDateTime"
							               type="both"/>
							<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
</body>
</html>
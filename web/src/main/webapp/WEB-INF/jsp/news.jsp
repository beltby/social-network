<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="news.title"/></title>
</head>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
    <div class="row">
        <%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
        <div class="col-md-6">
            <jsp:useBean id="news" scope="request" type="java.util.List"/>

            <c:choose>
                <c:when test="${empty news}">
                    <strong> <spring:message code="news.message.empty"/></strong>
                    <br> <br>
                    <div id="navButtons">
                        <button class="btn btn-primary" style="float:left "
                                onClick="window.location.href='${pageContext.request.contextPath}/';">
                            <spring:message code="news.button.back"/></button>
                    </div>
                </c:when>
                <c:otherwise>

                    <table class="table result-table">
                        <caption><strong><spring:message code="news.table.title"/></strong></caption>
                        <tr>
                            <th><spring:message code="news.table.column.friend"/></th>
                            <th><spring:message code="news.table.column.event"/></th>
                            <th><spring:message code="news.table.column.dateAndTime"/></th>
                        </tr>

                        <c:forEach items="${news}" var="event" varStatus="status">
                            <c:choose>
                                <c:when test="${event.senderAvatarExists}">
                                    <c:set var="avatarURL" scope="page"
                                           value="/images/uploaded?imageScope=accountAvatar&id=${event.senderId}"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
                                </c:otherwise>
                            </c:choose>

                            <c:set var="textWrote">
                                <spring:message code="news.table.text.wrote"/>
                            </c:set>

                            <c:set var="textImage">
                                <c:choose>
                                    <c:when test="${event.imageExists}">
                                        <spring:message code="news.table.text.withImage"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="news.table.text.withoutImage"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:set>

                            <c:set var="textWall">
                                <spring:message code="news.table.text.wall"/>
                            </c:set>

                            <tr>
                                <td id="chat-photo"><img src="${pageContext.request.contextPath} ${avatarURL}"
                                                         alt="Sender photo"
                                                         width="60"> <br/>
                                    <a href="${pageContext.request.contextPath}profile?id=${event.senderId}">${event.sender}</a>
                                </td>
                                <td id="news-left-align-1">
                                        ${textWrote.concat(' \"').concat(event.text).concat('\" ')} ${textImage.concat(' ').concat(textWall).concat(' ')}
                                    <a href="${pageContext.request.contextPath}profile?id=${event.recipientId}">${event.recipient}</a>
                                </td>
                                <td id="news-center-align-1">
                                    <fmt:parseDate value="${event.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
                                                   var="parsedDateTime"
                                                   type="both"/>
                                    <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>

                    <div id="navButtons">
                        <c:if test="${page != 1}">
                            <form method="post" action="${pageContext.request.contextPath}news"
                                  enctype="multipart/form-data"
                                  style="float: left">
                                <input type="hidden" name="p" value="${page-1}">
                                <button class="btn btn-primary" type="submit"><spring:message
                                        code="news.button.back"/></button>
                            </form>
                        </c:if>
                        <c:if test="${page == 1&&lastPage}">
                            <div id="navButtons">
                                <button class="btn btn-primary" style="float:left "
                                        onClick="window.location.href='${pageContext.request.contextPath}/';">
                                    <spring:message code="news.button.back"/></button>
                            </div>
                        </c:if>
                        <c:if test="${not lastPage}">
                            <form method="post" action="${pageContext.request.contextPath}news"
                                  enctype="multipart/form-data"
                                  style="float: right">
                                <input type="hidden" name="p" value="${page+1}">
                                <button class="btn btn-primary" type="submit"><spring:message
                                        code="news.button.next"/></button>
                            </form>
                        </c:if>
                        <br>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
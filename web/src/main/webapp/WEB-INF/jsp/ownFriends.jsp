<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="ownFriends.title"/></title>
</head>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
		<div class="col-md-6">
			<c:if test="${not empty requestScope.receivedFriendshipRequests||not empty requestScope.sentFriendshipRequests}">

				<table class="table result-table">
					<caption><strong><spring:message code="ownFriends.table.request.title"/></strong></caption>
					<tr>
						<th><spring:message code="ownFriends.table.request.column.name"/></th>
						<th><spring:message code="ownFriends.table.request.column.email"/></th>
						<th><spring:message code="ownFriends.table.request.column.photo"/></th>
						<th><spring:message code="ownFriends.table.request.column.status"/></th>
					</tr>

					<c:if test="${not empty requestScope.receivedFriendshipRequests}">
						<jsp:useBean id="receivedFriendshipRequests" scope="request" type="java.util.List"/>

						<c:forEach items="${receivedFriendshipRequests}" var="account">
							<c:choose>
								<c:when test="${account.avatarExists}">
									<c:set var="avatarURL" scope="page"
									       value="/images/uploaded?imageScope=accountAvatar&id=${account.id}"/>
								</c:when>
								<c:otherwise>
									<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
								</c:otherwise>
							</c:choose>
							<tr>
								<td id="friends-center-align-1"><a
										href="${pageContext.request.contextPath}profile?id=${account.id}">${account.firstName}${' '}${account.lastName}</a>
								</td>
								<td id="friends-center-align-2">${account.email}</td>
								<td id="photo-friends-1"><img src="${pageContext.request.contextPath} ${avatarURL}"
								                              alt="User photo"
								                              width="130"></td>
								<td id="friends-center-align-3">
									<div class="blink"><p><strong><spring:message
											code="ownFriends.table.request.incoming.message.newRequest"/></strong></p>
									</div>
									<form action="${pageContext.request.contextPath}friendship?action=accept&id=${account.id}"
									      method="post">
										<button class="btn btn-primary" type="submit"><spring:message
												code="ownFriends.table.request.incoming.button.accept"/></button>
									</form>
									<br/>
									<form action="${pageContext.request.contextPath}friendship?action=reject&id=${account.id}"
									      method="post">
										<button class="btn btn-primary" type="submit"><spring:message
												code="ownFriends.table.request.incoming.button.reject"/></button>
									</form>
									<c:if test="${not account.active}">
										<p id="red_text"><strong><spring:message
												code="ownFriends.status.blocked"/></strong>
										</p>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</c:if>

					<c:if test="${not empty requestScope.sentFriendshipRequests}">
						<jsp:useBean id="sentFriendshipRequests" scope="request" type="java.util.List"/>

						<c:forEach items="${sentFriendshipRequests}" var="account">
							<c:choose>
								<c:when test="${account.avatarExists}">
									<c:set var="avatarURL" scope="page"
									       value="/images/uploaded?imageScope=accountAvatar&id=${account.id}"/>
								</c:when>
								<c:otherwise>
									<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
								</c:otherwise>
							</c:choose>
							<tr>
								<td id="friends-center-align-4"><a
										href="${pageContext.request.contextPath}profile?id=${account.id}">${account.firstName}${' '}${account.lastName}</a>
								</td>
								<td id="friends-center-align-5">${account.email}</td>
								<td id="photo-friends-2"><img src="${pageContext.request.contextPath} ${avatarURL}"
								                              alt="User photo"
								                              width="130"></td>
								<td id="friends-center-align-6">
									<div><p style="color:#00bc12;"><strong><spring:message
											code="ownFriends.table.request.outgoing.message.request"/></strong></p>
									</div>
									<form action="${pageContext.request.contextPath}friendship?action=cancel&id=${account.id}"
									      method="post">
										<button class="btn btn-primary" type="submit"><spring:message
												code="ownFriends.table.request.outgoing.button.cancel"/></button>
									</form>
									<c:if test="${not account.active}">
										<p id="red_text"><strong><spring:message
												code="ownFriends.status.blocked"/></strong>
										</p>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
			</c:if>

			<table class="table result-table">
				<caption><strong><spring:message code="ownFriends.table.friends.title"/></strong></caption>
				<tr>
					<th><spring:message code="ownFriends.table.friends.column.name"/></th>
					<th><spring:message code="ownFriends.table.friends.column.email"/></th>
					<th><spring:message code="ownFriends.table.friends.column.photo"/></th>
					<th><spring:message code="ownFriends.table.friends.column.status"/></th>
				</tr>
				<jsp:useBean id="friends" scope="request" type="java.util.List"/>
				<c:forEach items="${friends}" var="friend" varStatus="status">
					<c:choose>
						<c:when test="${friend.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=accountAvatar&id=${friend.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="friends-center-align-7"><a
								href="${pageContext.request.contextPath}profile?id=${friend.id}">${friend.firstName}${' '} ${friend.lastName}</a>
						</td>
						<td id="friends-left-align-5"> ${friend.email}</td>
						<td id="photo-friends-3"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                              alt="User photo"
						                              width="130"></td>
						<td id="friends-center-align-8">

							<div><p><strong><spring:message code="ownFriends.table.status"/></strong></p></div>
							<form action="${pageContext.request.contextPath}friendship?action=delete&id=${friend.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="ownFriends.table.friends.button.unfriend"/></button>
							</form>

							<c:if test="${not friend.active}">
								<p id="red_text"><strong><spring:message code="ownFriends.status.blocked"/></strong></p>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
</body>
</html>
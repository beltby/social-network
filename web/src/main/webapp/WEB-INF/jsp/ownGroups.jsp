<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="ownGroups.title"/></title>
</head>
<jsp:useBean id="groupMemberships" scope="request" type="java.util.List"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
		<div class="col-md-6">
			<form action="${pageContext.request.contextPath}group-management"
			      method="get">
				<input type="hidden" name="id" value=0>
				<button class="btn btn-primary" type="submit" style="float: right"><strong><spring:message
						code="ownGroups.button.create"/></strong>
				</button>
			</form>

			<c:if test="${not empty requestScope.outgoingGroupMembershipRequests}">
				<table class="table result-table">
					<caption><strong><spring:message code="ownGroups.table.request.title"/></strong></caption>
					<tr>
						<th><spring:message code="ownGroups.table.request.column.name"/></th>
						<th><spring:message code="ownGroups.table.request.column.description"/></th>
						<th><spring:message code="ownGroups.table.request.column.creator"/></th>
						<th><spring:message code="ownGroups.table.request.column.photo"/></th>
						<th><spring:message code="ownGroups.table.request.column.status"/></th>
					</tr>

					<jsp:useBean id="outgoingGroupMembershipRequests" scope="request" type="java.util.List"/>
					<c:forEach items="${outgoingGroupMembershipRequests}" var="membership">
						<c:set var="URL" scope="page" value="/groups"/>
						<c:choose>
							<c:when test="${membership.group.avatarExists}">
								<c:set var="avatarURL" scope="page"
								       value="/images/uploaded?imageScope=groupAvatar&id=${membership.group.id}"/>
							</c:when>
							<c:otherwise>
								<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
							</c:otherwise>
						</c:choose>
						<tr>
							<td id="groups-center-align-1"><a
									href="${pageContext.request.contextPath}group?id=${membership.group.id}">${membership.group.name}</a>
							</td>
							<td id="groups-center-align-2">${membership.group.description}</td>
							<td id="groups-center-align-3">
								<a href="${pageContext.request.contextPath}profile?id=${membership.group.creator.id}"> ${membership.group.creator.firstName}${' '}${membership.group.creator.lastName}</a>
							</td>
							<td id="photo-groups-1"><img src="${pageContext.request.contextPath} ${avatarURL}"
							                             alt="Group picture"
							                             width="130"></td>
							<td id="groups-center-align-4">
								<div><p style="color:#00bc12"><strong><spring:message
										code="ownGroups.table.request.outgoing.message.request"/></strong></p></div>
								<form action=" ${pageContext.request.contextPath}group-membership"
								      method="post">
									<input type="hidden" name="action" value='delete'>
									<input type="hidden" name="userId" value=${user.id}>
									<input type="hidden" name="groupId" value=${membership.group.id}>
									<input type="hidden" name="redirectURL" value=${URL}>
									<button class="btn btn-primary" type="submit"><spring:message
											code="ownGroups.table.request.outgoing.button.cancel"/></button>
								</form>
							</td>

						</tr>
					</c:forEach>
				</table>
			</c:if>

			<table class="table result-table">
				<caption><strong><spring:message code="ownGroups.table.groups.title"/></strong></caption>
				<thead>
				<tr>
					<th><spring:message code="ownGroups.table.groups.column.name"/></th>
					<th><spring:message code="ownGroups.table.groups.column.description"/></th>
					<th><spring:message code="ownGroups.table.groups.column.creator"/></th>
					<th><spring:message code="ownGroups.table.groups.column.photo"/></th>
					<th><spring:message code="ownGroups.table.groups.column.status"/></th>
				</tr>
				</thead>
				<tbody>
				<c:forEach items="${groupMemberships}" var="membership">
					<c:choose>
						<c:when test="${membership.group.avatarExists}">
							<c:set var="avatarURL" scope="page"
							       value="/images/uploaded?imageScope=groupAvatar&id=${membership.group.id}"/>
						</c:when>
						<c:otherwise>
							<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
						</c:otherwise>
					</c:choose>

					<tr>
						<td id="groups-center-align-5"><a
								href="${pageContext.request.contextPath}group?id=${membership.group.id}">${membership.group.name}</a>
						</td>
						<td id="groups-center-align-6">${membership.group.description}</td>
						<td id="groups-center-align-7">
							<a href="${pageContext.request.contextPath}profile?id=${membership.group.creator.id}"> ${membership.group.creator.firstName}${' '}${membership.group.creator.lastName}</a>
						</td>
						<td id="photo-groups"><img src="${pageContext.request.contextPath} ${avatarURL}"
						                           alt="Group picture"
						                           width="130"></td>

						<c:choose>
							<c:when test="${membership.role.equals('owner')}">
								<spring:message code="ownGroups.table.role.owner" var="role"/>
							</c:when>
							<c:when test="${membership.role.equals('moderator')}">
								<spring:message code="ownGroups.table.role.moderator" var="role"/>
							</c:when>
							<c:when test="${membership.role.equals('member')}">
								<spring:message code="ownGroups.table.role.member" var="role"/>
							</c:when>
						</c:choose>

						<td id="groups-center-align-8">
							<div><p><strong>${role}</strong></p>
							</div>
							<c:choose>
								<c:when test="${membership.role.equals('owner')}">
									<form action="${pageContext.request.contextPath}group-management"
									      method="get">
										<input type="hidden" name="id" value=${membership.group.id}>
										<button class="btn btn-primary" type="submit"><spring:message
												code="ownGroups.table.groups.button.edit"/></button>
									</form>
								</c:when>
								<c:otherwise>
									<form class="buttons-block"
									      action="${pageContext.request.contextPath}group-membership?action=unfollow&userId=${user.id}&groupId=${membership.group.id}&redirectURL=/groups"
									      method="post">
										<button class="btn btn-primary" type="submit"><spring:message
												code="ownGroups.table.groups.button.unfollow"/></button>
									</form>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>
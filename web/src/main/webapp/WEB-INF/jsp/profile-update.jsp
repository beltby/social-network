<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="profile-update.title"/></title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>

<div class="container">
	<div class="row">
		<br>
		<div class="col-md-3 mr-4">
			<c:choose>
				<c:when test="${editableUser.avatarExists}">
					<c:set var="avatarURL" scope="page"
					       value="/images/uploaded?imageScope=accountAvatar&id=${editableUser.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
				</c:otherwise>
			</c:choose>
			<div class="left-block-image img-fluid">
				<img src="${pageContext.request.contextPath} ${avatarURL}" alt="User photo" width="200">
				<c:if test="${not editableUser.active}">
					<span class="status-text"><spring:message code="profile-update.status.blocked"/></span>
				</c:if>
				<c:if test="${editableUser.administrator}">
					<p><span class="status-text"><spring:message code="profile-update.status.administrator"/></span></p>
				</c:if>
				<c:if test="${not editableUser.administrator}">
					<br/>
					<br/>
				</c:if>
				<c:if test="${sessionScope.user.administrator||sessionScope.user.id==editableUser.id}">
					<form action="${pageContext.request.contextPath}exportToXml" method="get">
						<input type="hidden" name="id" value=${editableUser.id}>
						<button class="btn btn-primary" type="submit" style="margin:0 19px 0 -18px"><spring:message
								code="profile-update.button.ExportToXml"/></button>
					</form>
					<br/>
					<form action="${pageContext.request.contextPath}importXml" method="post"
					      enctype="multipart/form-data">
						<input type="hidden" name="id" value=${editableUser.id}>
						<p>
							<button class="btn btn-primary" type="submit" style="margin:10px 19px 12px -10px">
								<spring:message
										code="profile-update.button.ImportFromXml"/></button>
							<br/>
							<input type="file" name="xmlFile" id="xmlFile" accept="text/xml"
							       style="margin:-11px 19px 0 16px">
						</p>
					</form>
				</c:if>
			</div>
		</div>

		<div class="col-md-6">
			<script type="text/javascript">
                let confirmationAlertMessage = "<spring:message code='js.form.confirmation.message' javaScriptEscape='true' />";
                let phoneAlertMessage = "<spring:message code='js.form.phone.message' javaScriptEscape='true' />";
                let photoAlertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
			</script>
			<form onsubmit="return formConfirmation(confirmationAlertMessage)" method="post"
			      action="${pageContext.request.contextPath}edit"
			      enctype="multipart/form-data">

				<div class="table-row">
					<label for="email" style="font-weight: bold"> <spring:message
							code="profile-update.form.email"/></label>
					<input type="email" name="email" id="email"
					       placeholder="<spring:message code="profile-update.placeholder.email"/>"
					       value="${editableUser.email}"
					       required><br/><br/>

					<label for="password" style="font-weight: bold"><spring:message
							code="profile-update.form.password"/></label>
					<input type="password" name="password" id="password"
					       placeholder="<spring:message code="profile-update.placeholder.password"/>"
					       value="${editableUser.password}" required><br/><br/>

					<label for="firstName" style="font-weight: bold"><spring:message
							code="profile-update.form.firstName"/></label>
					<input type="text" name="firstName" id="firstName"
					       placeholder="<spring:message code="profile-update.placeholder.firstName"/>"
					       value="${editableUser.firstName}" required><br/><br/>

					<label for="lastName" style="font-weight: bold"><spring:message
							code="profile-update.form.lastName"/></label>
					<input type="text" name="lastName" id="lastName"
					       placeholder="<spring:message code="profile-update.placeholder.lastName"/>"
					       value="${editableUser.lastName}" required><br/><br/>

					<label for="birthDate" style="font-weight: bold"><spring:message
							code="profile-update.form.birthDate"/></label>
					<input type="text" name="birthDate" id="birthDate"
					       placeholder="<spring:message code="profile-update.placeholder.birthDate"/>"
					       value="${editableUser.birthDate}" required> <br/>
				</div>

				<c:forEach items="${editableUser.phones}" var="phone" varStatus="status">

					<c:choose>
						<c:when test="${phone.type.equals('work')}">
							<spring:message code="profile-update.form.workPhone" var="phoneType"/>
						</c:when>
						<c:when test="${phone.type.equals('cell')}">
							<spring:message code="profile-update.form.cellPhone" var="phoneType"/>
						</c:when>
					</c:choose>

					<div class="table-row" id="phone${status.index}"><br>
						<label for="phone${status.index}"
						       style="font-weight: bold"> ${phoneType}</label>
						<input type="tel" name="phones[${status.index}].value" id="phone${status.index}"
						       placeholder="<spring:message code="profile-update.placeholder.phoneNumberFormat"/>"
						       onchange="phoneValidation(this.value,phoneAlertMessage)" value="${phone.value}" required>
						<input type="hidden" name="phones[${status.index}].type" value=${phone.type}>
					</div>

					<c:if test="${status.index != 0}">
						<input type="button" value="<spring:message code="profile-update.form.button.delete"/>"
						       class="btn btn-primary button-delete" id="delete${status.index}"
						       onclick="deletePhone(${status.index})">
					</c:if>
				</c:forEach>

				<br>
				<div id="newPhoneInput"></div>
				<button class="btn btn-primary" type="button" id="addCellPhoneButton" class="button-add"
				        onclick="addPhone('cell',${editableUser.phones.size()})"><spring:message
						code="profile-update.form.button.addCellPhone"/>
				</button>
				<div id="extraSpace1">
					<br></div>
				<button type="button" class="btn btn-primary" id="addWorkPhoneButton" class="button-add"
				        onclick="addPhone('work', ${editableUser.phones.size()})"><spring:message
						code="profile-update.form.button.addWorkPhone"/>
				</button>
				<div id="extraSpace2">
					<br></div>
				<div class="table-row">
					<label for="homeAddress" style="font-weight: bold"><spring:message
							code="profile-update.form.homeAddress"/></label>
					<input type="text" name="homeAddress" id="homeAddress"
					       placeholder="<spring:message code="profile-update.placeholder.homeAddress"/>"
					       value="${editableUser.homeAddress}"><br/><br/>

					<label for="workAddress" style="font-weight: bold"><spring:message
							code="profile-update.form.workAddress"/></label>
					<input type="text" name="workAddress" id="workAddress"
					       placeholder="<spring:message code="profile-update.placeholder.workAddress"/>"
					       value="${editableUser.workAddress}"><br/><br/>

					<label for="icq" style="font-weight: bold"><spring:message code="profile-update.form.icq"/></label>
					<input type="text" name="icq" id="icq"
					       placeholder="<spring:message code="profile-update.placeholder.icq"/>"
					       value="${editableUser.icq}"><br/><br/>

					<label for="skype" style="font-weight: bold"><spring:message
							code="profile-update.form.skype"/></label>
					<input type="text" name="skype" id="skype"
					       placeholder="<spring:message code="profile-update.placeholder.skype"/>"
					       value="${editableUser.skype}"><br/><br/>

					<label for="additionalData" style="font-weight: bold"><spring:message
							code="profile-update.form.additionalData"/></label>
					<input type="text" name="additionalData" id="additionalData"
					       placeholder="<spring:message code="profile-update.placeholder.additionalData"/>"
					       value="${editableUser.additionalData}"><br/><br/>

					<label for="image" style="font-weight: bold"><spring:message
							code="profile-update.form.photo"/></label>
					<input type="file" name="image" id="image" accept="image/*"
					       onchange="validatePhoto(this,photoAlertMessage)">
					<br/><br/>
				</div>
				<input type="hidden" name="id" value="${editableUser.id}">

				<button class="btn btn-primary" type="submit" style="margin:0 19px 0 -4px"><spring:message
						code="profile-update.form.button.save"/></button>

				<input class="btn btn-primary" type=button value="<spring:message code="buttons.back"/>"
				       onCLick="history.back()">
			</form>
			<br/>
			<p>
				<button class="btn btn-primary" onClick="window.location.reload();" style="margin:0 19px 0 -4px">
					<spring:message
							code="buttons.refresh"/></button>
			</p>
		</div>

	</div>
</div>
</body>
</html>
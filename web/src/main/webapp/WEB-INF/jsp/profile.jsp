<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title>${profile.firstName}${" "}${profile.lastName}</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<jsp:useBean id="profile" scope="request" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>
<jsp:useBean id="posts" scope="request" type="java.util.List"/>
<c:set var="none" scope="page" value="-"/>
<c:set var="active" scope="page" value="${profile.active}"/>
<body>
<a id="top"></a>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<div class="col-md-3 mr-4">

			<c:choose>
				<c:when test="${profile.avatarExists}">
					<c:set var="avatarURL" scope="page"
					       value="/images/uploaded?imageScope=accountAvatar&id=${profile.id}"/>
				</c:when>
				<c:otherwise>
					<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
				</c:otherwise>
			</c:choose>

			<div class="left-block-image img-fluid">
				<img src="${pageContext.request.contextPath} ${avatarURL}" alt="User photo" width="200">
				<c:if test="${not active}">
					<span class="status-text"><spring:message code="profile.status.blocked"/></span>
				</c:if>
				<c:if test="${profile.administrator}">
					<span class="status-text"><spring:message code="profile.status.administrator"/></span>
				</c:if>
			</div>

			<c:if test="${not profile.administrator && active}">
				<br/> <br/>
			</c:if>

			<div class="left-nav-bar">
				<ul>
					<li><a href="${pageContext.request.contextPath}/"><spring:message code="profile.button.return"/></a>
					</li>
					<c:if test="${active}">
						<li><a href="${pageContext.request.contextPath}chat?profileId=${profile.id}"><spring:message
								code="profile.button.sendMessage"/></a></li>
					</c:if>
					<li><a href="${pageContext.request.contextPath}profile?id=${profile.id}"><spring:message
							code="profile.button.toUserProfile"/></a></li>
					<li><a href="${pageContext.request.contextPath}friends?profileId=${profile.id}"><spring:message
							code="profile.button.toUserFriends"/> </a></li>
					<li><a href="${pageContext.request.contextPath}groups?profileId=${profile.id}"><spring:message
							code="profile.button.toUserGroups"/> </a>
					</li>
				</ul>

				<c:if test="${active}">
					<c:set var="friendshipStatus" scope="page" value="${requestScope.friendshipStatus}"/>
					<c:choose>
						<c:when test="${friendshipStatus.equals('friend')}">
							<form action="${pageContext.request.contextPath}friendship?action=delete&id=${profile.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="profile.button.friendship.unfriend"/></button>
							</form>
						</c:when>

						<c:when test="${friendshipStatus.equals('outgoing')}">
							<form action="${pageContext.request.contextPath}friendship?action=cancel&id=${profile.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="profile.button.friendship.delete"/></button>
							</form>
						</c:when>

						<c:when test="${friendshipStatus.equals('received')}">
							<form action="${pageContext.request.contextPath}friendship?action=accept&id=${profile.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="profile.button.friendship.accept"/></button>
							</form>
							<br/>
							<form action="${pageContext.request.contextPath}friendship?action=reject&id=${profile.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="profile.button.friendship.reject"/></button>
							</form>
						</c:when>

						<c:when test="${friendshipStatus.equals('stranger')}">
							<form action="${pageContext.request.contextPath}friendship?action=offer&id=${profile.id}"
							      method="post">
								<button class="btn btn-primary" type="submit"><spring:message
										code="profile.button.friendship.request"/></button>
							</form>
						</c:when>
					</c:choose>
					<br/>
				</c:if>

				<c:if test="${user.administrator}">
					<form action="${pageContext.request.contextPath}edit"
					      method="get">
						<input type="hidden" name="profileId" value=${profile.id}>
						<button class="btn btn-primary" type="submit"><spring:message
								code="profile.button.editProfile"/></button>
					</form>
					<br/>

				<c:choose>
					<c:when test="${profile.active}">
						<spring:message code="profile.button.blockProfile" var="actionName" scope="page"/>
					</c:when>
					<c:otherwise>
						<spring:message code="profile.button.unblockProfile" var="actionName" scope="page"/>
					</c:otherwise>
				</c:choose>

				<form action="${pageContext.request.contextPath}profile?action=changeActivity&id=${profile.id}"
				      method="post">
					<button class="btn btn-primary" type="submit">${actionName}</button>
				</form>
				<br/>

				<c:choose>
					<c:when test="${not profile.administrator}">
						<form action="${pageContext.request.contextPath}profile?action=setAdministrator&id=${profile.id}"
						      method="post">
							<button type="submit"><spring:message code="profile.button.setAdministrator"/></button>
						</form>
					</c:when>
				</c:choose>
				</c:if>
			</div>
		</div>

		<div class="col-md-6">
			<c:if test="${not empty sessionScope.updateResult}">
				<c:choose>
					<c:when test="${sessionScope.updateResult}">
						<div><p style="color:#00bc12;font-weight: bold"><spring:message
								code="profile.message.updateSuccess"/></p></div>
						<c:remove var="updateResult"/>
					</c:when>
					<c:otherwise>
						<div><p style="color:#ff0000;font-weight: bold"><spring:message
								code="profile.message.updateFailed"/></p></div>
						<c:remove var="updateResult"/>
					</c:otherwise>
				</c:choose>
			</c:if>

			<p><strong><spring:message code="profile.description.firstName"/></strong> ${profile.firstName}</p>
			<p><strong><spring:message code="profile.description.lastName"/></strong> ${profile.lastName}</p>
			<p><strong><spring:message code="profile.description.email"/></strong> ${profile.email}</p>
			<p><strong><spring:message code="profile.description.birthDate"/></strong> ${profile.birthDate}</p>

			<c:forEach items="${profile.phones}" var="phone">
				<c:choose>
					<c:when test="${phone.type.equals('cell')}">
						<p><strong><spring:message code="profile.description.cellPhone"/></strong> ${phone.value}</p>
					</c:when>
					<c:otherwise>
						<p><strong><spring:message code="profile.description.workPhone"/></strong> ${phone.value}</p>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<p><strong><spring:message code="profile.description.homeAddress"/></strong> <c:choose>
				<c:when test="${empty profile.homeAddress}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.homeAddress}
				</c:otherwise>
			</c:choose></p>
			<p><strong><spring:message code="profile.description.workAddress"/></strong> <c:choose>
				<c:when test="${empty profile.workAddress}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.workAddress}
				</c:otherwise>
			</c:choose></p>
			<p><strong><spring:message code="profile.description.icq"/></strong> <c:choose>
				<c:when test="${empty profile.icq}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.icq}
				</c:otherwise>
			</c:choose></p>
			<p><strong><spring:message code="profile.description.skype"/></strong> <c:choose>
				<c:when test="${empty profile.skype}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.skype}
				</c:otherwise>
			</c:choose></p>
			<p><strong><spring:message code="profile.description.additionalData"/></strong> <c:choose>
				<c:when test="${empty profile.additionalData}">
					${none}
				</c:when>
				<c:otherwise>
					${profile.additionalData}
				</c:otherwise>
			</c:choose></p>
			<p><strong><spring:message
					code="profile.description.registrationDate"/></strong> ${profile.registrationDate}
			</p>

			<c:if test="${active}">
				<a id="form"></a>
				<form method="post" action="${pageContext.request.contextPath}post" enctype="multipart/form-data">
					<p><label for="text"></label>
						<textarea name="text" id="text" cols="48" rows="8"></textarea>
					</p>
					<script type="text/javascript">
                        let alertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
					</script>
					<input type="file" name="image" class="image" accept="image/*"
					       onchange="validatePhoto(this,alertMessage)">
					<input type="hidden" name="senderId" value="${user.id}"><br/>
					<input type="hidden" name="recipientId" value="${profile.id}"><br/>
					<input type="hidden" name="scope" value="wall">
					<button class="btn btn-primary" type="submit"><spring:message
							code="profile.wall.button.send"/></button>
					<br/>
				</form>

				<br/>
				<table class="table result-table">
					<tr>
						<th><spring:message code="profile.table.column.sender"/></th>
						<th><spring:message code="profile.table.column.message"/></th>
						<th><spring:message code="profile.table.column.dateAndTime"/></th>
					</tr>

					<c:forEach items="${posts}" var="post" varStatus="status">
						<c:if test="${post.imageExists}">
							<c:set var="imageURL" scope="page"
							       value="/images/uploaded?imageScope=attachedImage&id=${post.id}"/>
						</c:if>

						<c:set var="author" scope="page" value="${post.sender}"/>
						<jsp:useBean id="author" type="com.getjavajob.training.socialnetwork1907.kapustsina.Account"/>

						<c:choose>
							<c:when test="${post.sender.id==user.id}">
								<c:set var="senderFirstName" scope="page" value="${user.firstName}"/>
								<c:set var="senderLastName" scope="page" value="${user.lastName}"/>
								<c:set var="senderId" scope="page" value="${user.id}"/>
							</c:when>

							<c:otherwise>
								<c:set var="senderFirstName" scope="page" value="${author.firstName}"/>
								<c:set var="senderLastName" scope="page" value="${author.lastName}"/>
								<c:set var="senderId" scope="page" value="${author.id}"/>
							</c:otherwise>
						</c:choose>

						<c:choose>
							<c:when test="${post.sender.id==user.id&&user.avatarExists}">
								<c:set var="avatarURL" scope="page"
								       value="/images/uploaded?imageScope=accountAvatar&id=${user.id}"/>
							</c:when>
							<c:when test="${post.sender.id==author.id&&author.avatarExists}">
								<c:set var="avatarURL" scope="page"
								       value="/images/uploaded?imageScope=accountAvatar&id=${author.id}"/>
							</c:when>
							<c:otherwise>
								<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
							</c:otherwise>
						</c:choose>

						<tr>
							<td id="profile-wall-photo"><img src="${pageContext.request.contextPath} ${avatarURL}"
							                                 alt="Sender photo"
							                                 width="60">
								<br/> <a
										href="${pageContext.request.contextPath}profile?id=${senderId}">${senderFirstName}${' '} ${senderLastName}</a>
								<c:if test="${not author.active}">
									<br/>
									<span id="red_text"><spring:message code="profile.status.blocked"/></span>
								</c:if>
							</td>
							<td id="profile-wall-left-align">
								<div> ${post.text}</div>
								<c:if test="${post.imageExists}">
									<img src="${pageContext.request.contextPath} /images/uploaded?imageScope=attachedImage&id=${post.id}"
									     alt="Attached image" width="100"
									     onclick="window.open('/images/uploaded?imageScope=attachedImage&id='+${post.id}, '_blank');">
								</c:if>
							</td>
							<td id="profile-wall-center-align">
								<fmt:parseDate value="${post.creationTime}" pattern="yyyy-MM-dd'T'HH:mm"
								               var="parsedDateTime"
								               type="both"/>
								<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${parsedDateTime}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
				<a href="#top"><spring:message code="profile.button.top"/></a>
			</c:if>
		</div>
	</div>
</div>
</body>
</html>
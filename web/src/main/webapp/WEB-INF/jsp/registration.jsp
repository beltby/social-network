<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><spring:message code="registration.message"/></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
</head>
<body style="background-color: #F9F9F9">

<div class="text-center">
	<div><p class="h5 mb-0 mt-0 pt-3 font-weight-bold" style="color:#000000;"><spring:message
			code="registration.title"/></p></div>
</div>
<br/>
<div class="container">
	<div class="row">
		<script type="text/javascript">
            let confirmationAlertMessage = "<spring:message code='js.form.confirmation.message' javaScriptEscape='true' />";
            let phoneAlertMessage = "<spring:message code='js.form.phone.message' javaScriptEscape='true' />";
            let photoAlertMessage = "<spring:message code='js.form.photo.message' javaScriptEscape='true' />";
		</script>
		<div class="col-sm-4 offset-md-4 align-self-center">
			<form onsubmit="return formConfirmation(photoAlertMessage)" method="post"
			      action="${pageContext.request.contextPath}registration" class="form-signin"
			      enctype="multipart/form-data">
				<input type="email" name="email" class="form-control" placeholder="<spring:message
			code="registration.form.placeholder.email"/>" required autofocus>
				<input type="password" name="password" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.password"/>" required>

				<input type="text" name="firstName" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.firstName"/>" required>

				<input type="text" name="lastName" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.lastName"/>" required>

				<input type="text" name="birthDate" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.birthDate"/>" required>

				<input type="tel" name="phones[0].value" class="form-control mt-3"
				       onchange="phoneValidation(this.value,phoneAlertMessage)" placeholder="<spring:message
					code="registration.form.placeholder.cellPhone"/>"
				       required/>
				<input name="phones[0].type" value="cell" hidden/>

				<input type="tel" name="phones[1].value" class="form-control mt-3"
				       onchange="phoneValidation(this.value,alertMessage)"
				       placeholder="<spring:message
					code="registration.form.placeholder.workPhone"/>"/>
				<input name="phones[1].type" value="work" hidden/>

				<input type="text" name="homeAddress" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.homeAddress"/>">

				<input type="text" name="workAddress" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.workAddress"/>">

				<input type="text" name="icq" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.icq"/>">

				<input type="text" name="skype" class="form-control mt-3" placeholder="<spring:message
					code="registration.form.placeholder.skype"/>">

				<input type="text" name="additionalData" class="form-control mt-3 mb-3" placeholder="<spring:message
					code="registration.form.placeholder.additionalData"/>">
				<label for="image"> <spring:message code="registration.form.photo"/>
					<br/><input type="file" name="image" class="form-control-file mb-3" id="image" accept="image/*"
					            style="margin:3px 0 0 0"
					            onchange="validatePhoto(this,photoAlertMessage)">
				</label>
				<button type="submit" class="btn btn-lg btn-primary btn-block"><spring:message
						code="registration.form.button.createAccount"/></button>
				<label class="h5"><a href="${pageContext.request.contextPath}auth"><spring:message
						code="registration.form.button.signIn"/></a></label>

				<spring:message code="main.language.current" var="currentLanguage"/>
				<c:choose>
					<c:when test="${currentLanguage.equals('ru')}">
						<c:set var="switchLangKey" scope="page" value="?lang=en"/>
					</c:when>
					<c:when test="${currentLanguage.equals('en')}">
						<c:set var="switchLangKey" scope="page" value="?lang=ru"/>
					</c:when></c:choose>

				<spring:message code="auth.switch.language" var="language"/>
				<label class="h5 float-right"><a
						href="${pageContext.request.contextPath}${switchLangKey}">${language}</a></label>

			</form>
		</div>
		<div class="column side"></div>
	</div>
</div>
</body>
</html>
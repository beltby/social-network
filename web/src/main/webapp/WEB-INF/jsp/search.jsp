<%@include file="/WEB-INF/jsp/common-head.jsp" %>
<title><spring:message code="search.title"/></title>
</head>
<jsp:useBean id="accounts" scope="request" type="java.util.List"/>
<jsp:useBean id="groups" scope="request" type="java.util.List"/>
<body>
<%@include file="/WEB-INF/jsp/common-menu.jsp" %>
<div class="container">
	<div class="row">
		<%@include file="/WEB-INF/jsp/common-left-content.jsp" %>
		<div class="col-md-6">

			<c:choose>
				<c:when test="${empty accounts&&empty groups}">
					<br><br>
					<p class="h3 text-center"><strong><spring:message code="search.result.nothing"/></strong></p>
				</c:when>
				<c:otherwise>

					<table id="accountsResult" class="table result-table">
						<caption><strong><spring:message code="search.table.result.users.title"/></strong></caption>
						<thead>
						<tr>
							<th><spring:message code="search.table.result.users.column.name"/></th>
							<th><spring:message code="search.table.result.users.column.email"/></th>
							<th><spring:message code="search.table.result.users.column.photo"/></th>
							<th><spring:message code="search.table.result.users.column.status"/></th>
						</tr>
						</thead>

						<tbody id="accountResultRow">
						<c:if test="${not empty accounts}">
							<c:forEach items="${accounts}" var="user">
								<c:choose>
									<c:when test="${user.avatarExists}">
										<c:set var="avatarURL" scope="page"
										       value="/images/uploaded?imageScope=accountAvatar&id=${user.id}"/>
									</c:when>
									<c:otherwise>
										<c:set var="avatarURL" scope="page" value="/images/defaultUserPicture.jpg"/>
									</c:otherwise>
								</c:choose>

								<c:choose>
									<c:when test="${not user.active}">
										<spring:message code="search.table.result.users.status.blocked" var="status"/>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${user.administrator}">
												<spring:message code="search.table.result.users.status.administrator"
												                var="status"/>
											</c:when>
											<c:otherwise>
												<spring:message code="search.table.result.users.status.user"
												                var="status"/>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>

								<tr>
									<td id="search-center-align-1"><a
											href="${pageContext.request.contextPath}profile?id=${user.id}">${user.firstName}${' '} ${user.lastName}</a>
									</td>
									<td id="search-left-align-1"> ${user.email}</td>
									<td id="photo-search-2"><img src="${pageContext.request.contextPath} ${avatarURL}"
									                             alt="User photo"
									                             width="100"></td>
									<td id="search-left-align-2"><strong>${status}</strong></td>
								</tr>
							</c:forEach>
						</c:if>
						</tbody>
					</table>
					<br>

					<table id="groupsResult" class="table result-table">
						<caption><strong><spring:message code="search.table.result.groups.title"/></strong></caption>
						<thead>
						<tr>
							<th><spring:message code="search.table.result.groups.column.name"/></th>
							<th><spring:message code="search.table.result.groups.column.description"/></th>
							<th><spring:message code="search.table.result.groups.column.photo"/></th>
							<th><spring:message code="search.table.result.groups.column.creationDate"/></th>
						</tr>
						</thead>

						<tbody id="groupResultRow">
						<c:if test="${not empty groups}">
							<c:forEach items="${groups}" var="group">
								<c:choose>
									<c:when test="${group.avatarExists}">
										<c:set var="avatarURL" scope="page"
										       value="/images/uploaded?imageScope=groupAvatar&id=${group.id}"/>
									</c:when>
									<c:otherwise>
										<c:set var="avatarURL" scope="page" value="/images/defaultGroupPicture.jpg"/>
									</c:otherwise>
								</c:choose>
								<tr>
									<td id="search-center-align-2"><a
											href="${pageContext.request.contextPath}group?id=${group.id}">${group.name}</a>
									</td>
									<td id="search-left-align-3"> ${group.description}</td>
									<td id="photo-search-1"><img src="${pageContext.request.contextPath} ${avatarURL}"
									                             alt="Group picture"
									                             width="100"></td>
									<td id="search-center-align-5">
										<strong>${group.registrationDate}</strong>
									</td>
								</tr>
							</c:forEach>
						</c:if>
						</tbody>
					</table>
					<br>

					<div id="navButtons">
						<c:if test="${not lastPage}">
							<button class="btn btn-primary" id="nextButton" type="submit" style="float: right">
								<strong><spring:message
										code="search.table.result.button.next"/></strong></button>
						</c:if>

						<c:if test="${page > 0}">
							<button class="btn btn-primary" id="prevButton" type="submit" style="float: left">
								<strong><spring:message
										code="search.table.result.button.back"/></strong></button>
						</c:if>
					</div>

					<script type="text/javascript">
                        let strings = [];
                        strings['status.administrator'] = "<spring:message code='search.table.result.users.status.administrator' javaScriptEscape='true' />";
                        strings['status.user'] = "<spring:message code='search.table.result.users.status.user' javaScriptEscape='true' />";
                        strings['status.blocked'] = "<spring:message code='search.table.result.users.status.blocked' javaScriptEscape='true' />";
					</script>

					<script id="accountTemplate" type="x-tmpl-mustache">
<tr>
		<td id="search-center-align-1"><a
			href="${pageContext.request.contextPath}/profile?id={{id}}">{{firstName}}${' '} {{lastName}}</a></td>
		<td id="search-left-align-1"> {{email}}</td>
		<td id="photo-search-2"><img src="${pageContext.request.contextPath} {{avatarUrl}}"
		                             alt="User photo"
		                             width="100"></td>
		<td id="search-left-align-2"><strong>{{status}}</strong></td>
</tr>
					</script>
					<script id="groupTemplate" type="x-tmpl-mustache">
<tr>
		<td id="search-center-align-2"><a
			href="${pageContext.request.contextPath}/group?id={{id}}">{{name}}</a></td>
		<td id="search-left-align-3"> {{description}}</td>
		<td id="photo-search-1"><img src="${pageContext.request.contextPath} {{avatarUrl}}"
		                             alt="Group picture"
		                             width="100"></td>
		<td id="search-center-align-5">	<strong>{{registrationDate}}</strong>
</tr>
					</script>
					<script id="nextButtonTemplate" type="x-tmpl-mustache">
            <button class="btn btn-primary" id="nextButton" type="submit" style="float: right"><strong><strong><spring:message
							code="search.table.result.button.next"/></strong></button>
					</script>

					<script id="prevButtonTemplate" type="x-tmpl-mustache">
            <button class="btn btn-primary" id="prevButton" type="submit" style="float: left"><strong><strong><spring:message
							code="search.table.result.button.back"/></strong></button>
					</script>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://unpkg.com/mustache@latest"></script>
<script src="${pageContext.request.contextPath}/js/searchPaginated.js"></script>
<script>
    let accountsLastPage = ${accountsLastPage};
    let groupsLastPage = ${groupsLastPage};
    let accountsQty = ${accounts.size()};
    let groupsQty = ${groups.size()};
    let currentPage = "${page}";
    let query = "${q}";
    let lastPage = "${lastPage}";
</script>
</body>
</html>
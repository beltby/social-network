$(function () {
    var stompClient = null;
    connect();

    function setConnected(connected) {
        document.getElementById('sendMessage').disabled = !connected;
    }

    function connect() {
        var socket = new SockJS(contextPath + '/chat');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            setConnected(true);
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/messages', function (messageOutput) {
                console.log('Connected: ' + frame);
                showMessageOutput(JSON.parse(messageOutput.body));
            });
        });
    }

    $('#sendMessage').click(function (e) {
        console.log('sendMessage()');
        var text = document.getElementById('text').value;
        var imageExists = "";
        imageExists = !!document.getElementById('image').value;

        var msg = {
            'senderId': senderId,
            'recipientId': recipientId,
            'text': text,
            'imageExists': imageExists
        };
        console.log(JSON.stringify(msg));
        stompClient.send("/app/chat", {}, JSON.stringify(msg));
        document.getElementById('text').value = '';
    });

    function showMessageOutput(messageOutput) {
        var name = "";
        if (messageOutput.senderId == senderId)
            name = senderName;
        else name = recipientName;

        var avatarURL = "";
        if (messageOutput.senderId == senderId && senderAvatarExists)
            avatarURL = "/images/uploaded?imageScope=accountAvatar&id=" + senderId;
        if (messageOutput.senderId != senderId && senderAvatarExists)
            avatarURL = "/images/uploaded?imageScope=accountAvatar&id=" + recipientId;
        if (messageOutput.senderId == senderId && senderAvatarExists == 'false')
            avatarURL = "/images/defaultUserPicture.jpg";
        if (messageOutput.senderId != senderId && recipientAvatarExists == 'false')
            avatarURL = "/images/defaultUserPicture.jpg";

        $('#resultTable').append(
            '<tr id="message">' +
            '<td id="chat-photo">' +
            '<img src=' + avatarURL + ' alt="Sender photo" width="60"><br/>' +
            '<a href=' + contextPath + '/profile?id=' + messageOutput.senderId + '>' + name + '</a>' +
            '</td>' +
            '<td>' + messageOutput.text + '</td>' +
            '<td id="chat-center-align-1">' + messageOutput.creationTime + '</td>' +
            '</tr>'
        )
    }
});
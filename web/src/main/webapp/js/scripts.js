function phoneValidation(number, message) {
    const pattern = /^\+[0-9]+$/;
    if (!number.match(pattern)) {
        alert(message)
    }
}

function formConfirmation(message) {
    if (confirm(message)) {
        this.form.submit();
    } else {
        return false;
    }
}

function addPhone(phoneType, newId) {
    let newDiv;
    let innerElementId;
    newDiv = '<div class="table-row" id="phone' + newId + '">\r\n' +
        '<label for="phone' + newId + '" style="font-weight: bold"> ' + phoneType.charAt(0).toUpperCase() + phoneType.slice(1) + ' phone:</label>\r\n ' +
        '<input type="tel" name="phones[' + newId + '].value" id="phone' + newId + '" placeholder="International number format, starts with a +"\r\n' +
        ' onchange="phoneValidation(this.value)" required>\r\n</div>\r\n' +
        '<input type="hidden" name="phones[' + newId + '].type" value=' + phoneType + '>\r\n' +
        '<input type="button" value="Delete" class="button-delete" id="delete' + newId + '" ' +
        'onclick="deletePhone(' + newId + ')">\r\n<br id="extraSpace">';
    innerElementId = 'newPhoneInput'
    document.getElementById('extraSpace1').remove();
    document.getElementById('extraSpace2').remove();
    document.getElementById('addCellPhoneButton').remove();
    document.getElementById('addWorkPhoneButton').remove();
    /* document.getElementById(addWorkPhoneButton+1).remove();*/
    /* } else {
         newDiv = '<div class="table-row" id="work' + newId + '">\r\n' +
             '<label for="workPhone' + newId + '" style="font-weight: bold"> Work phone:</label>\r\n ' +
             '<input type="tel" name="workPhone" id="workPhone' + newId + '" placeholder="International number format, starts with a +"\r\n' +
             ' onchange="phoneValidation(this.value)" required>\r\n</div>\r\n' +
             '<input type="button" value="Delete" class="button-delete" id="deleteWork' + newId + '" ' +
             'onclick="deletePhone(\'work\',' + newId + ')">\r\n<br id="extraSpace">';
         innerElementId = 'newWorkInput'
     }*/
    document.getElementById(innerElementId).innerHTML += newDiv;
}

function deletePhone(id) {
    if (id !== 0) {
        document.getElementById('phone' + id).remove();
        let buttonId = 'delete';
        document.getElementById(buttonId + id).remove();
        document.getElementById('extraSpace').remove();
    }

}

function validatePhoto(file, message) {
    if (file.files[0].size > 4194304) {
        alert(message);
        file.value = "";
    }
}

function jumpToForm() {
    location.hash = "#form";
}

function uploadXmlFile() {
    document.getElementById('getFile').onclick = function() {
        document.getElementById('xmlFile').click();
    };
}

$(document).ready(
    function () {
        $("#search").autocomplete({
            source: function (request, response) {
                $.get(window.location.origin + '/searchAjax?q=' + request.term, function (data) {
                    response($.map(data, function (searchResult) {
                        return {
                            valueScope: searchResult.scope,
                            valueId: searchResult.id,
                            label: searchResult.value + ' (' + searchResult.scope.charAt(0).toUpperCase() + ')',
                            text: searchResult.value
                        }
                    }));
                });
            },
            select: function (event, ui) {
                location.href = window.location.origin + "/" + ui.item.valueScope + "?id=" + ui.item.valueId;
            },
            minLength: 2
        });
    })
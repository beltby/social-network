$(function () {
        const accountSearchUrl = 'searchAccountPaginated';
        const groupSearchUrl = 'searchGroupPaginated';

        console.log("totalAccount", accountsQty);
        console.log("totalGroup", groupsQty)
        console.log("lastPage", lastPage)
        console.log("currentPage", currentPage)
        console.log("accountsLastPage", accountsLastPage)
        console.log("groupsLastPage", groupsLastPage)
        console.log("q", query)

        $(document).on("click", "#nextButton", function () {
            console.log("Button next pressed")
            clearResultTable();
            currentPage++;
            fetchData(accountSearchUrl, 'Account');
            fetchData(groupSearchUrl, 'Group');
            drawNavButtons()
            changeUrl(query, currentPage)
        });

        $(document).on("click", "#prevButton", function () {
            console.log("Button back pressed")
            clearResultTable();
            currentPage--;
            fetchData(accountSearchUrl, 'Account');
            fetchData(groupSearchUrl, 'Group');
            drawNavButtons()
            changeUrl(query, currentPage)
        });

        function fetchData(url, type) {
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    q: query,
                    page: currentPage,
                },
                success: function (result) {
                    if (type === 'Account') {
                        drawAccountsRows(result);
                        console.log('Accounts search done');
                    } else {
                        drawGroupsRows(result);
                        console.log('Groups search done');
                    }
                },
            });
        }

        function drawAccountsRows(result) {
            let template = $(`#accountTemplate`).html();
            for (let i = 0; i < result.length; i++) {
                let account = result[i];

                let status = "";
                if (!account.active) {
                    status = "Blocked";
                } else if (account.administrator) {
                    status = "Administrator";
                } else {
                    status = "User"
                }

                let avatarUrl = "";
                if (result[i].avatarExists === true) {
                    avatarUrl = "/images/uploaded?imageScope=accountAvatar&id=" + account.id;
                } else {
                    avatarUrl = "/images/defaultUserPicture.jpg";
                }

                let userStatus;
                if (!account.active) {
                    userStatus = strings['status.blocked']
                } else if (account.administrator) {
                    userStatus = strings['status.administrator']
                } else {
                    userStatus = strings['status.user']
                }


                let output = Mustache.render(template, {
                    id: account.id,
                    avatarUrl: avatarUrl,
                    firstName: account.firstName,
                    lastName: account.lastName,
                    email: account.email,
                    status: userStatus
                });
                $("#accountResultRow").append(output);
            }
        }

        function drawGroupsRows(result) {
            let template = $(`#groupTemplate`).html();
            for (let i = 0; i < result.length; i++) {
                let group = result[i];
                let avatarUrl = "";
                if (result[i].avatarExists === true) {
                    avatarUrl = "/images/uploaded?imageScope=groupAvatar&id=" + group.id;
                } else {
                    avatarUrl = "/images/defaultGroupPicture.jpg";
                }

                let output = Mustache.render(template, {
                    id: group.id,
                    name: group.name,
                    description: group.description,
                    avatarUrl: avatarUrl,
                    registrationDate: group.registrationDate
                });
                $("#groupResultRow").append(output);
            }
        }

        function clearResultTable() {
            $("#accountsResult tbody tr").remove();
            $("#groupsResult tbody tr").remove();
            $("#nextButton").remove()
            $("#prevButton").remove()
        }

        function drawNavButtons() {
            if (currentPage < accountsLastPage || currentPage < groupsLastPage) {
                $('#navButtons').append(Mustache.render($(`#nextButtonTemplate`).html(), {}));
            }

            if (currentPage > 0) {
                $('#navButtons').append(Mustache.render($(`#prevButtonTemplate`).html(), {}));
            }
        }

        function changeUrl(query, currentPage) {
            history.replaceState({}, '', 'search?q=' + query + '&page=' + currentPage);
        }
    }
);